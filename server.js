const express = require('express');
const app = express();

app.use(express.static('dist/bds-admin'));

app.get('/', (req, res) => {
  res.render('dist/bds-admin/index.html');
});

const PORT = 5000;

app.listen(PORT, () => {
  console.log(`BDS Admin is running on port ${PORT}`);
});
