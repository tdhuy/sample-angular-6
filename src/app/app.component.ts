import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionService } from './shared/services/session.service';
import { CookieService } from 'ngx-cookie';

import { GlobalService } from './shared/services/global.service';
import { AlertService } from './shared/services/alert.service';

import * as moment from 'moment';

import CookieNames from './shared/constants/cookies';

const defaultPage = {
  login: 'login',
  dashboard: '_.wrap.dashboard'
};

const bodyClassByState = {
  login: 'page-header-fixed page-content-white login',
  others: 'page-header-fixed page-content-white page-sidebar-closed-hide-logo',
  othersCollapsedSidebar: 'page-header-fixed page-content-white page-sidebar-closed'
};

declare var $: any;

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'app';
  $: any;
  collapsed: Boolean = false;
  isLoginPage: Boolean = false;
  fullPageLoading$: Observable<boolean>;
  fullPageLoadingToLogin: Observable<boolean>;

  constructor(public globalService: GlobalService,
              private sessionService: SessionService,
              private cookieService: CookieService,
              private router: Router) {
    this.collapsed = this.globalService.sidebarCollapsed;
    this.$ = $;
    this.initCookie();

    this.router.events.subscribe((event) => {

      this.globalService.setFullPageLoading(false);

      const token = this.cookieService.get(CookieNames.token);
      const user = this.cookieService.getObject(CookieNames.user);

      if (event instanceof NavigationStart) {
        this.globalService.setFullPageLoading(true);
        this.handleUpdateClassesBody(event, token, user);
      }

      if (event instanceof NavigationEnd) {
        if (event.url === '/login') {

          document.querySelector('body').className = bodyClassByState.login;
        } else {
          setTimeout(() => {
            this.globalService.setFullPageLoadingLogin(false);
          }, 500);
        }
      }
    });
  }

  ngOnInit(): void {
    this.fullPageLoading$ = this.globalService.getFullPageLoadingStatus();

    this.fullPageLoadingToLogin = this.globalService.getFullPageLoadingLogin();

  }

  initCookie(): void {
    const token = this.cookieService.get(CookieNames.token);
    const user = this.cookieService.getObject(CookieNames.user);

    if (token && user) {
      this.globalService.loggedInUser = user;
      this.sessionService.set(token, user);
    } else {
      this.sessionService.remove();
      this.router.navigate(['/login']);
    }

    // sidebar collapsed
    const collapsed = this.cookieService.get(CookieNames.collapsed);
    this.globalService.sidebarCollapsed = collapsed === 'true';
  }

  private handleUpdateClassesBody(event: any, token, user) {
    if (event.url === '/login') {
      if (token && user) {
        this.router.navigate(['/dashboard']);
        return;
      }
      document.querySelector('body').className = bodyClassByState.login;
    } else {
      if (this.globalService.sidebarCollapsed) {
        document.querySelector('body').className = bodyClassByState.othersCollapsedSidebar;
        const fn = setInterval(() => {
          const target = document.querySelector('.page-sidebar-menu');
          if (target) {
            target.classList.add('page-sidebar-menu-closed');
            clearInterval(fn);
          }
        }, 100);
      } else {
        document.querySelector('body').className = bodyClassByState.others;
        const fn = setInterval(() => {
          const target = document.querySelector('.page-sidebar-menu');
          if (target) {
            target.classList.remove('page-sidebar-menu-closed');
            clearInterval(fn);
          }
        }, 100);
      }
      if (!token || !user) {
        this.sessionService.remove();
        this.router.navigate(['/login']);
      }
    }
  }
}
