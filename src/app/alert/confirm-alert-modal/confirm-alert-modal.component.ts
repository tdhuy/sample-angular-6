import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-confirm-alert-modal',
  templateUrl: './confirm-alert-modal.component.html',
  styleUrls: ['./confirm-alert-modal.component.css']
})
export class ConfirmAlertModalComponent implements OnInit {

  msgs: String[] = [];


  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

  confirm() {
    this.bsModalRef.content.callback();
  }

}
