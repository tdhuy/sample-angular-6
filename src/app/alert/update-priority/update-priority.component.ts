import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { PageBaseComponent } from '../../shared/components/base/page-base.component';

@Component({
  selector: 'app-modal-update-priority',
  templateUrl: './update-priority.component.html'
})

export class UpdatePriorityComponent implements OnInit {
  priority = null;
  type = '';
  currentValue = 0;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
    setTimeout(() => {
      if (this.type === 'MINDAY') {
        this.currentValue = this.priority.minDay;
      }
      if (this.type === 'COSTBYDAY') {
        this.currentValue = this.priority.costByDay;
      }
    }, 500);
  }

  confirm() {
    this.bsModalRef.content.callback(this.currentValue);
    this.bsModalRef.hide();
  }
}
