import { Component, OnInit } from "@angular/core";
import { IFixedOption } from "../../shared/components/table-control/iTableColumn";
import { BsModalRef } from "ngx-bootstrap";
import { ISelectTextItem } from "../../shared/components/select-text/i-select-text-item";

@Component({
  selector: 'app-update-user-type-modal',
  templateUrl: './update-user-type.component.html'
})

export class UpdateUserTypeComponent implements OnInit {
  user: any;
  type: any;

  types: ISelectTextItem[] = [
    {
      id: '1',
      text: 'Công ty'
    },
    {
      id: '2',
      text: 'Cá nhân'
    }
  ];

  selectedType = null;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
    setTimeout(() => {
      this.types.forEach(t => {
        if (t.id.toString() === this.type.toString()) {
          this.selectedType = t;
        }
      });
    }, 200);
  }

  onSelectType(value: IFixedOption) {
    this.selectedType = value;
  }

  confirm() {
    this.bsModalRef.content.callback(this.selectedType);
  }
}
