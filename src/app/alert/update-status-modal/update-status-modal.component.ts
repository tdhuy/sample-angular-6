import { AfterViewInit, Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { IFixedOption } from '../../shared/components/table-control/iTableColumn';

@Component({
  selector: 'app-update-status-modal',
  templateUrl: './update-status-modal.component.html'
})
export class UpdateStatusModalComponent implements OnInit {
  status: IFixedOption[] = [];
  selectedItem: IFixedOption;

  constructor(public bsModalRef: BsModalRef) {

  }

  ngOnInit() {
  }

  confirm() {
    this.bsModalRef.content.callback(this.selectedItem);
  }

  onSelectedStatus(value: IFixedOption) {
    this.selectedItem = value;
  }
}
