import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-modal-change-usr-balance',
  templateUrl: 'change-usr-expiration-date.component.html'
})

export class ChangeUsrExpirationDateComponent implements OnInit {
  user = null;
  currentValue: Date = new Date();

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
    setTimeout(() => {
        this.currentValue = new Date(this.user.expirationDate);
    }, 500);
  }

  confirm() {
    this.bsModalRef.content.callback(this.currentValue.getTime());
    this.bsModalRef.hide();
  }
}
