import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-reset-own-password-modal',
  templateUrl: './reset-own-password-modal.component.html',
  providers: []
})

export class ResetOwnPasswordModalComponent {
  oldPassword = '';
  newPassword = '';

  errMsg = [];

  constructor(public bsModalRef: BsModalRef) {

  }

  confirm() {
    this.bsModalRef.content.callback(this.oldPassword, this.newPassword);
  }
}
