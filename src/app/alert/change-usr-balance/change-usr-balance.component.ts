import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { PageBaseComponent } from '../../shared/components/base/page-base.component';

@Component({
  selector: 'app-modal-change-usr-balance',
  templateUrl: './change-usr-balance.component.html'
})

export class ChangeUsrBalanceComponent implements OnInit {
  type = '';
  user = null;
  currentValue = 0;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
    setTimeout(() => {
      if (this.type !== '') {
        this.currentValue = this.user.balance[this.type.toLowerCase()];
      }
    }, 500);
  }

  confirm() {
    this.bsModalRef.content.callback(this.currentValue);
    this.bsModalRef.hide();
  }
}
