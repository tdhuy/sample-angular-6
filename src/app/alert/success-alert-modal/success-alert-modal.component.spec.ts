import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessAlertModalComponent } from './success-alert-modal.component';

describe('SuccessAlertModalComponent', () => {
  let component: SuccessAlertModalComponent;
  let fixture: ComponentFixture<SuccessAlertModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessAlertModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessAlertModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
