import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-success-alert-modal',
  templateUrl: './success-alert-modal.component.html',
  styleUrls: ['./success-alert-modal.component.css']
})

export class SuccessAlertModalComponent implements OnInit {

  msgs: String[] = [];

  constructor(public bsModalRef: BsModalRef) {

  }

  ngOnInit() {
  }

}
