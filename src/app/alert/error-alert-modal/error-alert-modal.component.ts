import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-error-alert-modal',
  templateUrl: './error-alert-modal.component.html',
  styleUrls: ['./error-alert-modal.component.css']
})
export class ErrorAlertModalComponent implements OnInit {

  msgs: String[] = [];

  constructor(
    public bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
  }

}
