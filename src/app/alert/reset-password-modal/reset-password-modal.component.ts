import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-reset-password-modal',
  templateUrl: './reset-password-modal.component.html',
  providers: []
})

export class ResetPasswordModalComponent {
  password = '';
  confirmed = '';
  errMsg = '';

  constructor(public bsModalRef: BsModalRef) {

  }

  confirm() {
    if (this.password === this.confirmed) {
      this.bsModalRef.content.callback(this.password);
    } else {
      this.errMsg = '2 passwords are not same';
    }
  }
}
