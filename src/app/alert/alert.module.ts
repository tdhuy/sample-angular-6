import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CheckBoxGroupModule } from '../shared/components/check-box-group/check-box-group.module';
import { InputDateModule } from '../shared/components/input-date/input-date.module';
import { InputNumberModule } from '../shared/components/input-number/input-number.module';
import { InputPasswordModule } from '../shared/components/input-password/input-password.module';
import { InputTextModule } from '../shared/components/input-text/input-text.module';
import { SelectSearchModule } from '../shared/components/select-search/select-search.module';
import { AlertService } from '../shared/services/alert.service';
import { UserService } from '../shared/services/user/user.service';
import { ChangeUsrBalanceComponent } from './change-usr-balance/change-usr-balance.component';
import { ChangeUsrExpirationDateComponent } from './change-usr-expiration-date/change-usr-expiration-date.component';
import { ConfirmAlertModalComponent } from './confirm-alert-modal/confirm-alert-modal.component';
import { ErrorAlertModalComponent } from './error-alert-modal/error-alert-modal.component';
import { ResetOwnPasswordModalComponent } from './reset-own-password-modal/reset-own-password-modal.component';
import { ResetPasswordModalComponent } from './reset-password-modal/reset-password-modal.component';
import { SuccessAlertModalComponent } from './success-alert-modal/success-alert-modal.component';
import { UpdatePriorityComponent } from './update-priority/update-priority.component';
import { UpdateStatusModalComponent } from './update-status-modal/update-status-modal.component';
import { UpdateUserTypeComponent } from './update-user-type/update-user-type.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SelectSearchModule,
    CheckBoxGroupModule,
    InputTextModule,
    InputPasswordModule,
    InputNumberModule,
    InputDateModule,
  ],
  declarations: [
    ConfirmAlertModalComponent,
    SuccessAlertModalComponent,
    ResetPasswordModalComponent,
    ResetOwnPasswordModalComponent,
    ErrorAlertModalComponent,
    ChangeUsrBalanceComponent,
    UpdateStatusModalComponent,
    UpdatePriorityComponent,
    UpdateUserTypeComponent,
    ChangeUsrExpirationDateComponent,
  ],
  entryComponents: [
    ConfirmAlertModalComponent,
    SuccessAlertModalComponent,
    ResetPasswordModalComponent,
    ResetOwnPasswordModalComponent,
    ErrorAlertModalComponent,
    ChangeUsrBalanceComponent,
    UpdateStatusModalComponent,
    UpdatePriorityComponent,
    UpdateUserTypeComponent,
    ChangeUsrExpirationDateComponent,
  ],
  providers: [
    UserService,
    AlertService
  ]
})

export class AlertModule {}
