import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { AlertService } from '../../shared/services/alert.service';
import { GlobalService } from '../../shared/services/global.service';
import { AuthService } from '../../shared/services/auth.service';
import { SessionService } from '../../shared/services/session.service';
import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie';
import { AppConfig, IAppConfig } from '../../app.config';
import CookieNames from '../../shared/constants/cookies';
import { environment } from '../../../environments/environment';

declare let $: any;

@Component({
  selector: 'app-layout-header',
  templateUrl: './layout-header.component.html',
  styleUrls: ['./layout-header.component.scss'],
  providers: [
    AlertService
  ]
})
export class LayoutHeaderComponent implements OnInit {
  appConfig: IAppConfig = AppConfig;
  loggedInUser: {
    id: any,
    username: string,
    email: string
  } = null;
  $: any;
  env = environment;
  isOpened = false;

  constructor(@Inject(DOCUMENT) private  document: Document,
              public globalService: GlobalService,
              private authService: AuthService,
              private alertService: AlertService,
              private router: Router,
              private sessionService: SessionService,
              private cookieService: CookieService) {
    this.loggedInUser = this.globalService.loggedInUser;
    this.$ = $;
  }

  toggleCollapseAside(): void {
    // toogle sidebar collapse
    this.globalService.sidebarCollapsed = !this.globalService.sidebarCollapsed;

    // save to cookie
    const today = new Date();
    today.setDate(today.getDate() + 30);
    this.cookieService.put(CookieNames.collapsed, this.globalService.sidebarCollapsed.toString(), {expires: today});

    // update body class
    if (this.globalService.sidebarCollapsed) {
      this.document.body.classList.add('page-sidebar-closed');
      this.document.getElementsByClassName('page-sidebar-menu')[0].classList.add('page-sidebar-menu-closed');
    } else {
      this.document.body.classList.remove('page-sidebar-closed');
      this.document.getElementsByClassName('page-sidebar-menu')[0].classList.remove('page-sidebar-menu-closed');
    }
  }

  logout() {
    this.globalService.setFullPageLoadingLogin(true);

    this.authService.logout()
      .then(
        () => {
          this.sessionService.remove();
          this.globalService.loggedInUser = null;
          this.router.navigate(['/login']);
          this.globalService.setFullPageLoadingLogin(false);
        }
      );
  }

  ngOnInit() {

  }

  closeFileManager() {
    this.isOpened = false;
    this.globalService.toggleBodyScroll(false);
  }

  openFilemanager() {
    this.isOpened = true;
    this.globalService.toggleBodyScroll(true);
  }
}
