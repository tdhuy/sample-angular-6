import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BsModalService, ModalModule } from 'ngx-bootstrap';
import { AlertModule } from '../alert/alert.module';
import { UserService } from '../shared/services/user/user.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LayoutAsideComponent } from './layout-aside/layout-aside.component';
import { LayoutHeaderComponent } from './layout-header/layout-header.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';

@NgModule({
  declarations: [
    DashboardComponent,
    LayoutComponent,
    LayoutHeaderComponent,
    LayoutAsideComponent
  ],
  imports: [
    LayoutRoutingModule,
    CommonModule,
    FormsModule,
    ModalModule.forRoot()
  ],
  exports: [],
  providers: [
    BsModalService,
    UserService
  ],
})

export class LayoutModule { }
