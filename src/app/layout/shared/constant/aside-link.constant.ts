export interface AsideLinkItem {
  title: string;
  icon: string;
  routerLink: string[];
  isVisible: boolean;
  targetType?: TargetTypes;
}

export enum TargetTypes {
  Dashboard,
  Area,
  Category,
  Brand,
  Mall,
  Store,
  Deal,
  Campaign,
  Collection,
  Comment,
  Voucher,
  Banner,
  CMSUser,
  User
}

export const RoleToNavs = {
  'ROLE_SUPER_ADMIN': [], // show all
  'ROLE_USER': [
    TargetTypes.Dashboard
  ], // ko config
  'ROLE_SALE_ADMIN': [
    TargetTypes.Dashboard,
    TargetTypes.Deal,
    TargetTypes.Voucher
  ],
  'ROLE_BD': [
    TargetTypes.Dashboard
  ],
  'ROLE_CONTENT_MANAGER': [
    TargetTypes.Dashboard,
    TargetTypes.Area,
    TargetTypes.Category,
    TargetTypes.Store,
    TargetTypes.Brand,
    TargetTypes.Deal,
    TargetTypes.Campaign,
    TargetTypes.Collection,
    TargetTypes.Voucher,
    TargetTypes.Banner
  ],
  'ROLE_CONTENT': [
    TargetTypes.Dashboard,
    TargetTypes.Area,
    TargetTypes.Category,
    TargetTypes.Store,
    TargetTypes.Brand,
    TargetTypes.Deal,
    TargetTypes.Campaign,
    TargetTypes.Collection
  ],
  'ROLE_OPERATOR': [
    TargetTypes.Dashboard,
    TargetTypes.Area,
    TargetTypes.Category,
    TargetTypes.Store,
    TargetTypes.Brand,
    TargetTypes.Deal,
    TargetTypes.Campaign,
    TargetTypes.Collection,
    TargetTypes.Voucher,
    TargetTypes.Banner
  ],
  'ROLE_MARKETING_MANAGER': [
    TargetTypes.Dashboard
  ],
  'ROLE_MARKETING': [
    TargetTypes.Dashboard
  ],
  'ROLE_CUSTOMER_SERVICE': [
    TargetTypes.Dashboard,
    TargetTypes.Store,
    TargetTypes.Brand,
    TargetTypes.Deal,
    TargetTypes.Voucher,
    TargetTypes.Comment
  ]
};

export namespace AsideLinks {
  export const List: AsideLinkItem[] = [
    {
      title: 'Area',
      icon: 'icon-map',
      routerLink: ['/area'],
      isVisible: true,
      targetType: TargetTypes.Area
    },
    {
      title: 'Category',
      icon: 'icon-list',
      routerLink: ['/category'],
      isVisible: true,
      targetType: TargetTypes.Category
    },
    {
      title: 'Brand',
      icon: 'icon-globe',
      routerLink: ['/brand'],
      isVisible: true,
      targetType: TargetTypes.Brand
    },
    {
      title: 'Mall',
      icon: 'icon-basket-loaded',
      routerLink: ['/mall'],
      isVisible: true,
      targetType: TargetTypes.Mall
    },
    {
      title: 'Store',
      icon: 'icon-basket',
      routerLink: ['/store'],
      isVisible: true,
      targetType: TargetTypes.Store
    },
    {
      title: 'Campaign',
      icon: 'icon-briefcase',
      routerLink: ['/campaign'],
      isVisible: true,
      targetType: TargetTypes.Campaign
    },
    {
      title: 'Collection',
      icon: 'icon-book-open',
      routerLink: ['/collection'],
      isVisible: true,
      targetType: TargetTypes.Collection
    },
    {
      title: 'Deal',
      icon: 'icon-handbag',
      routerLink: ['/deal'],
      isVisible: true,
      targetType: TargetTypes.Deal
    },
    {
      title: 'Voucher',
      icon: 'icon-present',
      routerLink: ['/voucher'],
      isVisible: true,
      targetType: TargetTypes.Voucher
    },
    {
      title: 'Comment',
      icon: 'icon-bubbles',
      routerLink: ['/comment'],
      isVisible: true,
      targetType: TargetTypes.Comment
    },
    {
      title: 'Banner',
      icon: 'icon-picture',
      routerLink: ['/banner'],
      isVisible: true,
      targetType: TargetTypes.Banner
    },
    {
      title: 'CMS User',
      icon: 'icon-user',
      routerLink: ['/cms-user'],
      isVisible: true,
      targetType: TargetTypes.CMSUser
    },
    {
      title: 'User',
      icon: 'icon-user',
      routerLink: ['/user'],
      isVisible: true,
      targetType: TargetTypes.User
    }
  ];
}
