import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { APIs } from "../../../shared/constants/api";

@Injectable()
export class PostSaleService {
  constructor(private httpClient: HttpClient) {}

  list(params: any): Observable<any> {
    return this.httpClient.get(APIs.post.list, {params});
  }

  // create(data: any): Observable<any> {
  //   return this.httpClient.post(APIs.postSale.add, data);
  // }

  softDelete(id: string): Observable<any> {
    const url = APIs.post.updateSale.replace('{id}', id);
    return this.httpClient.post(url, {status: 0});
  }

  updateInfo(id: string, data: any): Observable<any> {
    const url = APIs.post.updateSale.replace('{id}', id);
    return this.httpClient.post(url, data);
  }

  // getDetail(url: string): Observable<any> {
  //   return this.httpClient.get(APIs.search, {params: {url}});
  // }
}