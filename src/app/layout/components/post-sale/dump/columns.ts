import { CellDisplay, ITableColumn } from '../../../../shared/components/table-control/iTableColumn';

export const columns: ITableColumn[] = [
  {
    headerName: 'Hình ảnh',
    displayValuePath: '_imageAvatar',
    displayValueAs: CellDisplay.IMAGE,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Tiêu đề',
    displayValuePath: 'title',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false,
    contentStyle: {
      'max-width': '250px'
    }
  },
  {
    headerName: 'Hình thức',
    displayValuePath: 'formality',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Loại',
    displayValuePath: 'type',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Địa chỉ',
    displayValuePath: 'address',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Giá',
    displayValuePath: 'price',
    displayValueAs: CellDisplay.NUMBER,
    filterable: false,
    sortable: false,
    contentStyle: {
      'text-align': 'right'
    }
  },
  {
    headerName: 'Diện tích',
    displayValuePath: 'area',
    displayValueAs: CellDisplay.NUMBER,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Hướng ban công',
    displayValuePath: 'balconyDirection',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Số phòng ngủ',
    displayValuePath: 'bedroomCount',
    displayValueAs: CellDisplay.NUMBER,
    filterable: false,
    sortable: false,
    contentStyle: {
      'text-align': 'right'
    }
  },
  {
    headerName: 'DS keyword',
    displayValuePath: 'keywordList',
    displayValueAs: CellDisplay.LIST_KEYWORD,
    filterable: false,
    sortable: false,
    contentStyle: {
      'max-width': '250px'
    }
  },
  {
    headerName: 'Status',
    displayValuePath: 'status',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  }
];