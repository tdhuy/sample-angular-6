import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { PostSaleComponent } from "./post-sale.component";
import { AddEditPostSaleComponent } from "./add-edit-post-sale/add-edit-post-sale.component";
import { UpdateSeoInfoComponent } from "./update-seo-info/update-seo-info.component";

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: PostSaleComponent
  }, {
    path: 'add',
    component: AddEditPostSaleComponent
  }, {
    path: 'modify',
    component: AddEditPostSaleComponent
  }, {
    path: 'update-seo-info',
    component: UpdateSeoInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PostSaleRoutingModule {

}