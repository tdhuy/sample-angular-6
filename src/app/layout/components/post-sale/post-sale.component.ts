import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PageBaseComponent } from '../../../shared/components/base/page-base.component';
import { Component, ViewChild } from '@angular/core';
import { IRequestParams } from '../../../shared/components/table-control/iRequestParams';
import { TableControlComponent } from '../../../shared/components/table-control/table-control.component';
import { columns } from './dump/columns';
import { PostSaleService } from './post-sale.service';
import { postSaleStatus, getStatusName } from '../../../shared/constants/status';
import { IFixedOption } from '../../../shared/components/table-control/iTableColumn';

@Component({
  selector: 'app-post-sale-list',
  templateUrl: './post-sale.component.html'
})

export class PostSaleComponent extends PageBaseComponent {
  @ViewChild('tableControl') tableControl: TableControlComponent;

  grid = {
    itemsSource: [],
    selectedItems: [],
    totalItems: 0
  };

  columns = columns;
  selectedRow = -1;
  private _requestParams: IRequestParams;

  constructor(
    private postSaleService: PostSaleService) {
    super();

    this.columns.forEach((col, index) => {
      this.columns[index].code = this.stringHelperService.genRandomString();
    });
  }

  _fetchData(requestParams: IRequestParams) {
    this.selectedRow = -1;
    this._settingDisable();
    this._requestParams = requestParams;
    this._loadData();
  }

  receiveAction(data: { type: string, item: any }) {
    switch (data.type) {
      case 'REMOVE':
        this.removeItem(data.item);
        break;
      case 'UPDATE_STATUS':
        this.updateStatus(data.item);
        break;
    }
  }

  updateStatus(post: any) {
    this.alertService.updateStatus(postSaleStatus, post.status || '', (status) => {
      this._sendRequestUpdateStatus(post, status);
    });
  }

  _sendRequestUpdateStatus(post: any, status: IFixedOption) {
    // const deal = this._getDealById(this.selectedRow)[0];
    this.globalService.setFullPageLoading(true);
    const sub = this.postSaleService.updateInfo(post.id, {
      status: status.id
    }).subscribe(res => {
      if (res.status === 1) {
        this.selectedRow = -1;
        this._loadData();
        this._settingDisable();
      }
      this.globalService.setFullPageLoading(false);
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.globalService.setFullPageLoading(false);
    });

    this.subscriptions.push(sub);
  }

  private removeItem(item: any) {
    this.alertService.confirm(['Có chắc muốn xoá tin đăng này?'], () => {
      this.globalService.setFullPageLoading(true);

      this.postSaleService.softDelete(item.id).subscribe(res => {
        this.globalService.setFullPageLoading(false);
        if (res.status === 1) {
          this.alertService.showSuccess([`Xoá dự án ${item.title} thành công`]);
          this._loadData();
        } else {
          this.alertService.error([`${res.message}`]);
        }
      });
    });
  }

  private _settingDisable() { }

  private _loadData() {
    const options: any = {
      size: this._requestParams.size,
      page: this._requestParams.page,
      postType: 1
    };

    const req: Observable<any> = this.postSaleService.list(options);

    const sub = req.subscribe((res) => {
      if (res.status === 1) {
        if (res.data) {
          this.mapDataFromList(res.data.items);
          this.grid.totalItems = res.data.total;
          this.tableControl.onRenderDone();
        }
      } else {
        this.grid.itemsSource = [];
        this.grid.totalItems = 0;
        this.tableControl.onRenderDone();
      }
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.grid.itemsSource = [];
      this.grid.totalItems = 0;
      this.tableControl.onRenderDone();
    });

    this.subscriptions.push(sub);
  }

  private mapDataFromList(items: any[]) {
    this.grid.itemsSource = items.map(item => {
      item._imageAvatar = '';
      if (item.images && item.images.length && item.images.length > 0) {
        item._imageAvatar = this.stringHelperService.appendHostGetImage(item.images[0]);
      }

      const _objCity = this.stringHelperService.getCityByCode(item.city);
      if (_objCity) {
        item.city = _objCity.name;
      }

      const _objFormality = this.stringHelperService.getFormilitySaleByValue(item.formality);
      if (_objFormality) {
        item.formality = _objFormality.name;

        const _objType = this.stringHelperService.getTypeByValue(_objFormality, item.type);
        if (_objType) {
          item.type = _objType.name;
        }
      }

      // hướng ban công
      const _objBalconyDirection = this.stringHelperService.getDirectionsByValue(item.balconyDirection);
      if (_objBalconyDirection) {
        item.balconyDirection = _objBalconyDirection.text;
      }

      // hướng nhà
      const _objHouseDirection = this.stringHelperService.getDirectionsByValue(item.direction);
      if (_objHouseDirection) {
        item.direction = _objHouseDirection.text;
      }

      // status: from value to name
      item.status = getStatusName(item.status);

      return item;
    });

    console.log(this.grid.itemsSource);
  }
}
