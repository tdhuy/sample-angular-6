import { Component, AfterContentInit } from "@angular/core";
import { EditableFormBaseComponent } from "src/app/shared/components/base/editable-form-base.component";

@Component({
  selector: 'add-edit-post-sale',
  templateUrl: './add-edit-post-sale.component.html'
})

export class AddEditPostSaleComponent extends EditableFormBaseComponent implements AfterContentInit {
  post() {

  }

  ngAfterContentInit() {
    this.initForm();
  }

  private initForm() {
    this.form = this.fb.group({
      title: ['', []],
      formality: [null, []],
      city: [null, []],
      district: [null, []],
      ward: [null, []],
      street: [null, []],
      project: [null, []],
      area: [0, []],
      price: [0, []],
      keyworkList: [[], []],
      unit: [null, []],
      address:['', []],
      googleAddress: ['', []],
      description: ['', []],
      streetWidth: [0, []],
      frontSize: [0, []],
      direction: [null, []],
      balconyDirection: [null, []],
      floorCount: [0, []],
      bedroomCount: [0, []],
      toiletCount: [0, []],
      furniture: ['', []],
      images: [[], []],
      contactName: ['', []],
      contactAddress: ['', []],
      contactPhone: ['', []],
      contactMobile: ['', []],
      contactEmail: ['', []],
      priority: [0, []],
      from: [new Date(), []],
      to: [new Date(), []]
    });
  }
}