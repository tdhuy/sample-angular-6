import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PostSaleComponent } from "./post-sale.component";
import { PostSaleService } from "./post-sale.service";
import { AddEditPostSaleComponent } from "./add-edit-post-sale/add-edit-post-sale.component";
import { PostSaleRoutingModule } from "./post-sale-routing.module";
import { TableControlModule } from "src/app/shared/components/table-control/table-control.module";
import { InputTextModule } from "src/app/shared/components/input-text/input-text.module";
import { SelectSearchModule } from "src/app/shared/components/select-search/select-search.module";
import { SelectMapLocationModule } from "src/app/shared/components/select-map-location/select-map-location.module";
import { ReactiveFormsModule } from "@angular/forms";
import { StringHelperService } from "src/app/shared/services/string-helper.service";
import { TabsModule } from "ngx-bootstrap/tabs/tabs.module";
import { UpdateSeoInfoComponent } from "./update-seo-info/update-seo-info.component";

@NgModule({
  imports: [
    CommonModule,
    TabsModule.forRoot(),
    ReactiveFormsModule,
    PostSaleRoutingModule,
    TableControlModule,
    InputTextModule,
    SelectSearchModule,
    SelectMapLocationModule
  ],
  declarations: [
    PostSaleComponent,
    AddEditPostSaleComponent,
    UpdateSeoInfoComponent
  ],
  exports: [
    PostSaleComponent
  ],
  providers: [
    PostSaleService,
    StringHelperService
  ]
})

export class PostSaleModule {

}