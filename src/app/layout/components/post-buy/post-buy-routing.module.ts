import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { PostBuyComponent } from "./post-buy.component";
import { UpdateSeoInfoComponent } from "./update-seo-info/update-seo-info.component";

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: PostBuyComponent
  },
  {
    path: 'update-seo-info',
    component: UpdateSeoInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PostBuyRoutingModule {

}