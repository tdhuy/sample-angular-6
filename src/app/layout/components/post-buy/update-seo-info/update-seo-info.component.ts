import { Component, OnInit } from "@angular/core";
import { EditableFormBaseComponent } from "../../../../shared/components/base/editable-form-base.component";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { PostService } from "../../../../shared/services/post.service";

@Component({
  selector: 'app-update-seo-post-buy-page',
  templateUrl: './update-seo-info.component.html',
  providers: []
})

export class UpdateSeoInfoComponent extends EditableFormBaseComponent implements OnInit {
  form: FormGroup;
  postId = null;
  postInfo = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private postService: PostService,
    private router: Router
  ) {
    super();

    this.activatedRoute.queryParams.subscribe(param => {
      this.postId = param.id;

      if (this.postId) {
        this.getDetailPost();
      }
    })
  }

  ngOnInit() {
    this.initForm();
  }

  post(name?: string) {
    const params = this.form.value;

    this.postService.updateSeoInfo(this.postId, params)
      .subscribe((res: any) => {
        if (res.status === 1) {
          this.alertService.showSuccess(['Cập nhật thông tin SEO thành công']);
          this.router.navigate(['/post-buy']);
        } else {
          this.alertService.error([res.error]);
        }
      });
  }

  onClickBtnSubmit() {
    if (this.postInfo) {
      this.onSubmit();
    }
  }

  onClickBtnClear() {
    this.form.reset();
  }

  private initForm() {
    this.form = this.fb.group({
      url: ['', [
        this.validatorService.checkUrlSeo
      ]],
      metaTitle: ['', []],
      metaDescription: ['', []],
      metaType: ['', []],
      metaUrl: ['', []],
      metaImage: ['', []],
      canonical: ['', []],
      textEndPage: ['', []]
    });
  }

  private getDetailPost() {
    this.postService.getDetail(this.postId.toString())
      .subscribe((res: any) => {
        if (res.status === 1) {
          this.postInfo = res.data;

          this.form.setValue({
            url: this.postInfo.url || '',
            metaTitle: this.postInfo.metaTitle || '',
            metaDescription: this.postInfo.metaDescription || '',
            metaType: this.postInfo.metaType || '',
            metaUrl: this.postInfo.metaUrl || '',
            metaImage: this.postInfo.metaImage || '',
            canonical: this.postInfo.canonical || '',
            textEndPage: this.postInfo.textEndPage || ''
          })
        } else {
          this.alertService.error([res.message]);
        }
      });
  }
}
