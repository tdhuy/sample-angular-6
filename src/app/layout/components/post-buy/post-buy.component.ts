import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PageBaseComponent } from '../../../shared/components/base/page-base.component';
import { Component, ViewChild } from '@angular/core';
import { IRequestParams } from '../../../shared/components/table-control/iRequestParams';
import { TableControlComponent } from '../../../shared/components/table-control/table-control.component';
import { APIs } from '../../../shared/constants/api';
import { AlertService } from '../../../shared/services/alert.service';
import { GlobalService } from '../../../shared/services/global.service';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { columns } from './dump/columns';
import { PostBuyService } from './post-buy.service';
import { PostSaleService } from '../post-sale/post-sale.service';
import { postSaleStatus, getStatusName } from 'src/app/shared/constants/status';
import { IFixedOption } from 'src/app/shared/components/table-control/iTableColumn';

@Component({
  selector: 'app-post-buy-list',
  templateUrl: './post-buy.component.html',
  providers: [PostBuyService]
})

export class PostBuyComponent extends PageBaseComponent {
  @ViewChild('tableControl') tableControl: TableControlComponent;

  grid = {
    itemsSource: [],
    selectedItems: [],
    totalItems: 0
  };

  columns = columns;
  selectedRow = -1;
  private _requestParams: IRequestParams;

  constructor(
    private postBuyService: PostBuyService) {
    super();

    this.columns.forEach((col, index) => {
      this.columns[index].code = this.stringHelperService.genRandomString();
    });
  }

  _fetchData(requestParams: IRequestParams) {
    this.selectedRow = -1;
    this._settingDisable();
    this._requestParams = requestParams;
    this._loadData();
  }

  receiveAction(data: { type: string, item: any }) {
    switch (data.type) {
      case 'REMOVE':
        this.removeItem(data.item);
        break;
      case 'UPDATE_STATUS':
        this.updateStatus(data.item);
        break;
    }
  }

  private removeItem(item: any) {
    this.alertService.confirm(['Có chắc muốn xoá dự án này?'], () => {
      this.globalService.setFullPageLoading(true);

      this.postBuyService.softDelete(item.id).subscribe(res => {
        this.globalService.setFullPageLoading(false);
        if (res.status === 1) {
          this.alertService.showSuccess([`Xoá dự án ${item.title} thành công`]);
          this._loadData();
        } else {
          this.alertService.error([`${res.message}`]);
        }
      });
    });
  }

  updateStatus(post: any) {
    this.alertService.updateStatus(postSaleStatus, post.status || '', (status) => {
      this._sendRequestUpdateStatus(post, status);
    });
  }

  _sendRequestUpdateStatus(post: any, status: IFixedOption) {
    // const deal = this._getDealById(this.selectedRow)[0];
    this.globalService.setFullPageLoading(true);
    const sub = this.postBuyService.updateInfo(post.id, {
      status: status.id
    }).subscribe(res => {
      if (res.status === 1) {
        this.selectedRow = -1;
        this._loadData();
        this._settingDisable();
      }
      this.globalService.setFullPageLoading(false);
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.globalService.setFullPageLoading(false);
    });

    this.subscriptions.push(sub);
  }

  private _settingDisable() { }

  private _loadData() {
    const options: any = {
      size: this._requestParams.size,
      page: this._requestParams.page,
      postType: 2
    };

    const req: Observable<any> = this.postBuyService.list(options);

    const sub = req.subscribe((res) => {
      if (res.status === 1) {
        if (res.data) {
          this.mapDataFromList(res.data.items);
          this.grid.totalItems = res.data.total;
          this.tableControl.onRenderDone();
        }
      } else {
        this.grid.itemsSource = [];
        this.grid.totalItems = 0;
        this.tableControl.onRenderDone();
      }
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.grid.itemsSource = [];
      this.grid.totalItems = 0;
      this.tableControl.onRenderDone();
    });

    this.subscriptions.push(sub);
  }

  private mapDataFromList(items: any[]) {
    this.grid.itemsSource = items.map(item => {
      item._imageAvatar = '';
      if (item.images.length > 0) {
        item._imageAvatar = this.stringHelperService.appendHostGetImage(item.images[0]);
      }

      const _objCity = this.stringHelperService.getCityByCode(item.city);
      if (_objCity) {
        item.city = _objCity.name;
      }

      const _objFormality = this.stringHelperService.getFormilityBuyByValue(item.formality);
      if (_objFormality) {
        item.formality = _objFormality.name;

        const _objType = this.stringHelperService.getTypeByValue(_objFormality, item.type);
        if (_objType) {
          item.type = _objType.name;
        }
      }

      // status: from value to name
      item.status = getStatusName(item.status);

      return item;
    });
  }
}
