import { ITableColumn, CellDisplay, FilterType } from "../../../../shared/components/table-control/iTableColumn";

export const columns: ITableColumn[] = [
  {
    headerName: 'id',
    displayValuePath: 'id',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Url',
    displayValuePath: 'url',
    displayValueAs: CellDisplay.STRING,
    filterable: true,
    sortable: false,
    filterConfig: {
      queryName: 'url',
      dataType: FilterType.STRING
    }
  },
  {
    headerName: 'Meta title',
    displayValuePath: 'metaTitle',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  }
];