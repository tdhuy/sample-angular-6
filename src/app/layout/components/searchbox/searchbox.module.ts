import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { SearchBoxService } from "./searchbox.service";
import { SearchBoxComponent } from "./searchbox.component";
import { SearchBoxRoutingModule } from "./searchbox-routing.module";
import { TableControlModule } from "../../../shared/components/table-control/table-control.module";
import { StringHelperService } from "../../../shared/services/string-helper.service";
import { AlertService } from "../../../shared/services/alert.service";
import { UpdateSeoInfoComponent } from "./update-seo-info/update-seo-info.component";
import { InputTextModule } from "../../../shared/components/input-text/input-text.module";

@NgModule({
  declarations: [
    SearchBoxComponent,
    UpdateSeoInfoComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SearchBoxRoutingModule,
    TableControlModule,
    InputTextModule
  ],
  providers: [
    SearchBoxService,
    StringHelperService,
    AlertService
  ]
})

export class SearchBoxModule {}