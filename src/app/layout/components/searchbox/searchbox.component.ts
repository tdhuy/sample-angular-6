import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PageBaseComponent } from '../../../shared/components/base/page-base.component';
import { IRequestParams } from '../../../shared/components/table-control/iRequestParams';
import { TableControlComponent } from '../../../shared/components/table-control/table-control.component';
import { AlertService } from '../../../shared/services/alert.service';
import { GlobalService } from '../../../shared/services/global.service';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { columns } from './dump/columns';
import { SearchBoxService } from './searchbox.service';

@Component({
  selector: 'app-searchbox-list-page',
  templateUrl: './searchbox.component.html'
})

export class SearchBoxComponent extends PageBaseComponent {
  @ViewChild('tableControl') tableControl: TableControlComponent;

  grid = {
    itemsSource: [],
    selectedItems: [],
    totalItems: 0
  };

  columns = columns;
  selectedRow = -1;
  private _requestParams: IRequestParams;

  constructor(private searchboxService: SearchBoxService) {
    super();

    this.columns.forEach((col, index) => {
      this.columns[index].code = this.stringHelperService.genRandomString();
    });
  }

  _fetchData(requestParams: IRequestParams) {
    this.selectedRow = -1;
    this._settingDisable();
    this._requestParams = requestParams;
    this._loadData();
  }

  receiveAction(data: { type: string, item: any, extra?: any }) {
    switch (data.type) {
      case 'UPDATE_STATUS':
        this.updateStatus(data.item);
        break;
    }
  }

  private updateStatus(item: any) {
    this.alertService.confirm(['Bạn chắc muốn cập nhật status searchbox này?'], () => {
      this.globalService.setFullPageLoading(true);

      this.searchboxService.updateInfo(item.id, item)
        .subscribe((res: any) => {
          this.globalService.setFullPageLoading(false);

          if (res.status === 1) {
            this.alertService.showSuccess([`Cập nhật ${item.name} thành công`]);
            this._loadData();
          } else {
            this.alertService.error([`${res.message}`]);
          }
        });
    });
  }

  private _settingDisable() { }

  private _loadData() {
    const options: any = {
      limit: this._requestParams.size,
      page: this._requestParams.page
    };

    if (this._requestParams.filter) {
      Object.assign(options, this._requestParams.filter);
    }

    const req: Observable<any> = this.searchboxService.getList(options);

    const sub = req.subscribe((res) => {
      if (res.status === 1 && res.data.items.length) {
        this.grid.itemsSource = this.mapDataFromList(res.data.items);
        this.grid.totalItems = res.data.total;
      } else {
        this.grid.itemsSource = [];
        this.grid.totalItems = 0;
      }

      this.tableControl.onRenderDone();
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.grid.itemsSource = [];
      this.grid.totalItems = 0;
      this.tableControl.onRenderDone();
    });

    this.subscriptions.push(sub);
  }

  private mapDataFromList(items: any[]) {
    return items.map(item => {

      return item;
    });
  }
}