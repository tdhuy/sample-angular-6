import { Component, OnInit } from "@angular/core";
import { EditableFormBaseComponent } from "../../../../shared/components/base/editable-form-base.component";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { SearchBoxService } from "../searchbox.service";

@Component({
  selector: 'app-update-seo-post-buy-page',
  templateUrl: './update-seo-info.component.html',
  providers: []
})

export class UpdateSeoInfoComponent extends EditableFormBaseComponent implements OnInit {
  form: FormGroup;
  info = null;
  urlSearchBoxId = null;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private searchBoxService: SearchBoxService) {
    super();

    this.activatedRoute.queryParams.subscribe(param => {
      this.urlSearchBoxId = param.id;
    })
  }

  ngOnInit() {
    this.initForm();

    if (this.searchBoxService) {
      this.searchBoxService.getDetail(this.urlSearchBoxId)
        .subscribe(res => {
          if (res.status === 1) {
            this.info = res.data;

            this.form.setValue({
              url: this.info.url || '',
              metaTitle: this.info.metaTitle || '',
              metaDescription: this.info.metaDescription || '',
              metaType: this.info.metaType || '',
              metaUrl: this.info.metaUrl || '',
              metaImage: this.info.metaImage || '',
              canonical: this.info.canonical || '',
              textEndPage: this.info.textEndPage || ''
            });
          } else {
            this.alertService.error(res.message);
          }
        });
    }
  }

  post(name?: string) {
    const params = this.form.value;

    this.searchBoxService.updateInfo(this.urlSearchBoxId, params)
      .subscribe(res => {
        if (res.status === 1) {
          this.router.navigate(['/search-box']);
          this.alertService.showSuccess(['Update thông tin SEO thành công!']);
        } else {
          this.alertService.error(res.message);
        }
      })
  }

  onClickBtnSubmit() {
    if (this.info) {
      this.onSubmit();
    }
  }

  onClickBtnClear() {
    this.form.reset();
  }

  private initForm() {
    this.form = this.fb.group({
      url: ['', []],
      metaTitle: ['', []],
      metaDescription: ['', []],
      metaType: ['', []],
      metaUrl: ['', []],
      metaImage: ['', []],
      canonical: ['', []],
      textEndPage: ['', []]
    });
  }
}
