import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { NewsComponent } from './news.component';
import { UpdateEditNewsComponent } from './update-edit-news/update-edit-news.component';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: NewsComponent
  },
  {
    path: 'add',
    component: UpdateEditNewsComponent
  },
  {
    path: 'modify',
    component: UpdateEditNewsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class NewsRoutingModule {

}
