import { CellDisplay, ITableColumn, FilterType, SelectValuesType } from '../../../../shared/components/table-control/iTableColumn';
import { Observable } from 'rxjs';
import { newsStatus } from '../../../../shared/constants/status';
import { of } from 'rxjs';

export const columns: ITableColumn[] = [
  {
    headerName: 'Tiêu đề',
    displayValuePath: 'title',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  }, {
    headerName: 'Loại',
    displayValueAs: CellDisplay.STRING,
    displayValuePath: 'cate',
    filterable: false,
    sortable: false
  }, {
    headerName: 'Mô tả',
    displayValueAs: CellDisplay.STRING,
    displayValuePath: 'description',
    filterable: false,
    sortable: false
  }, {
    headerName: 'Status',
    displayValueAs: CellDisplay.STRING,
    displayValuePath: 'status',
    filterable: true,
    sortable: false,
    filterConfig: {
      dataType: FilterType.SELECT,
      queryName: 'status',
      dataLoadBy: SelectValuesType.FIXED
    },
    optionsFilter: of(newsStatus)
  }
];
