import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { APIs } from '../../../shared/constants/api';
import { newsStatus } from '../../../shared/constants/status';

@Injectable()
export class NewsService {
  constructor(private httpClient: HttpClient) {}

  getTypes(): Observable<any> {
    return this.httpClient.get(APIs.news.category);
  }

  list(params: any): Observable<any> {
    return this.httpClient.get(APIs.news.list, {params});
  }

  create(data: any): Observable<any> {
    return this.httpClient.post(APIs.news.add, data);
  }

  softDelete(id: string): Observable<any> {
    const url = APIs.news.update.replace('{id}', id);
    return this.httpClient.post(url, {status: 3});
  }

  updateInfo(id: string, data: any): Observable<any> {
    const url = APIs.news.update.replace('{id}', id);
    return this.httpClient.post(url, data);
  }

  getDetail(id: any): Observable<any> {
    const url = APIs.news.detail.replace('{id}', id);
    return this.httpClient.get(url);
  }
}
