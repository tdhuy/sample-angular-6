import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PageBaseComponent } from '../../../shared/components/base/page-base.component';
import { IRequestParams } from '../../../shared/components/table-control/iRequestParams';
import { TableControlComponent } from '../../../shared/components/table-control/table-control.component';
import { AlertService } from '../../../shared/services/alert.service';
import { GlobalService } from '../../../shared/services/global.service';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { columns } from './dump/columns';
import { NewsService } from 'src/app/layout/components/news/news.service';
import { getStatusName } from '../../../shared/constants/status';
import {GlobalConstant} from '../../../shared/constants/globalConstant';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html'
})

export class NewsComponent extends PageBaseComponent {
  @ViewChild('tableControl') tableControl: TableControlComponent;

  grid = {
    itemsSource: [],
    selectedItems: [],
    totalItems: 0
  };

  columns = columns;
  selectedRow = -1;
  private _requestParams: IRequestParams;

  constructor(private newsService: NewsService) {
    super();

    this.columns.forEach((col, index) => {
      this.columns[index].code = this.stringHelperService.genRandomString();
    });
  }

  _fetchData(requestParams: IRequestParams) {
    this.selectedRow = -1;
    this._settingDisable();
    this._requestParams = requestParams;
    this._loadData();
  }

  receiveAction(data: { type: string, item: any }) {
    switch (data.type) {
      case 'REMOVE':
        this.removeItem(data.item);
        break;
    }
  }

  private removeItem(item: any) {
    this.alertService.confirm(['Có chắc muốn xoá bài viết này?'], () => {
      this.globalService.setFullPageLoading(true);

      this.newsService.softDelete(item.id).subscribe(res => {
        this.globalService.setFullPageLoading(false);
        if (res.status === 1) {
          this.alertService.showSuccess([`Xoá bài viết ${item.title} thành công`]);
          this._loadData();
        } else {
          this.alertService.error([`${res.message}`]);
        }
      })
    });
  }

  private _settingDisable() { }

  private _loadData() {
    const options: any = {
      size: this._requestParams.size,
      page: this._requestParams.page,
      postType: GlobalConstant.PostTypeNews
    };

    if (this._requestParams.filter) {
      Object.assign(options, this._requestParams.filter);
    }

    const req: Observable<any> = this.newsService.list(options);

    const sub = req.subscribe((res) => {
      if (res.status === 1) {
        if (res.data) {
          this.grid.itemsSource = this.mapDataFromList(res.data.items);
          this.grid.totalItems = res.data.total;
          this.tableControl.onRenderDone();
        }
      } else {
        this.grid.itemsSource = [];
        this.grid.totalItems = 0;
        this.tableControl.onRenderDone();
      }
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.grid.itemsSource = [];
      this.grid.totalItems = 0;
      this.tableControl.onRenderDone();
    });

    this.subscriptions.push(sub);
  }

  private mapDataFromList(items: any[]) {
    return items.map(item => {
      // status: from value to name
      item.status = getStatusName(item.status);

      return item;
    });
  }
}
