import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageUploaderModule } from '../../../shared/components/image-uploader/imageUploader.module';
import { InputRichText2Module } from '../../../shared/components/input-rich-text-2/input-rich-text-2.module';
import { InputTextModule } from '../../../shared/components/input-text/input-text.module';
import { SelectSearchModule } from '../../../shared/components/select-search/select-search.module';
import { TableControlModule } from '../../../shared/components/table-control/table-control.module';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news.component';
import { UpdateEditNewsComponent } from './update-edit-news/update-edit-news.component';
import { NewsService } from '../../../layout/components/news/news.service';
import { InputTextareaModule } from '../../../shared/components/input-textarea/input-textarea.module';
import { TabsModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [
    NewsComponent,
    UpdateEditNewsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NewsRoutingModule,
    TableControlModule,
    InputTextModule,
    InputRichText2Module,
    InputTextareaModule,
    SelectSearchModule,
    ImageUploaderModule,
    TabsModule.forRoot()
  ],
  providers: [
    StringHelperService,
    NewsService
  ],
  exports: [
    NewsComponent,
    UpdateEditNewsComponent
  ]
})

export class NewsModule {

}
