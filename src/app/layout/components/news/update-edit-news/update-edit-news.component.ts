import { HttpClient } from '@angular/common/http';
import { AfterContentInit, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EditableFormBaseComponent } from '../../../../shared/components/base/editable-form-base.component';
import { APIs } from '../../../../shared/constants/api';
import { GlobalConstant } from '../../../../shared/constants/globalConstant';
import { AlertService } from '../../../../shared/services/alert.service';
import { ValidatorsService } from '../../../../shared/services/validator.service';
import NewsCategories = GlobalConstant.NewsCategories;
import { NewsService } from 'src/app/layout/components/news/news.service';
import { IFileTextValue } from 'src/app/shared/components/image-uploader/imageUploader.component';
import { Observable } from 'rxjs/internal/Observable';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-update-edit-news',
  templateUrl: './update-edit-news.component.html',
  providers: [
    AlertService
  ]
})

export class UpdateEditNewsComponent extends EditableFormBaseComponent implements AfterContentInit {
  categoryItemsSource: any[] = [];
  oldData: any = null;
  onFlowEdit = false;
  params: any = {};

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private newsService: NewsService) {
    super();

    this.activatedRoute.queryParams.subscribe(params => {
      this.params = params;

      if (this.params.id) {
        this.onFlowEdit = true;
      }
    });
  }

  ngAfterContentInit() {
    this.initForm();
    this.getCategory(() => {
      if (this.params.id) {
        this.loadDetailNews();
      }
    });
  }

  onClickBtnSubmit() {
    this.onSubmit();
  }

  onClickBtnClear() {
    this.form.reset();
  }

  post() {
    this.globalService.setFullPageLoading(true);
    const params = this.buildObjectPost();

    let req: Observable<any>;
    if (this.onFlowEdit) {
      req = this.newsService.updateInfo(this.oldData.id, params);
    } else {
      req = this.newsService.create(params);
    }

    req.subscribe((res: any) => {
      this.globalService.setFullPageLoading(false);

      if (res.status !== 1) {
        this.alertService.error([res.message]);
        return;
      }

      this.openDialogSuccess();
      this.router.navigate(['/news']);
    });

  }

  private openDialogSuccess() {
    this.alertService.showSuccess(['Thêm tin tức thành công']);
  }

  private buildObjectPost(): any {
    const params = { ...this.form.value };
    params.cate = params.cate.id;
    params.image = params.image[0].id;

    if (!this.onFlowEdit) {
      delete params.id; // chỉ dành cho lúc page ở trạng thái là edit
    }

    return params;
  }

  private initForm() {
    this.form = this.fb.group({
      title: ['', [Validators.required]],
      content: ['', [Validators.required]],
      cate: [null, [Validators.required]],
      image: [[], Validators.required],
      description: ['', Validators.required],
      // for SEO
      url: ['', []],
      metaTitle: ['', []],
      metaDescription: ['', []],
      metaType: ['', []],
      metaUrl: ['', []],
      metaImage: ['', []],
      canonical: ['', []],
      textEndPage: ['', []]
    });
  }

  private getCategory(cb?: Function) {
    this.newsService.getTypes().subscribe((res: any) => {
      if (res.status === 1) {
        this.categoryItemsSource = res.data;
        if (cb) {
          cb();
        }
      }
    });
  }

  private loadDetailNews() {
    this.newsService.getDetail(this.params.id)
      .subscribe((res: any) => {
        if (res.status !== 1) {
          this.alertService.error(['Không tìm thấy thông tin bài viết']);
          return;
        }

        this.oldData = res.data;
        this.assignOldData();
      });
  }

  private assignOldData() {
    const data = { ...this.oldData };
    data.cate = this.getCateById(data.cate.toString());
    data.image = [<IFileTextValue>{
      id: data.image,
      text: ''
    }];

    data.description = data.description || '';
    data.url = data.url || '';
    data.content = data.content || '';
    data.metaUrl = data.metaUrl || '';
    data.metaDescription = data.metaDescription || '';
    data.metaTitle = data.metaTitle || '';
    data.metaImage = data.metaImage || '';
    data.canonical = data.canonical || '';
    data.textEndPage = data.textEndPage || '';

    this.form.patchValue(data);
  }

  private getCateById(id: string): any {
    return this.categoryItemsSource.find(c => {
      return c.id.toString() === id;
    });
  }
}
