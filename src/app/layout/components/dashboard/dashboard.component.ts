import { AfterViewInit, Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../shared/services/global.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  constructor(
    private globalService: GlobalService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.globalService.setFullPageLoading(false);
  }

}
