import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { AddProjectComponent } from './add-project/add-project.component';
import { ProjectComponent } from './project.component';
import { UpdateSeoInfoProjectComponent } from './update-seo-info/update-seo-info.component';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: ProjectComponent
  },
  {
    path: 'add',
    component: AddProjectComponent
  },
  {
    path: 'modify',
    component: AddProjectComponent
  }, 
  {
    path: 'update-seo-info',
    component: UpdateSeoInfoProjectComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
})

export class ProjectRoutingModule {

}
