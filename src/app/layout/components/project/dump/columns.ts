import { CellDisplay, ITableColumn, FilterType, SelectValuesType } from '../../../../shared/components/table-control/iTableColumn';
import { projectStatus } from '../../../../shared/constants/status';
import { of } from 'rxjs';

export const columns: ITableColumn[] = [
  {
    headerName: 'Tiêu đề',
    displayValuePath: 'title',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  }, {
    headerName: 'Loại',
    displayValueAs: CellDisplay.STRING,
    displayValuePath: 'type',
    filterable: false,
    sortable: false
  }, {
    headerName: 'Địa chỉ',
    displayValueAs: CellDisplay.STRING,
    displayValuePath: 'address',
    filterable: false,
    sortable: false
  }, {
    headerName: 'Diện tích',
    displayValueAs: CellDisplay.NUMBER,
    displayValuePath: 'area',
    filterable: false,
    sortable: false
  }, {
    headerName: 'Giá',
    displayValueAs: CellDisplay.NUMBER,
    displayValuePath: 'price',
    filterable: false,
    sortable: false
  }, {
    headerName: 'Status',
    displayValueAs: CellDisplay.STRING,
    displayValuePath: 'status',
    filterable: true,
    sortable: false,
    filterConfig: {
      dataType: FilterType.SELECT,
      queryName: 'status',
      dataLoadBy: SelectValuesType.FIXED
    },
    optionsFilter: of(projectStatus)
  }
];
