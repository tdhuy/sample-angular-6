import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap';
import { ImageUploaderModule } from '../../../shared/components/image-uploader/imageUploader.module';
import { InputDateModule } from '../../../shared/components/input-date/input-date.module';
import { InputNumberModule } from '../../../shared/components/input-number/input-number.module';
import { InputRichText2Module } from '../../../shared/components/input-rich-text-2/input-rich-text-2.module';
import { InputRichTextModule } from '../../../shared/components/input-rich-text/input-rich-text.module';
import { InputTextModule } from '../../../shared/components/input-text/input-text.module';
import { InputTextareaModule } from '../../../shared/components/input-textarea/input-textarea.module';
import { SelectSearchModule } from '../../../shared/components/select-search/select-search.module';
import { TableControlModule } from '../../../shared/components/table-control/table-control.module';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { AddProjectComponent } from './add-project/add-project.component';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectComponent } from './project.component';
import { ProjectService } from 'src/app/layout/components/project/project.service';
import { UpdateSeoInfoProjectComponent } from './update-seo-info/update-seo-info.component';

@NgModule({
  declarations: [
    ProjectComponent,
    AddProjectComponent,
    UpdateSeoInfoProjectComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ProjectRoutingModule,
    TabsModule.forRoot(),
    SelectSearchModule,
    InputTextModule,
    InputNumberModule,
    InputTextareaModule,
    InputRichTextModule,
    InputRichText2Module,
    InputDateModule,
    ImageUploaderModule,
    TableControlModule
  ],
  exports: [
    ProjectComponent,
    AddProjectComponent
  ],
  providers: [
    StringHelperService,
    ProjectService
  ]
})

export class ProjectModule {

}
