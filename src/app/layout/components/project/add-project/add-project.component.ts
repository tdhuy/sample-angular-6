import { AfterContentInit, Component } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EditableFormBaseComponent } from '../../../../shared/components/base/editable-form-base.component';
import { GlobalConstant } from '../../../../shared/constants/globalConstant';
import { AlertService } from '../../../../shared/services/alert.service';
import CityListOther1 = GlobalConstant.CityListOther1;
import { ProjectService } from '../project.service';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  providers: [
    AlertService,
    ProjectService
  ]
})

export class AddProjectComponent extends EditableFormBaseComponent implements AfterContentInit {
  projectTypeItemsSource: any[] = [];
  form: FormGroup;

  projectCityItemsSource: any = [];
  projectDistrictItemsSource: any = [];

  selectedObjs = {
    city: {
      index: -1
    },
    district: {
      index: -1
    }
  };

  onFlowEdit = false;
  params: any = {};
  oldData: any = null;

  constructor(
    private router: Router,
    private projectService: ProjectService,
    private activatedRoute: ActivatedRoute) {
    super();

    this.activatedRoute.queryParams.subscribe(params => {
      if (params.id) {
        // case update project
        this.params = params;
        this.onFlowEdit = true;
      }
    });
  }

  ngAfterContentInit() {
    this.initForm();
    this.initItemsSourceCities();
    this.loadProjectType(() => {
      this.loadProjectById();
    });
  }

  post() {
    const params = this.generatePostObject();
    let req: Observable<any>;

    if (this.onFlowEdit) {
      req = this.projectService.updateInfo(this.params.id, params);
    } else {
      req = this.projectService.create(params);
    }


    req
      .subscribe((res: any) => {
        if (res.status !== 1) {
          this.alertService.error([res.message]);
          return;
        }

        this.openDialogSuccess(res.message);
        this.router.navigate(['/project']);
      });
  }

  openDialogSuccess(msg: string) {
    this.alertService.showSuccess([msg]);
  }

  onClickBtnClear() {
    this.form.reset();
  }

  onClickBtnSubmit() {
    this.onSubmit();
  }

  onChangedCity(city: any) {
    this.form.get('district').setValue(null);

    if (!city) {
      return;
    }

    const index = CityListOther1.findIndex(item => item.code.toString() === city.value.toString());
    if (index >= 0) {
      this.selectedObjs.city.index = index;

      this.projectDistrictItemsSource = CityListOther1[index].district.map(dis => {
        return {
          text: dis.name,
          value: dis.id
        };
      });
    }
  }

  private generatePostObject(): any {

    const params = this.form.value;

    params.type = params.type ? params.type.id : '';
    params.city = params.city ? params.city.value : '';
    params.district = params.district ? params.district.value : '';

    // time
    params.deliveryHouseDate = params.deliveryHouseDate.getTime();
    params.projectProgressStartDate = params.projectProgressStartDate.getTime();
    params.projectProgressEndDate = params.projectProgressEndDate.getTime();
    params.projectProgressDate = params.projectProgressDate.getTime();

    if (!this.onFlowEdit) {
      delete params.url;
    }

    return params;
  }

  private initForm() {
    this.form = this.fb.group({
      isShowOverview: [true, []],
      type: [null, [Validators.required]],
      introImages: [[], []],
      title: ['', [Validators.required]],
      city: [null, [Validators.required]],
      district: [null, [Validators.required]],
      address: ['', []],
      area: [0, []],
      projectScale: ['', []],
      price: [0, []],
      deliveryHouseDate: [new Date(), []],
      constructionArea: ['', []],
      descriptionInvestor: ['', []],
      description: ['', []],

      isShowLocationAndDesign: [false, []],
      location: ['', []],
      infrastructure: ['', []],

      isShowGround: [false, []],
      overallSchema: [[], []],
      groundImages: [[], []],

      isShowImageLibs: [false, []],
      imageAlbums: [[], []],

      isShowProjectProgress: [false, []],
      projectProgressTitle: ['', []],
      projectProgressStartDate: [new Date(), []],
      projectProgressEndDate: [new Date(), []],
      projectProgressDate: [new Date(), []],
      projectProgressImages: [[], []],

      isShowTabVideo: [false, []],
      video: ['', []],

      isShowFinancialSupport: [false, []],
      financialSupport: ['', []],

      isShowInvestor: [false, []],
      detailInvestor: ['', []],

      // seo
      url: ['', []],
      metaTitle: ['', []],
      metaDescription: ['', []],
      metaType: ['', []],
      metaUrl: ['', []],
      metaImage: ['', []],
      canonical: ['', []],
      textEndPage: ['', []]
    });
  }

  private initItemsSourceCities() {
    this.projectCityItemsSource = CityListOther1.map(city => {
      return {
        text: city.name,
        value: city.code
      };
    });
  }

  private loadProjectType(cb: Function) {
    this.projectService.getProjectTypes()
      .subscribe(res => {
        if (res.status === 1) {
          this.projectTypeItemsSource = res.data;
          cb();
        }
      });
  }

  private loadProjectById() {
    if (this.params.id) {
      this.projectService.getDetail(this.params.id)
        .subscribe(res => {
          if (res.status !== 1) {
            this.alertService.error(['Không tìm thấy thông tin dự án']);
          } else {
            this.oldData = res.data;
            this.assignOldData();
          }
        });
    }
  }

  /**
   * Use when flow edit
   */
  private assignOldData() {
    const data = { ...this.oldData };
    data.type = this.mapTypeIdToObject(data.type);
    data.city = this.mapCityCodeToObject(data.city);
    data.deliveryHouseDate = new Date(data.deliveryHouseDate);
    data.projectProgressDate = new Date(data.projectProgressDate);
    data.projectProgressEndDate = new Date(data.projectProgressEndDate);
    data.projectProgressStartDate = new Date(data.projectProgressStartDate);

    data.url = data.url || '';
    data.content = data.content || '';
    data.metaUrl = data.metaUrl || '';
    data.metaDescription = data.metaDescription || '';
    data.metaTitle = data.metaTitle || '';
    data.metaImage = data.metaImage || '';
    data.canonical = data.canonical || '';
    data.textEndPage = data.textEndPage || '';

    this.form.patchValue(data);
    this.onChangedCity(data.city);

    // map district, it depends on city
    setTimeout(() => {
      const district = this.mapDistrictIdToObject(this.oldData.district);
      this.form.controls.district.setValue(district);
    }, 0);
  }

  /**
   * Use when flow edit
   */
  private mapTypeIdToObject(id: any): any {
    return this.projectTypeItemsSource.find(item => {
      return item.id.toString() === id.toString();
    });
  }

  /**
   * Use when flow edit
   */
  private mapCityCodeToObject(cd: string): any {
    return this.projectCityItemsSource.find(item => {
      return item.value.toString() === cd;
    });
  }

  /**
   * Use when flow edit
   */
  private mapDistrictIdToObject(id: any): any {
    return this.projectDistrictItemsSource.find(item => {
      return item.value.toString() === id.toString();
    });
  }
}
