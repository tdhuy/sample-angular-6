import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { APIs } from "src/app/shared/constants/api";

@Injectable()
export class ProjectService {
  constructor(
    private httpClient: HttpClient
  ) {}

  getProjectTypes(): Observable<any> {
    return this.httpClient.get(APIs.project.projectType);
  }

  create(data: any): Observable<any> {
    return this.httpClient.post(APIs.project.add, data);
  }

  list(params: any): Observable<any> {
    return this.httpClient.get(APIs.project.list, {params});
  }

  softDeleteProject(id: string): Observable<any> {
    const url = APIs.project.update.replace('{id}', id);
    return this.httpClient.post(url, {status: 0});
  }

  updateInfo(id: string, data: any): Observable<any> {
    const url = APIs.project.update.replace('{id}', id);
    return this.httpClient.post(url, data);
  }

  getDetail(id: string): Observable<any> {
    return this.httpClient.get(APIs.project.detail.replace('{id}', id));
  }
}
