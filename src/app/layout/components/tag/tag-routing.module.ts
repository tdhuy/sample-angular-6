import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { TagComponent } from "./tag.component";
import { UpdateSeoInfoTagComponent } from "./update-seo-info/update-seo-info.component";

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: TagComponent
  },
  {
    path: 'update-seo-info',
    component: UpdateSeoInfoTagComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TagRoutingModule {

}