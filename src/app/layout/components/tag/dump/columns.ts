import { ITableColumn, CellDisplay, FilterType } from "../../../../shared/components/table-control/iTableColumn";

export const columns: ITableColumn[] = [
  {
    headerName: 'Keyword',
    displayValuePath: 'keyword',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Slug',
    displayValuePath: 'slug',
    displayValueAs: CellDisplay.STRING,
    filterable: true,
    sortable: false,
    filterConfig: {
      queryName: 'slug',
      dataType: FilterType.STRING
    }
  },
  {
    headerName: 'Meta title',
    displayValuePath: 'metaTitle',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
];