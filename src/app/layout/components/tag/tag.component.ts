import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { PageBaseComponent } from '../../../shared/components/base/page-base.component';
import { IRequestParams } from '../../../shared/components/table-control/iRequestParams';
import { TableControlComponent } from '../../../shared/components/table-control/table-control.component';
import { getStatusName } from '../../../shared/constants/status';
import { columns } from './dump/columns';
import { TagService } from './tag.service';

@Component({
  selector: 'app-tag-list-page',
  templateUrl: './tag.component.html'
})

export class TagComponent extends PageBaseComponent {
  @ViewChild('tableControl') tableControl: TableControlComponent;

  grid = {
    itemsSource: [],
    selectedItems: [],
    totalItems: 0
  };

  columns = columns;
  selectedRow = -1;
  private _requestParams: IRequestParams;

  constructor(private tagService: TagService) {
    super();

    this.columns.forEach((col, index) => {
      this.columns[index].code = this.stringHelperService.genRandomString();
    });
  }

  _fetchData(requestParams: IRequestParams) {
    this.selectedRow = -1;
    this._settingDisable();
    this._requestParams = requestParams;
    this._loadData();
  }

  receiveAction(data: { type: string, item: any, extra?: any }) {
    switch (data.type) {
      case 'UPDATE_STATUS':
        this.updateStatus(data.item);
        break;
    }
  }

  private updateStatus(item: any) {
    this.alertService.confirm(['Bạn chắc muốn cập nhật status tag này?'], () => {
      this.globalService.setFullPageLoading(true);

      this.tagService.updateInfo(item.id.toString(), item).subscribe((res: any) => {
        this.globalService.setFullPageLoading(false);
        if (res.status === 1) {
          this.alertService.showSuccess([`Cập nhật ${item.name} thành công`]);
          this._loadData();
        } else {
          this.alertService.error([`${res.message}`]);
        }
      });
    });
  }

  private _settingDisable() { }

  private _loadData() {
    const options: any = {
      limit: this._requestParams.size,
      page: this._requestParams.page
    };

    if (this._requestParams.filter) {
      Object.assign(options, this._requestParams.filter);
    }

    const req: Observable<any> = this.tagService.getList(options);

    const sub = req.subscribe((res) => {
      if (res.status === 1 && res.data.items.length) {
        this.grid.itemsSource = this.mapDataFromList(res.data.items);
        this.grid.totalItems = res.data.total;
        this.tableControl.onRenderDone();
      } else {
        this.grid.itemsSource = [];
        this.grid.totalItems = 0;
        this.tableControl.onRenderDone();
      }
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.grid.itemsSource = [];
      this.grid.totalItems = 0;
      this.tableControl.onRenderDone();
    });

    this.subscriptions.push(sub);
  }

  private mapDataFromList(items: any[]) {
    return items.map(item => {
      item.status = getStatusName(item.status);
      return item;
    });
  }
}