import { Component, OnInit } from "@angular/core";
import { EditableFormBaseComponent } from "../../../../shared/components/base/editable-form-base.component";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { TagService } from "../tag.service";
import { AlertService } from "../../../../shared/services/alert.service";
import { GlobalService } from "../../../../shared/services/global.service";
import { ValidatorsService } from "../../../../shared/services/validator.service";

@Component({
  selector: 'app-update-seo-info-tag-page',
  templateUrl: './update-seo-info.component.html'
})

export class UpdateSeoInfoTagComponent extends EditableFormBaseComponent implements OnInit  {
  form: FormGroup;
  tagId = null;
  tagInfo = null;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tagService: TagService) {
    super();

    this.activatedRoute.queryParams.subscribe(param => {
      this.tagId = param.id;
    })
  }

  ngOnInit() {
    this.initForm();

    if (this.tagId) {
      this.tagService.detail(this.tagId)
        .subscribe(res => {
          if (res.status === 1) {
            this.tagInfo = res.data;

            this.form.setValue({
              slug: this.tagInfo.slug || '',
              metaTitle: this.tagInfo.metaTitle || '',
              metaDescription: this.tagInfo.metaDescription || '',
              metaType: this.tagInfo.metaType || '',
              metaUrl: this.tagInfo.metaUrl || '',
              metaImage: this.tagInfo.metaImage || '',
              canonical: this.tagInfo.canonical || '',
              textEndPage: this.tagInfo.textEndPage || ''
            });
          } else {
            this.alertService.error([res.message]);
          }
        });
    }
  }

  post(name?: string) {
    const params = this.form.value;

    this.tagService.updateInfo(this.tagId, params)
      .subscribe((res: any) => {
        if (res.status === 1) {
          this.alertService.showSuccess(['Cập nhật thông tin SEO thành công!']);
          this.router.navigate(['/tag']);
        } else {
          this.alertService.error([res.message]);
        }
      });
  }

  onClickBtnSubmit() {
    if (!this.tagInfo) {
      return;
    }

    this.onSubmit();
  }

  onClickBtnClear() {
    this.form.reset();
  }

  private initForm() {
    this.form = this.fb.group({
      slug: ['', [
        this.validatorService.checkUrlSeo
      ]],
      metaTitle: ['', []],
      metaDescription: ['', []],
      metaType: ['', []],
      metaUrl: ['', []],
      metaImage: ['', []],
      canonical: ['', []],
      textEndPage: ['', []]
    });
  }
}