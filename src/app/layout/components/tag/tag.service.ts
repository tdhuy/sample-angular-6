import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { APIs } from "../../../shared/constants/api";

@Injectable()
export class TagService {
  constructor(private _http: HttpClient) {}

  getList(params: any): Observable<any> {
    return this._http.get(APIs.Tag.List, {params});
  }

  detail(id: string): Observable<any> {
    const url = APIs.Tag.Detail.replace('{id}', id);
    return this._http.get(url);
  }

  updateInfo(id: string, data: any): Observable<any> {
    const url = APIs.Tag.Update.replace('{id}', id);
    return this._http.post(url, data);
  }
}