import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TagComponent } from "./tag.component";
import { UpdateSeoInfoTagComponent } from "./update-seo-info/update-seo-info.component";
import { ReactiveFormsModule } from "@angular/forms";
import { TagRoutingModule } from "./tag-routing.module";
import { TagService } from "./tag.service";
import { InputTextModule } from "../../../shared/components/input-text/input-text.module";
import { TableControlModule } from "../../../shared/components/table-control/table-control.module";
import { StringHelperService } from "../../../shared/services/string-helper.service";
import { AlertService } from "../../../shared/services/alert.service";

@NgModule({
  declarations: [
    TagComponent,
    UpdateSeoInfoTagComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TagRoutingModule,
    InputTextModule,
    TableControlModule
  ],
  exports: [],
  providers: [
    TagService,
    StringHelperService,
    AlertService
  ]
})

export class TagModule { }