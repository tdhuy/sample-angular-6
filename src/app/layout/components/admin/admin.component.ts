import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PageBaseComponent } from '../../../shared/components/base/page-base.component';
import { IRequestParams } from '../../../shared/components/table-control/iRequestParams';
import { IFixedOption } from '../../../shared/components/table-control/iTableColumn';
import { TableControlComponent } from '../../../shared/components/table-control/table-control.component';
import { getStatusName, userStatus } from '../../../shared/constants/status';
import { AlertService } from '../../../shared/services/alert.service';
import { GlobalService } from '../../../shared/services/global.service';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { AdminService } from './admin.service';
import { columns } from './dump/columns';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin.component.html'
})

export class AdminComponent extends PageBaseComponent {
  @ViewChild('tableControl') tableControl: TableControlComponent;

  grid = {
    itemsSource: [],
    selectedItems: [],
    totalItems: 0
  };

  columns = columns;
  selectedRow = -1;
  private _requestParams: IRequestParams;

  constructor(private adminService: AdminService) {
    super();

    this.columns.forEach((col, index) => {
      this.columns[index].code = this.stringHelperService.genRandomString();
    });
  }

  _fetchData(requestParams: IRequestParams) {
    this.selectedRow = -1;
    this._settingDisable();
    this._requestParams = requestParams;
    this._loadData();
  }

  receiveAction(data: { type: string, item: any }) {
    switch (data.type) {
      case 'UPDATE_STATUS':
        this.updateStatus(data.item);
        break;
    }
  }

  private updateStatus(admin: any) {
    this.alertService.updateStatus(userStatus, admin.status || '', (status) => {
      this.sendRequestUpdateStatus(admin, status);
    });
  }

  private sendRequestUpdateStatus(admin: any, status: IFixedOption) {
    this.globalService.setFullPageLoading(true);
    const sub = this.adminService.updateStatus(admin._id, {
      status: status.id
    }).subscribe(res => {
      if (res.status === 1) {
        this.selectedRow = -1;
        this._loadData();
        this._settingDisable();
      }
      this.globalService.setFullPageLoading(false);
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.globalService.setFullPageLoading(false);
    });

    this.subscriptions.push(sub);
  }

  private _settingDisable() { }

  private _loadData() {
    const options: any = {
      size: this._requestParams.size,
      page: this._requestParams.page
    };

    const req: Observable<any> = this.adminService.list(options);

    const sub = req.subscribe((res) => {
      if (res.status === 1) {
        if (res.data) {
          this.grid.itemsSource = this.mapDataFromList(res.data.items);
          this.grid.totalItems = res.data.total;
          this.tableControl.onRenderDone();
        }
      } else {
        this.grid.itemsSource = [];
        this.grid.totalItems = 0;
        this.tableControl.onRenderDone();
      }
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.grid.itemsSource = [];
      this.grid.totalItems = 0;
      this.tableControl.onRenderDone();
    });

    this.subscriptions.push(sub);
  }

  private mapDataFromList(items: any[]) {
    return items.map(item => {
      // status: from value to name
      item.status = getStatusName(item.status);

      return item;
    });
  }
}
