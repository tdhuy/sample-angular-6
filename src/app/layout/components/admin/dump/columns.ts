import { CellDisplay, ITableColumn } from '../../../../shared/components/table-control/iTableColumn';

export const columns: ITableColumn[] = [
  {
    headerName: 'Name',
    displayValuePath: 'name',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  }, {
    headerName: 'Username',
    displayValuePath: 'username',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  }, {
    headerName: 'Email',
    displayValueAs: CellDisplay.STRING,
    displayValuePath: 'email',
    filterable: false,
    sortable: false
  }, {
    headerName: 'Status',
    displayValueAs: CellDisplay.STRING,
    displayValuePath: 'status',
    filterable: false,
    sortable: false
  }
];
