import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { APIs } from '../../../shared/constants/api';

@Injectable()
export class AdminService {
  constructor(private _http: HttpClient) {}

  list(params: any): Observable<any> {
    return this._http.get(APIs.Admin.List, {params});
  }

  updateStatus(id: string, data: any): Observable<any> {
    const url = APIs.Admin.UpdateStatus.replace('{id}', id);
    return this._http.post(url, data);
  }

  create(data: any): Observable<any> {
    return this._http.post(APIs.Admin.Create, data);
  }

  updateInfo(data: any): Observable<any> {
    return this._http.post(APIs.Admin.UpdateInfo, data);
  }
}
