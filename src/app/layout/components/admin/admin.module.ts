import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { InputPasswordModule } from '../../../shared/components/input-password/input-password.module';
import { InputTextModule } from '../../../shared/components/input-text/input-text.module';
import { TableControlModule } from '../../../shared/components/table-control/table-control.module';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminService } from './admin.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CreateUpdateAdminComponent } from './create-update-admin/create-update-admin.component';

@NgModule({
  declarations: [
    AdminComponent,
    CreateUpdateAdminComponent,
    ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    InputTextModule,
    InputPasswordModule,
    TableControlModule
  ],
  exports: [],
  providers: [
    AdminService,
    StringHelperService
  ]
})

export class AdminModule {

}
