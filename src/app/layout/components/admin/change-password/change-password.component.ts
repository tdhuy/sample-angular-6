import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EditableFormBaseComponent } from '../../../../shared/components/base/editable-form-base.component';
import { AlertService } from '../../../../shared/services/alert.service';
import { GlobalService } from '../../../../shared/services/global.service';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-change-password-page',
  templateUrl: './change-password.component.html'
})

export class ChangePasswordComponent extends EditableFormBaseComponent implements OnInit {
  form: FormGroup;

  constructor(private adminService: AdminService) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      password: ['', Validators.required],
      reenterPassword: ['', Validators.required],
      oldPassword: ['', Validators.required]
    });
  }

  post() {
    if (this.form.controls['password'].value !== this.form.controls['reenterPassword'].value) {
      this.alertService.error(['2 mật khẩu mới không khớp']);
      return;
    }

    this.globalService.setFullPageLoading(true);

    this.adminService.updateInfo(this.form.value)
      .subscribe((res: any) => {
        this.globalService.setFullPageLoading(false);

        if (res.status === 1) {
          this.alertService.showSuccess(['Đổi mật khẩu thành công']);
          this.form.reset();
          return;
        }

        this.alertService.error([res.message]);
      });
  }

  onClickBtnSubmit() {
    this.onSubmit();
  }

  onClickBtnClear() {
    this.form.reset();
  }
}
