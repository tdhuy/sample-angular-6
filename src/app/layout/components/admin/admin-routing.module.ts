import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CreateUpdateAdminComponent } from './create-update-admin/create-update-admin.component';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: AdminComponent
  }, {
    path: 'add',
    component: CreateUpdateAdminComponent
  }, {
    path: 'change-password',
    component: ChangePasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminRoutingModule {

}
