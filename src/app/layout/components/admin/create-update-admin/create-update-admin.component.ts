import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EditableFormBaseComponent } from '../../../../shared/components/base/editable-form-base.component';
import { AlertService } from '../../../../shared/services/alert.service';
import { GlobalService } from '../../../../shared/services/global.service';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-create-update-admin-page',
  templateUrl: './create-update-admin.component.html'
})

export class CreateUpdateAdminComponent extends EditableFormBaseComponent implements OnInit {
  form: FormGroup;

  constructor(private adminService: AdminService) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      reenterPassword: ['', Validators.required],
      phone: ['', Validators.required]
    });
  }

  onClickBtnClear() {
    this.alertService.confirm(['Bạn có chắc muốn xoá form?'], () => {
      this.form.reset();
    });
  }

  post() {
    if (this.form.controls['password'].value !== this.form.controls['reenterPassword'].value) {
      this.alertService.error(['2 mật khẩu không giống nhau!']);
      return;
    }

    this.globalService.setFullPageLoading(true);

    this.adminService.create(this.form.value)
      .subscribe((res: any) => {
        this.globalService.setFullPageLoading(false);
        if (res.status === 1) {
          this.alertService.showSuccess(['Thêm admin mới thành công']);
          this.form.reset();
          return;
        }

        this.alertService.error([res.message]);
      });
  }

  onClickBtnSubmit() {
    this.onSubmit();
  }
}
