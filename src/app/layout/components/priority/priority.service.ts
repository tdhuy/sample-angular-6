import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APIs } from '../../../shared/constants/api';
import { priorityStatus } from '../../../shared/constants/status';

@Injectable()
export class PriorityService {
  constructor(private _http: HttpClient) {}

  list(params: any): Observable<any> {
    return this._http.get(APIs.priority.list, {params});
  }

  updateStatus(priority: any) {
    const url = APIs.priority.update.replace('{id}', priority._id);
    if (priority.status === 'Active') {
      return this._http.post(url, {status: priorityStatus[1].id});
    } else {
      return this._http.post(url, {status: priorityStatus[0].id});
    }
  }

  updateValue(priorityId: string | number, data: any): Observable<any> {
    const url = APIs.priority.update.replace('{id}', priorityId.toString());
    return this._http.post(url, data);
  }
}
