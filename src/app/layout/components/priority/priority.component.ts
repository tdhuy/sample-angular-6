import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { PageBaseComponent } from '../../../shared/components/base/page-base.component';
import { IRequestParams } from '../../../shared/components/table-control/iRequestParams';
import { TableControlComponent } from '../../../shared/components/table-control/table-control.component';
import { getStatusName } from '../../../shared/constants/status';
import { columns } from './dump/columns';
import { PriorityService } from './priority.service';

@Component({
  selector: 'app-priority-list',
  templateUrl: './priority.component.html'
})

export class PriorityComponent extends PageBaseComponent {
  @ViewChild('tableControl') tableControl: TableControlComponent;

  grid = {
    itemsSource: [],
    selectedItems: [],
    totalItems: 0
  };

  columns = columns;
  selectedRow = -1;
  private _requestParams: IRequestParams;

  constructor(private priorityService: PriorityService) {
    super();

    this.columns.forEach((col, index) => {
      this.columns[index].code = this.stringHelperService.genRandomString();
    });
  }

  _fetchData(requestParams: IRequestParams) {
    this.selectedRow = -1;
    this._settingDisable();
    this._requestParams = requestParams;
    this._loadData();
  }

  receiveAction(data: { type: string, item: any, extra?: any }) {
    console.log('data receiveAction', data);
    switch (data.type) {
      case 'UPDATE_STATUS':
        this.updateStatus(data.item);
        break;
      case 'USER_CHANGE_BALANCE':
        console.log(data.extra);
        const type = data.extra.col.displayValuePath.toUpperCase();
        this.openDialogUpdatePriority(data.item, type);
        break;
    }
  }

  private openDialogUpdatePriority(priority: any, type: string) {
    this.alertService.updatePriority(priority, type, (value: number) => {

      let param: any;

      if (type === 'MINDAY') {
        param = {'minDay': value};
      } else {
        param = {'costByDay': value};
      }

      console.log('param', param);

      this.priorityService.updateValue(priority._id, param)
        .subscribe((res: any) => {
        if (res.status === 1) {
          this._loadData();
        } else {
          this.alertService.error([res.message]);
        }
      });


    });
  }

  private updateStatus(item: any) {
    this.alertService.confirm(['Bạn chắc muốn cập nhật status priority này?'], () => {
      this.globalService.setFullPageLoading(true);

      this.priorityService.updateStatus(item).subscribe((res: any) => {
        this.globalService.setFullPageLoading(false);
        if (res.status === 1) {
          this.alertService.showSuccess([`Cập nhật ${item.name} thành công`]);
          this._loadData();
        } else {
          this.alertService.error([`${res.message}`]);
        }
      });
    });
  }

  private _settingDisable() { }

  private _loadData() {
    const options: any = {
      limit: this._requestParams.size,
      page: this._requestParams.page
    };

    const req: Observable<any> = this.priorityService.list(options);

    const sub = req.subscribe((res) => {
      console.log(res);
      if (res.status === 1 && res.data.length) {
        this.grid.itemsSource = this.mapDataFromList(res.data);
        this.grid.totalItems = res.data.length;
        this.tableControl.onRenderDone();
      } else {
        this.grid.itemsSource = [];
        this.grid.totalItems = 0;
        this.tableControl.onRenderDone();
      }
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.grid.itemsSource = [];
      this.grid.totalItems = 0;
      this.tableControl.onRenderDone();
    });

    this.subscriptions.push(sub);
  }

  private mapDataFromList(items: any[]) {
    return items.map(item => {
      // status: from value to name
      item.status = getStatusName(item.status);
      // item.type = item.type === 1 ? 'Cá nhân' : 'Công ty';
      // item.gender = item.gender === 1 ? 'Nam' : 'Nữ';

      return item;
    });
  }
}
