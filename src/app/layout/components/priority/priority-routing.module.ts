import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { PriorityComponent } from './priority.component';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: PriorityComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PriorityRoutingModule {}
