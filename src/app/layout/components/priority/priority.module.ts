import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { InputNumberModule } from '../../../shared/components/input-number/input-number.module';
import { SelectSearchModule } from '../../../shared/components/select-search/select-search.module';
import { TableControlModule } from '../../../shared/components/table-control/table-control.module';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { InputTextModule } from '../../../shared/components/input-text/input-text.module';
import { PriorityRoutingModule } from './priority-routing.module';
import { PriorityComponent } from './priority.component';
import { PriorityService } from './priority.service';

@NgModule({
  declarations: [
    PriorityComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    PriorityRoutingModule,
    TableControlModule,
    InputTextModule,
    InputNumberModule,
    SelectSearchModule
  ],
  providers: [
    PriorityService,
    StringHelperService
  ]
})

export class PriorityModule {

}
