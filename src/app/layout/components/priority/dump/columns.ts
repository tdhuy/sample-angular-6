import { CellDisplay, ITableColumn } from '../../../../shared/components/table-control/iTableColumn';

export const columns: ITableColumn[] = [
  {
    headerName: 'Name',
    displayValuePath: 'name',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Priority',
    displayValuePath: 'priority',
    displayValueAs: CellDisplay.NUMBER,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Min day',
    displayValuePath: 'minDay',
    displayValueAs: CellDisplay.USER_BALANCE_WITH_BTN,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Cost by day',
    displayValuePath: 'costByDay',
    displayValueAs: CellDisplay.USER_BALANCE_WITH_BTN,
    filterable: false,
    sortable: false,
  },
  {
    headerName: 'Status',
    displayValuePath: 'status',
    displayValueAs: CellDisplay.NUMBER,
    filterable: false,
    sortable: false,
    contentStyle: {
      'text-align': 'center'
    }
  }
];
