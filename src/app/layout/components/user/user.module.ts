import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TableControlModule } from '../../../shared/components/table-control/table-control.module';
import { StringHelperService } from '../../../shared/services/string-helper.service';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserService } from './user.service';

@NgModule({
  declarations: [
    UserComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    TableControlModule
  ],
  providers: [
    UserService,
    StringHelperService
  ]
})

export class UserModule {

}
