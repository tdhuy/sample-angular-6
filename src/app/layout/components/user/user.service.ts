import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APIs } from '../../../shared/constants/api';
import { userStatus } from '../../../shared/constants/status';

@Injectable()
export class UserService {
  constructor(private _http: HttpClient) {}

  list(params: any): Observable<any> {
    return this._http.get(APIs.users.list, {params});
  }

  updateType(userId: string | number, data: any): Observable<any> {
    const url = APIs.users.updateUserType.replace('{id}', userId.toString());
    return this._http.put(url, data);
  }

  update(userId: string | number, data) {
    const url = APIs.users.update.replace('{id}', userId.toString());
    return this._http.post(url, data);
  }

  softDelete(userId: string | number) {
    const url = APIs.users.update.replace('{id}', userId.toString());
    return this._http.post(url, {status: userStatus[3].id});
  }

  updateMainBalance(userId: string | number, data: any): Observable<any> {
    const url = APIs.users.updateMainBalance.replace('{userId}', userId.toString());
    return this._http.post(url, data);
  }

  updatePromoBalance(userId: string | number, data: any): Observable<any> {
    const url = APIs.users.updatePromoBalance.replace('{userId}', userId.toString());
    return this._http.post(url, data);
  }
}
