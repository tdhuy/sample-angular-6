import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { UserComponent } from './user.component';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'null',
    component: UserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class UserRoutingModule {}
