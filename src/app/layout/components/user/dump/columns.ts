import { CellDisplay, ITableColumn } from '../../../../shared/components/table-control/iTableColumn';

export const columns: ITableColumn[] = [
  {
    headerName: 'Username',
    displayValuePath: 'username',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Tên',
    displayValuePath: 'name',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Email',
    displayValuePath: 'email',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Giói tính',
    displayValuePath: 'gender',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false
  },
  {
    headerName: 'Loại user',
    displayValuePath: 'typeNm',
    displayValueAs: CellDisplay.STRING,
    filterable: false,
    sortable: false,
    contentStyle: {
      'text-align': 'center'
    }
  },
  {
    headerName: 'Tài khoản chính',
    displayValuePath: 'balance.main',
    displayValueAs: CellDisplay.USER_BALANCE_WITH_BTN,
    filterable: false,
    sortable: false,
    contentStyle: {
      'text-align': 'right'
    }
  },
  {
    headerName: 'Tài khoản \n khuyến mãi',
    displayValuePath: 'balance.promo',
    displayValueAs: CellDisplay.USER_BALANCE_WITH_BTN,
    filterable: false,
    sortable: false,
    contentStyle: {
      'text-align': 'right'
    }
  },
  {
    headerName: 'Ngày hết hạn \n sử dụng tiền',
    displayValuePath: 'expirationDate',
    displayValueAs: CellDisplay.DATE,
    filterable: false,
    sortable: false,
    contentStyle: {
      'text-align': 'right'
    }
  },
  {
    headerName: 'Trạng thái',
    displayValuePath: 'status',
    displayValueAs: CellDisplay.NUMBER,
    filterable: false,
    sortable: false,
    contentStyle: {
      'text-align': 'center'
    }
  }
];
