import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { PageBaseComponent } from '../../../shared/components/base/page-base.component';
import { IRequestParams } from '../../../shared/components/table-control/iRequestParams';
import { TableControlComponent } from '../../../shared/components/table-control/table-control.component';
import { getStatusName, userStatus } from '../../../shared/constants/status';
import { columns } from './dump/columns';
import { UserService } from './user.service';
import { IFixedOption } from '../../../shared/components/table-control/iTableColumn';
import { ISelectTextItem } from '../../../shared/components/select-text/i-select-text-item';
import HTTP_CODE from '../../../shared/constants/http_code';

@Component({
  selector: 'app-user-list',
  templateUrl: './user.component.html'
})

export class UserComponent extends PageBaseComponent {
  @ViewChild('tableControl') tableControl: TableControlComponent;

  grid = {
    itemsSource: [],
    selectedItems: [],
    totalItems: 0
  };

  columns = columns;
  selectedRow = -1;
  private _requestParams: IRequestParams;

  constructor(private userService: UserService) {
    super();

    this.columns.forEach((col, index) => {
      this.columns[index].code = this.stringHelperService.genRandomString();
    });
  }

  _fetchData(requestParams: IRequestParams) {
    this.selectedRow = -1;
    this._settingDisable();
    this._requestParams = requestParams;
    this._loadData();
  }

  receiveAction(data: { type: string, item: any, extra?: any }) {
    switch (data.type) {
      case 'REMOVE':
        this.removeItem(data.item);
        break;
      case 'USER_CHANGE_BALANCE':
        const type = data.extra.col.displayValuePath.split('.')[1].toUpperCase();
        this.openDialogChangeBalance(data.item, type);
        break;
      case 'USER_EXPIRATION_DATE':
        this.openDialogChangeExpirationDate(data.item);
        break;
      case 'UPDATE_STATUS':
        this.updateStatus(data.item);
        break;
      case 'CHANGE_USER_TYPE':
        this.openModdalChangeUserType(data.item);
        break;
    }
  }

  openModdalChangeUserType(user: any) {
    this.alertService.updateUserType(user, user.type, (type: ISelectTextItem) => {
      this._sendRequestUpdateUserType(user, type);
    });
  }

  updateStatus(user: any) {
    this.alertService.updateStatus(userStatus, user.status || '', (status: any) => {
      this._sendRequestUpdateStatus(user, status);
    });
  }

  private _sendRequestUpdateUserType(user: any, type: ISelectTextItem) {
    this.globalService.setFullPageLoading(true);

    const sub = this.userService.updateType(user.id, {
      type: type.id
    }).subscribe((res: any) => {
      if (res.status === HTTP_CODE.SUCCESS) {
        this.alertService.showSuccess(['Cập nhật loại người dùng thành công']);
        this._loadData();
      } else {
        this.alertService.error(res.message);
      }

      this.globalService.setFullPageLoading(false);
    });

    this.subscriptions.push(sub);
  }

  private _sendRequestUpdateStatus(user: any, status: IFixedOption) {
    this.globalService.setFullPageLoading(true);

    const sub = this.userService.update(user.id, {
      status: parseInt(status.id, 0)
    }).subscribe((res: any) => {
      if (res.status === HTTP_CODE.SUCCESS) {
        this.selectedRow = -1;
        this._loadData();
        this._settingDisable();
      } else {
        this.alertService.error([res.message]);
      }

      this.globalService.setFullPageLoading(false);
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.globalService.setFullPageLoading(false);
    });

    this.subscriptions.push(sub);
  }

  private openDialogChangeBalance(user: any, type: string) {
    this.alertService.changeUserBalance(user, type, (amount: number) => {
      let req: Observable<any>;
      const param = {
        amount,
        note: '',
        info: ''
      };

      if (type === 'MAIN') {
        req = this.userService.updateMainBalance(user.id, param);
      } else {
        req = this.userService.updatePromoBalance(user.id, param);
      }

      req.subscribe((res: any) => {
        if (res.status === 1) {
          this._loadData();
        } else {
          this.alertService.error([res.message]);
        }
      });
    });
  }

  private openDialogChangeExpirationDate(user: any) {
    this.alertService.changeUserExpirationDate(user, (expirationDate: number) => {
      const param = {expirationDate};
      this.userService.update(user.id, param).subscribe( (res: any) => {
        if (res.status === 1) {
          this._loadData();
        } else {
          this.alertService.error([res.message]);
        }
      });
    });
  }

  private removeItem(item: any) {
    this.alertService.confirm(['Có chắc muốn xoá user này?'], () => {
      this.globalService.setFullPageLoading(true);

      this.userService.softDelete(item.id).subscribe((res: any) => {
        this.globalService.setFullPageLoading(false);
        if (res.status === 1) {
          this.alertService.showSuccess([`Xoá user ${item.email} thành công`]);
          this._loadData();
        } else {
          this.alertService.error([`${res.message}`]);
        }
      });
    });
  }

  private _settingDisable() { }

  private _loadData() {
    const options: any = {
      limit: this._requestParams.size,
      page: this._requestParams.page
    };

    const req: Observable<any> = this.userService.list(options);

    const sub = req.subscribe((res) => {
      if (res.status === 1) {
        if (res.data) {
          this.grid.itemsSource = this.mapDataFromList(res.data.items);
          this.grid.totalItems = res.data.total * this._requestParams.size;
          this.tableControl.onRenderDone();
        }
      } else {
        this.grid.itemsSource = [];
        this.grid.totalItems = 0;
        this.tableControl.onRenderDone();
      }
    }, (err) => {
      this.alertService.error(['Lỗi']);
      this.grid.itemsSource = [];
      this.grid.totalItems = 0;
      this.tableControl.onRenderDone();
    });

    this.subscriptions.push(sub);
  }

  private mapDataFromList(items: any[]) {
    return items.map(item => {
      // status: from value to name
      item.status = getStatusName(item.status);
      item.typeNm = item.type === 2 ? 'Cá nhân' : 'Công ty';
      item.gender = item.gender === 1 ? 'Nam' : 'Nữ';

      return item;
    });
  }
}
