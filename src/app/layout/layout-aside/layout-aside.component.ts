import { Component } from '@angular/core';
import { UserService } from '../../shared/services/user/user.service';
import { AsideLinkItem } from '../shared/constant/aside-link.constant';
import { SessionService } from '../../shared/services/session.service';

@Component({
  selector: 'app-layout-aside',
  templateUrl: './layout-aside.component.html',
  styleUrls: ['./layout-aside.component.css'],
  providers: [
    UserService
  ]
})

export class LayoutAsideComponent {

  constructor(private sessionService: SessionService) {}

  links: AsideLinkItem[] = [
    {
      icon: 'icon-paper-plane',
      isVisible: true,
      routerLink: ['/project'],
      title: 'Dự án'
    },
    {
      icon: 'icon-book-open',
      isVisible: true,
      routerLink: ['/news'],
      title: 'Tin Tức'
    },
    {
      icon: 'icon-badge',
      isVisible: true,
      routerLink: ['/post-sale'],
      title: 'Tin Đăng - Bán'
    },
    {
      icon: 'icon-badge',
      isVisible: true,
      routerLink: ['/post-buy'],
      title: 'Tin Đăng - Mua'
    },
    {
      icon: 'icon-user',
      isVisible: true,
      routerLink: ['/user'],
      title: 'User'
    },
    {
      icon: 'icon-ghost',
      isVisible: this.sessionService.user.email === 'master@hecta.vn',
      routerLink: ['/admin'],
      title: 'Admin'
    },
    {
      icon: 'icon-rocket',
      isVisible: true,
      routerLink: ['/priority'],
      title: 'Priority'
    },
    {
      icon: 'icon-calculator',
      isVisible: true,
      routerLink: ['/search-box'],
      title: 'Searchbox'
    },
    {
      icon: 'icon-tag',
      isVisible: true,
      routerLink: ['/tag'],
      title: 'Tag'
    }
  ];
}
