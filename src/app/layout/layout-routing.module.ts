import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { CanActivateRoute } from '../shared/services/canActivateRoute.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LayoutComponent } from './layout.component';

const routes: Route[] = [
  {
    path: '',
    component: LayoutComponent,
    canActivateChild: [CanActivateRoute],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'project',
        loadChildren: 'src/app/layout/components/project/project.module#ProjectModule'
      },
      {
        path: 'news',
        loadChildren: 'src/app/layout/components/news/news.module#NewsModule'
      },
      {
        path: 'post-sale',
        loadChildren: 'src/app/layout/components/post-sale/post-sale.module#PostSaleModule'
      },
      {
        path: 'post-buy',
        loadChildren: 'src/app/layout/components/post-buy/post-buy.module#PostBuyModule'
      },
      {
        path: 'user',
        loadChildren: 'src/app/layout/components/user/user.module#UserModule'
      },
      {
        path: 'admin',
        loadChildren: 'src/app/layout/components/admin/admin.module#AdminModule'
      },
      {
        path: 'priority',
        loadChildren: 'src/app/layout/components/priority/priority.module#PriorityModule'
      },
      {
        path: 'search-box',
        loadChildren: 'src/app/layout/components/searchbox/searchbox.module#SearchBoxModule'
      },
      {
        path: 'tag',
        loadChildren: 'src/app/layout/components/tag/tag.module#TagModule'
      },
      {
        path: '**',
        redirectTo: 'dashboard'
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: [CanActivateRoute]
})

export class LayoutRoutingModule { }
