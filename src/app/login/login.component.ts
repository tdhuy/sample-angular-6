import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import CookieNames from '../shared/constants/cookies';
import { AuthService } from '../shared/services/auth.service';
import { GlobalService } from '../shared/services/global.service';
import { SessionService } from '../shared/services/session.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements AfterViewInit {

  env = environment;

  error = {
    login: []
  };

  // init credentials
  login = {
    username: '',
    password: ''
  };

  // default show form login
  showFormHtml = 'LOGIN';

  constructor(private authService: AuthService,
              private globalService: GlobalService,
              private sessionService: SessionService,
              private location: Location,
              private router: Router) {

  }

  loginFn(): void {
    const dataLogin = {
      username: this.login.username,
      password: this.login.password
    };

    this.authService.login(dataLogin)
      .subscribe((res: any) => {

        if (res.status !== 1) {
          this.error.login = [
            'Username hoặc password sai'
          ];

          return;
        }

        const token = res.data['token'];
        delete res.data['token'];

        this.globalService.loggedInUser = res.data;
        this.sessionService.set(token, res.data);
        // this.sessionService.set(CookieNames.user, res.data);
        // this.globalService.setFullPageLoading(true);
        this.globalService.setFullPageLoadingLogin(true);
        this.router.navigate(['/dashboard']);
      });
  }

  ngAfterViewInit() {
    if (this.sessionService.token && this.sessionService.user) {
      setTimeout(() => {
        this.globalService.setFullPageLoading(true);
      });
      setTimeout(() => {
        this.location.forward();
      }, 1000);
    } else {
      this.globalService.setFullPageLoading(false);
    }
  }
}
