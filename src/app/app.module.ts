import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Injector } from '@angular/core';
import { BsModalService, ModalModule } from 'ngx-bootstrap';
import { AlertModule } from './alert/alert.module';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, FormBuilder } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CookieModule, CookieService } from 'ngx-cookie';
import { AgmCoreModule } from '@agm/core';
import { AppComponent } from './app.component';
import { SpinnerToLoginModule } from './shared/components/spinner-to-login/spinner-to-login.module';
import { SpinnerModule } from './shared/components/spinner/spinner.module';
import { IcheckDirective } from './shared/directives/icheck';
import { AuthService } from './shared/services/auth.service';
import { GlobalService } from './shared/services/global.service';
import { SessionService } from './shared/services/session.service';
import { APP_BASE_HREF } from '@angular/common';
import { TokenInterceptor } from './shared/services/token-interceptor.service';
import { ValidatorsService } from './shared/services/validator.service';
import { PostService } from './shared/services/post.service';
import { ServiceLocator } from './shared/services/service-locator';
import { StringHelperService } from './shared/services/string-helper.service';

@NgModule({
  declarations: [
    AppComponent,
    IcheckDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    CookieModule.forRoot(),
    AppRoutingModule,
    AgmCoreModule.forRoot(),
    ModalModule.forRoot(),
    SpinnerModule,
    SpinnerToLoginModule,
    HttpClientModule,
    AlertModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    GlobalService,
    CookieService,
    SessionService,
    BsModalService,
    AuthService,
    PostService,
    ValidatorsService,
    StringHelperService,
    FormBuilder,
    {provide: APP_BASE_HREF, useValue: '/'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    ServiceLocator.injector = this.injector;
  }
}
