import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { InputTextComponent } from '../input-text/input-text.component';

@Component({
  selector: 'app-input-password',
  templateUrl: './input-password.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputPasswordComponent)
    }
  ]
})

export class InputPasswordComponent extends InputTextComponent {
  type = 'password';
}
