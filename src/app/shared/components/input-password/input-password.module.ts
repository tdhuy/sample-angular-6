import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { InputPasswordComponent } from './input-password.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MoleculeContainerModule
  ],
  declarations: [InputPasswordComponent],
  exports: [InputPasswordComponent]
})
export class InputPasswordModule {}
