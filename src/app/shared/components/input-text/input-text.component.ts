import { Component, EventEmitter, Input, Output, forwardRef, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputTextComponent)
    }
  ]
})
export class InputTextComponent extends BaseComponent implements ControlValueAccessor {
  type = 'text';

  _value = '';

  @Input()
  set value(v: string) {
    this._value = v;
  }

  get value(): string {
    return this._value;
  }

  @Output() valueChanged = new EventEmitter<string>();

  writeValue(obj: any) {
    this.value = obj;
  }

  updateModel() {
    this.onModelChange(this.value);
  }

  onValueChanged() {
    this.updateModel();
    this.valueChanged.emit(this.value);
  }

  onGotFocus() {
    super.onGotFocus();
  }

  onLostFocus() {
    super.onLostFocus();
    this.onModelTouched();
  }
}
