import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { InputTextComponent } from './input-text.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MoleculeContainerModule
  ],
  declarations: [InputTextComponent],
  exports: [InputTextComponent]
})
export class InputTextModule {}
