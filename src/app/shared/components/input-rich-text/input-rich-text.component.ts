import { Component, EventEmitter, Input, Output, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { StringHelperService } from '../../services/string-helper.service';

declare const CKEDITOR: any;

@Component({
  selector: 'app-input-rich-text',
  templateUrl: './input-rich-text.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputRichTextComponent)
    }
  ]
})

export class InputRichTextComponent extends BaseComponent implements ControlValueAccessor, AfterViewInit, OnDestroy, OnInit {
  _id = this.stringHelper.genRandomString();
  _value = '';
  editor = null;

  @Input()
  set value(v: string) {
    this._value = v;

    if (this.editor) {
      if (this.editor.getData() !== v) {
        this.editor.setData(v);
      }
    } else {
      this._setValue(this.value);
    }
  }

  get value(): string {
    return this._value;
  }

  @Output() valueChanged = new EventEmitter<string>();

  constructor(private stringHelper: StringHelperService) {
    super();
  }

  private _setValue(v: string) {
    setTimeout(() => {
      this.editor.setData(v);
    }, 200);
  }

  onValueChanged() {
    this.updateModel();
    this.valueChanged.emit(this.value);
  }

  updateModel() {
    this.onModelChange(this.value);
    this.valueChanged.emit(this.value);
  }

  writeValue(obj: any) {
    this.value = obj;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.editor = CKEDITOR.replace(this._id.toString());
      this.editor.on('change', (evt) => {
        this.value = evt.editor.getData();
        this.updateModel();
      });

      let count = 0;
      setInterval(() => {
        if (this.editor && this.editor.getData() === '' && count < 5) {
          this.editor.setData(this.value);
          count++;
        }
      }, 1000);
    }, 100);
  }

  ngOnDestroy() {
    if (CKEDITOR.instances[this._id.toString()]) {
      CKEDITOR.instances[this._id.toString()].destroy();
      this.editor = null;
    }
  }

  ngOnInit() {
  }
}
