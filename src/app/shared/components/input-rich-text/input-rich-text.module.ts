import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EditorModule } from 'primeng/primeng';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { InputRichTextComponent } from './input-rich-text.component';

@NgModule({
  declarations: [
    InputRichTextComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MoleculeContainerModule,
    EditorModule
  ],
  exports: [
    InputRichTextComponent
  ]
})


export class InputRichTextModule {}
