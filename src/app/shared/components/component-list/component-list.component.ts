import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { File } from '../../constants/file-type';
import { AlertService } from '../../services/alert.service';
import { GlobalService } from '../../services/global.service';
import { RadioGroupComponent } from '../radio-group/radio-group.component';
import Types = File.Types;

@Component({
  selector: 'app-molecule-container',
  templateUrl: './component-list.component.html',
  providers: [
    AlertService
  ]
})

export class ComponentListComponent implements OnInit, AfterViewInit {
  imageTypes = [Types.png];

  selectTextItemsSource: { id: string | number, text: string }[] = [
    {id: 1, text: 'OMG 1'},
    {id: 2, text: 'OMG 2'},
    {id: 3, text: 'OMG 3'},
    {id: 4, text: 'OMG 4'},
    {id: 5, text: 'OMG 5'},
    {id: 6, text: 'OMG 6'},
    {id: 7, text: 'OMG 7'},
    {id: 8, text: 'OMG 8'},
    {id: 9, text: 'OMG 9'},
    {id: 10, text: 'OMG 10'}
  ];

  brand = {
    itemsSource: []
  };

  radioGroupCfg = {
    displayPath: 'name',
    valuePath: 'value',
    itemsSource: [
      {
        name: '1',
        value: '1'
      },
      {
        name: '2',
        value: '2'
      },
      {
        name: '3',
        value: '3'
      }
    ]
  };

  form: FormGroup;

  @ViewChild('radioGroup') radioGroupEl: RadioGroupComponent;

  onSelectText(value) { }

  onInputDateChange(value) { }

  onInputPeriodChange(value) { }

  constructor(private globalService: GlobalService,
              private fb: FormBuilder,
              private alertService: AlertService) {
    this.globalService.setFullPageLoading(false);
  }

  ngOnInit() {
    this.form = this.fb.group({
      images: [[]],
      richText2: ['', []],
    });

    this.form.valueChanges.subscribe(value => {
      console.log(value);
    });
  }

  ngAfterViewInit() {

  }

  openDialogSuccess() {
    this.alertService.showSuccess(['Success']);
  }

  openDialogConfirm() {
    this.alertService.confirm(['Hello world'], () => {});
  }

  openDialogError() {
    this.alertService.error(['Hello world']);
  }
}
