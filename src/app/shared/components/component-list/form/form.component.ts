import { Component, OnInit } from "@angular/core";
import { GlobalService } from "../../../services/global.service";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
	selector: 'app-component-list-form',
	templateUrl: './form.component.html'
})

export class FormComponent implements OnInit {
	form: FormGroup;

	constructor(
		private globalService: GlobalService,
		private fb: FormBuilder
	) {
		setTimeout(() => {
			this.globalService.setFullPageLoading(false);
		});
	}

	ngOnInit() {
		this.initForm();
	}

	submitForm() {
	}

	onAgeChange(value) {
	}

	private initForm() {
		this.form = this.fb.group({
			name: [''],
			age: [25]
		});
	}

}
