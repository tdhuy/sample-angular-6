import { NgModule } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { AlertModule } from '../../../alert/alert.module';
import { ConfirmAlertModalComponent } from '../../../alert/confirm-alert-modal/confirm-alert-modal.component';
import { ErrorAlertModalComponent } from '../../../alert/error-alert-modal/error-alert-modal.component';
import { SuccessAlertModalComponent } from '../../../alert/success-alert-modal/success-alert-modal.component';
import { StringHelperService } from '../../services/string-helper.service';
import { ImageUploaderModule } from '../image-uploader/imageUploader.module';
import { InputDatePeriodModule } from '../input-date-period/input-date-period.module';
import { InputDateModule } from '../input-date/input-date.module';
import { InputNumberModule } from '../input-number/input-number.module';
import { InputRichText2Module } from '../input-rich-text-2/input-rich-text-2.module';
import { InputRichTextModule } from '../input-rich-text/input-rich-text.module';
import { InputTextModule } from '../input-text/input-text.module';
import { InputTextareaModule } from '../input-textarea/input-textarea.module';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { RadioGroupModule } from '../radio-group/radio-group.module';
import { SectionModule } from '../section/section.module';
import { SelectSearchMultipleModule } from '../select-search-multiple/select-search-multiple.module';
import { SelectSearchModule } from '../select-search/select-search.module';
import { SelectTextModule } from '../select-text/select-text.module';
import { ComponentListRoutingModule } from './component-list-routing.module';
import { ComponentListComponent } from './component-list.component';
import { FormComponent } from './form/form.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoInputText } from './demo-input-text/demo-input-text.component';
import { MediaSelectionModule } from '../media-selection/media-selection.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentListRoutingModule,
    MoleculeContainerModule,
    SectionModule,
    InputTextModule,
    InputNumberModule,
    InputTextareaModule,
    InputRichTextModule,
    SelectTextModule,
    InputDateModule,
    InputDatePeriodModule,
    RadioGroupModule,
    SelectSearchModule,
    SelectSearchMultipleModule,
    AlertModule,
    ImageUploaderModule,
    InputRichText2Module,
    MediaSelectionModule
  ],
  declarations: [
    ComponentListComponent,
    FormComponent,
    DemoInputText
  ],
  entryComponents: [
    ConfirmAlertModalComponent,
    ErrorAlertModalComponent,
    SuccessAlertModalComponent
  ],
  providers: [
    BsModalService,
    StringHelperService
  ]
})
export class ComponentListModule { }
