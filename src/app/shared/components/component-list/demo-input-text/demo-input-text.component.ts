import { Component, forwardRef, Input, Output, EventEmitter } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { ControlValueAccessor } from "@angular/forms/src/directives/control_value_accessor";

@Component({
  selector: 'demo-input-text',
  templateUrl: './demo-input-text.component.html',
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => DemoInputText),
    }
  ]
})

export class DemoInputText implements ControlValueAccessor {
  _value = '';

  @Input()
  set value(v: string) {
    this._value = v;
  }

  get value(): string{
    return this._value;
  }

  private onModelChange = (_: any) => {};

  private onModelTouched = () => {};

  @Output() valueChanged = new EventEmitter<string>();

  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onModelTouched = fn;
  }

  writeValue(obj: any) {
    this.value = obj; 
  }

  updateModel() {
    this.onModelChange(this.value);
  }

  onTextChange(value: string) {
    this.updateModel();
    this.valueChanged.emit(this.value);
  }
 }