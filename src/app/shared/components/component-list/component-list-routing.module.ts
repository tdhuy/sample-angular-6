import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ComponentListComponent } from './component-list.component';
import { FormComponent } from './form/form.component';

const routes: Route[] = [
  {
    path: '', component: ComponentListComponent, pathMatch: 'full'
  },
  {
    path: 'form', component: FormComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})

export class ComponentListRoutingModule {}
