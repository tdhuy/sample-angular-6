import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FilterType, ITableColumn, SelectValuesType } from '../table-control/iTableColumn';

const UPDATE_OBJ_TYPES = {
  'UPDATE': 'UPDATE',
  'REMOVE': 'REMOVE'
};

@Component({
  selector: '[table-filter]',
  templateUrl: './table-header-filterable.component.html',
  providers: [],
  changeDetection: ChangeDetectionStrategy.Default
})

export class TableHeaderFilterableComponent implements OnDestroy {
  _columns: ITableColumn[] = [];
  _filter: { [key: string]: string } = {};
  _filterTypes = FilterType;
  _selectValuesType = SelectValuesType;

  @Input() showIndexCol = false;

  @Input()
  set columns(cols: ITableColumn[]) {
    this._columns = cols;
  }

  get columns(): ITableColumn[] {
    return this._columns;
  }

  @Output() filterChanged = new EventEmitter<any>();

  onInputFieldChange($event) {
    const type = $event.textValue === '' ? UPDATE_OBJ_TYPES.REMOVE : UPDATE_OBJ_TYPES.UPDATE;
    this._updateFilterObject($event.column, $event.textValue, type);
  }

  onSelectFieldChange($event) {
    const type = $event.selectedValue === 'NONE' ? UPDATE_OBJ_TYPES.REMOVE : UPDATE_OBJ_TYPES.UPDATE;
    this._updateFilterObject($event.column, $event.selectedValue, type);
  }

  private _updateFilterObject(key, value, type) {
    switch (type) {
      case UPDATE_OBJ_TYPES.UPDATE:
        this._filter[key] = value;
        break;
      case UPDATE_OBJ_TYPES.REMOVE:
        delete this._filter[key];
        break;
    }

    this._onFilterChanged();
  }

  private _onFilterChanged() {
    this.filterChanged.emit(this._filter);
  }

  ngOnDestroy() {

  }
}
