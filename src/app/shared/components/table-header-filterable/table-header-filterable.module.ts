import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {TableFilterInputModule} from '../table-filter-input/table-filter-input.module';
import {TableFilterSelectModule} from '../table-filter-select/table-filter-select.module';
import {TableHeaderFilterableComponent} from './table-header-filterable.component';
import {TableFilterSelect2Module} from '../table-filter-select-2/table-filter-select-2.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TableFilterSelectModule,
    TableFilterInputModule,
    TableFilterSelect2Module
  ],
  declarations: [TableHeaderFilterableComponent],
  exports: [TableHeaderFilterableComponent]
})
export class TableHeaderFilterableModule {
}
