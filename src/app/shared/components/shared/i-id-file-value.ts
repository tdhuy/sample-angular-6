import { IFileValue } from './i-file-value';

export interface IIdFileValue {
  data: IFileValue;

  id: string;
}
