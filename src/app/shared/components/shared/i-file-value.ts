export interface IFileValue {
  file: any;

  base64: string;

  url?: string;
}
