export interface ITextProperty {
  title: string;
  tooltip: string;
  infos: string[];
  placeholder: string;
}
