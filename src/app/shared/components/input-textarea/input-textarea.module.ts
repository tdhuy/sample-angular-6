import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { InputTextareaComponent } from './input-textarea.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MoleculeContainerModule
  ],
  declarations: [
    InputTextareaComponent
  ],
  exports: [
    InputTextareaComponent
  ]
})
export class InputTextareaModule {}
