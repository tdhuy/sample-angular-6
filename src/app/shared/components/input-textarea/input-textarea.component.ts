import { Component, EventEmitter, Input, Output, forwardRef } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@angular/forms/src/directives/control_value_accessor';

@Component({
  selector: 'app-input-textarea',
  templateUrl: './input-textarea.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputTextareaComponent),
    }
  ]
})

export class InputTextareaComponent extends BaseComponent implements ControlValueAccessor {
  _value = '';

  @Input() rows = 10;

  @Input()
  set value(v: string) {
    this._value = v;
  }

  get value(): string {
    return this._value;
  }

  @Output() valueChanged = new EventEmitter<string>();

  onValueChanged() {
    this.updateModel();
    this.valueChanged.emit(this._value);
  }

  updateModel() {
    this.onModelChange(this.value);
  }

  writeValue(obj: any) {
    this.value = obj;
  }
}
