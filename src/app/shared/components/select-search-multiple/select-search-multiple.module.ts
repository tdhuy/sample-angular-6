import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { SelectSearchMultipleComponent } from './select-search-multiple.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MoleculeContainerModule
  ],
  declarations: [
    SelectSearchMultipleComponent
  ],
  exports: [
    SelectSearchMultipleComponent
  ]
})

export class SelectSearchMultipleModule {}
