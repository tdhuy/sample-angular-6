import { Component, ElementRef, EventEmitter, HostListener, Input, Output, forwardRef } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BaseComponent } from '../base/base.component';
import { ISelectTextItem } from '../select-text/i-select-text-item';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-select-search-multiple',
  templateUrl: './select-search-multiple.component.html',
  styleUrls: ['./select-search-multiple.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SelectSearchMultipleComponent),
    }
  ]
})

export class SelectSearchMultipleComponent extends BaseComponent implements ControlValueAccessor {
  isOpened = false;
  _itemsSource: ISelectTextItem[] = [];
  _displayMemberPath = '';
  _selectedValuePath = '';
  _selectedItemObj: any = {};
  private _textSearchApi = new Subject<string>();
  private _txt = '';
  private _bufferItemsSource: ISelectTextItem[] = [];

  @Input()
  set itemsSource(values: ISelectTextItem[]) {
    this._itemsSource = values;
    this._bufferItemsSource = values;
    if (this.dataFlowType === 'API') {
      this._filterOutSelectedCaseAPI();
    }
  }

  get itemSource(): ISelectTextItem[] {
    return this._itemsSource;
  }

  @Input()
  set displayMemberPath(value: string) {
    this._displayMemberPath = value;
    this._propChanged();
  }

  @Input()
  set selectedValuePath(value: string) {
    this._selectedValuePath = value;
    this._propChanged();
  }

  @Input() dataFlowType: 'API' | 'FIX' = 'FIX';
  @Input() selectedItem: ISelectTextItem[] = [];

  @Output() requestData = new EventEmitter<string>();
  @Output() valueChanged = new EventEmitter<ISelectTextItem[]>();

  constructor(private el: ElementRef) {
    super();

    this._textSearchApi
      .debounceTime(200)
      .distinctUntilChanged()
      .subscribe(value => {
        this.requestData.emit(value);
      });
  }

  @HostListener('document:click', ['$event'])
  handleClick(event: Event) {
    if (!this.el.nativeElement.querySelector('.select-search').contains(event.target)) {
      this.isOpened = false;
    }
  }

  onSelectItem(event, item) {
    event.stopPropagation();
    const exist = this._selectedItemObj[item[this._selectedValuePath]];
    if (!exist) {
      this.selectedItem.push(item);
      this.updateModel();
      this._selectedItemObj[item[this._selectedValuePath]] = true;
      this._broadcastSelectedItemsChanged();
      setTimeout(() => {
        this._handleGetItemsSource();
      }, 10);
    }
  }

  open() {
    this.isOpened = !this.isOpened;
  }

  onTextChanged($event) {
    this._txt = $event.target.value;
    this._handleGetItemsSource();
  }

  trackByFn(index, item: ISelectTextItem) {
    return item.id;
  }

  removeSelected(event, item) {
    event.stopPropagation();
    const newSelectedItems = this.selectedItem.filter((i) => {
      return i[this._selectedValuePath] !== item[this._selectedValuePath];
    });

    this.selectedItem = newSelectedItems;
    this.updateModel();
    this._selectedItemObj[item[this._selectedValuePath]] = false;
    this._handleGetItemsSource();
    this._broadcastSelectedItemsChanged();
  }

  writeValue(obj: any) {
    this.selectedItem = obj;
  }

  updateModel() {
    this.onModelChange(this.selectedItem);
  }

  private _broadcastSelectedItemsChanged() {
    this.valueChanged.emit(this.selectedItem);
  }

  private _propChanged() { }

  private _handleGetItemsSource() {
    if (this.dataFlowType === 'FIX') {
      this._filterItemsSource();
    } else if (this.dataFlowType === 'API') {
      this._textSearchApi.next(this._txt);
      this._filterOutSelectedCaseAPI();
    }
  }

  private _filterItemsSource() {
    this._itemsSource = this._bufferItemsSource.filter(item => {
      const itemTxt = item.text.toLowerCase();
      return itemTxt.indexOf(this._txt.toUpperCase()) !== -1 && !this._selectedItemObj[item[this._selectedValuePath]];
    });
  }

  private _filterOutSelectedCaseAPI() {
    this._itemsSource = this._bufferItemsSource.filter(item => {
      return !this._selectedItemObj[item[this._selectedValuePath]];
    });
  }
}
