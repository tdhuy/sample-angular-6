import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { ImageUploaderComponent } from './imageUploader.component';

@NgModule({
  declarations: [ImageUploaderComponent],
  imports: [
    CommonModule,
    FormsModule,
    MoleculeContainerModule
  ],
  exports: [
    ImageUploaderComponent
  ]
})

export class ImageUploaderModule {

}
