import { HttpClient } from '@angular/common/http';
import {
  AfterContentInit,
  Component,
  DebugElement,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  Output, ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { APIs } from '../../constants/api';
import { StringHelperService } from '../../services/string-helper.service';
import { BaseComponent } from '../base/base.component';

const IMAGE_UPLOADER_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => ImageUploaderComponent),
  multi: true
};

interface IFile {
  elId: string;
  file: any;
  percentUploaded: number;
  result: any;
  url: string;
  text?: string;
}

export interface IFileTextValue {
  id: string;
  text: string;
}

@Component({
  templateUrl: './imageUploader.component.html',
  selector: 'app-image-uploader',
  styleUrls: ['./imageUploader.component.scss'],
  providers: [IMAGE_UPLOADER_VALUE_ACCESSOR]
})

export class ImageUploaderComponent extends BaseComponent implements AfterContentInit, ControlValueAccessor {
  _files: IFile[] = [];
  _values: IFileTextValue[] = [];
  private _acceptTypes = ['png'];

  @Input()
  set values(values: IFileTextValue[]) {
    this._values = values;
    this._files = this.initFileObjectsFromValues(values);
  }

  get values(): IFileTextValue[] {
    return this._values;
  }

  @Input() max = -1;
  @Input() maxSize = 1024 * 2; // 2mb
  @Input() showInput = false;

  @Input()
  set types(types: string[]) {
    this._acceptTypes = types;
    this._updateAcceptTypes();
  }

  get types(): string[] {
    return this._acceptTypes;
  }

  @Output() valueChanged = new EventEmitter<IFileTextValue[]>();
  @ViewChild('inputFile') inputFile: DebugElement;

  constructor(private el: ElementRef,
              private stringHelpser: StringHelperService,
              private http: HttpClient) {
    super();
  }

  private _updateAcceptTypes() {
    const acceptTypes = this._acceptTypes.map(t => {
      return `image/${t}`;
    });

    this.inputFile.nativeElement.accept = acceptTypes.join(',');
  }

  ngAfterContentInit() {

  }

  writeValue(obj: any): void {
    this.values = obj;
  }

  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onModelTouched = fn;
  }

  updateModel() {
    this.values = this._files.filter(f => f.result)
      .map(f => {
        return {
          id: f.result._id,
          text: f.text
        };
      });

    this.valueChanged.emit(this.values);
    this.onModelChange(this.values);
  }

  onChangeInputFiles(event: any) {
    let selectedFiles = event.target.files;
    if (selectedFiles.length === 0) {
      return;
    }

    if (this._existFileTooLarge(selectedFiles)) {
      this.resetValueOfInput();
      return;
    }

    if (this.max !== -1 && this._files.length + selectedFiles.length > this.max) {
      selectedFiles = selectedFiles.slice(0, this.max);
      this.resetValueOfInput();
      return;
    }

    const waitingImages = this.generateObjectImage(selectedFiles);

    this._files = [
      ...this._files,
      ...waitingImages
    ];

    if (waitingImages.length > 0) {
      setTimeout(() => {
        this._renderNewImages(waitingImages);
      }, 100);
      this.resetValueOfInput();
    }
  }

  trackBy(index, item) {
    return item.elId;
  }

  removeFile(file: IFile) {
    const index = this.findIndexFile(file);
    if (index !== -1) {
      this._files.splice(index, 1);
      this.updateModel();
    }
  }

  onTextChange() {
    this.updateModel();
  }

  private generateObjectImage(files: any): IFile[] {
    const results: IFile[] = [];

    for (const key in files) {
      if (files.hasOwnProperty(key)) {
        results.push({
          elId: this.stringHelpser.genRandomString(20),
          file: files[key],
          percentUploaded: 0,
          result: null,
          url: '',
          text: ''
        });
      }
    }

    return results;
  }

  private findIndexFile(file: IFile) {
    return this._files.findIndex(f => f.elId === file.elId);
  }

  private resetValueOfInput() {
    this.inputFile.nativeElement.value = '';
  }

  private _renderNewImages(newImages: IFile[]) {
    newImages.forEach(imgFile => {
      this._uploadImage(imgFile);
    });
  }

  private _existFileTooLarge(selectedFiles: any): boolean {
    // TODO:
    return false;
  }

  private _uploadImage(imgFile: IFile) {
    const index = this.findIndexFile(imgFile);

    if (index === -1) {
      return;
    }

    const formData = new FormData();
    formData.append('file', imgFile.file);

    this.http.post(APIs.uploadImage, formData)
      .subscribe((res: any) => {
        if (res.status === 1) {
          this._files[index].url = APIs.getImage.replace('{id}', res.data._id);
          this._files[index].result = res.data;
          this.updateModel();
        }
      });
  }

  private initFileObjectsFromValues(values: IFileTextValue[]): IFile[] {
    return values.map((v: IFileTextValue) => {
      return <IFile>{
        elId: this.stringHelpser.genRandomString(20),
        file: null,
        percentUploaded: 0,
        result: {
          _id: v.id
        },
        text: v.text,
        url: APIs.getImage.replace('{id}', v.id)
      }
    });
  }
}
