import { NgModule } from '@angular/core';
import { TableFooterComponent } from './table-footer.component';
import { PaginationModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [TableFooterComponent],
  imports: [
    FormsModule,
    CommonModule,
    PaginationModule.forRoot()
  ],
  exports: [TableFooterComponent]
})

export class TableFooterModule { }
