import { Component, Input, Output, EventEmitter, AfterViewInit, ElementRef, AfterContentInit } from '@angular/core';

@Component({
  selector: 'app-table-control-footer',
  templateUrl: './table-footer.component.html'
})

export class TableFooterComponent implements AfterViewInit, AfterContentInit {
  _page = 1;
  _size = 10;
  _total = 100;
  _text = {
    from: 0,
    to: 0
  };
  _isVisiblePaginationBtns = true;
  _flagResetPage = false;

  @Input()
  set render(value) {
    this._init();
  }

  /**
   *
   */
  @Input()
  set page(value: number) {
    this._page = value;
  }

  get page(): number {
    return this._page;
  }

  /**
   *
   */
  @Input()
  set size(value: number) {
    this._size = value;
  }

  get size(): number {
    return this._size;
  }

  /**
   *
   */
  @Input()
  set total(value: number) {
    this._total = value;
    this._changeTextFromTo();
  }

  get total(): number {
    return this._total;
  }

  @Input() sizeValues: number[] = [];

  @Output() pageChanged = new EventEmitter<Object>();

  constructor(private el: ElementRef) {

  }

  private _changeTextFromTo() {
    this._text.from = (this._page - 1) * this._size + 1;

    if (this._page * this._size < this._total) {
      this._text.to = this._page * this._size;
    } else {
      this._text.to = this._total;
    }
  }

  private _init() {

  }

  onChangedSize(value: any) {
    this._flagResetPage = true;
    setTimeout(() => {
      this._page = 1;
      const data = {
        size: this._size,
        page: this._page
      };
      this.pageChanged.emit(data);
    }, 0);
  }

  onChangedPage(obj: any) {
    if (this._flagResetPage === true) {
      this._flagResetPage = false;
      return;
    }

    this._page = obj.page;
    const data = {
      size: this._size,
      page: this._page
    };
    this.pageChanged.emit(data);
    this._changeTextFromTo();
  }

  ngAfterViewInit() {

  }

  ngAfterContentInit() {
    setTimeout(() => {
      this._changeTextFromTo();
    }, 0);
  }
}
