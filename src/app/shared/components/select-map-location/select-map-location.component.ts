import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IMapLocation } from './i-map-location';

@Component({
  selector: 'app-select-map-location',
  templateUrl: './select-map-location.component.html'
})

export class SelectMapLocationComponent {
  _location: IMapLocation = {latitude: 0, longitude: 0};

  @Input()
  set value(s: IMapLocation) {
    this._location = s;
  }

  get value(): IMapLocation {
    return this._location;
  }

  @Output() valueChanged = new EventEmitter<IMapLocation>();

  mapClick($event): void {
    this._location.latitude = $event.coords.lat;
    this._location.longitude = $event.coords.lng;

    this.valueChanged.emit(this._location);
  }
}

