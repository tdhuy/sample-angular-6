export interface IMapLocation {
  latitude: number;
  longitude: number;
}
