import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SelectMapLocationComponent } from './select-map-location.component';

@NgModule({
  declarations: [
    SelectMapLocationComponent
  ],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBPP9wntlCwMk-pieV7psplTSD4qDmWnkU'
    })
  ],
  exports: [
    SelectMapLocationComponent
  ]
})

export class SelectMapLocationModule {}
