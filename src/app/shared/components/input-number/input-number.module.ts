import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { InputNumberComponent } from './input-number.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MoleculeContainerModule,
    CurrencyMaskModule
  ],
  declarations: [
    InputNumberComponent
  ],
  exports: [
    InputNumberComponent
  ]
})
export class InputNumberModule {}
