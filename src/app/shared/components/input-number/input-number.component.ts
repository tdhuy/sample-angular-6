import { Component, EventEmitter, Input, Output, forwardRef } from '@angular/core';
import { CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import { BaseComponent } from '../base/base.component';
import {} from 'angular2-text-mask';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  decimal: '.',
  precision: 2,
  prefix: '',
  suffix: '',
  thousands: ' '
};

@Component({
  selector: 'app-input-number',
  templateUrl: './input-number.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputNumberComponent),
    }
  ]
})

export class InputNumberComponent extends BaseComponent implements ControlValueAccessor {
  _value = 0;
  options = CustomCurrencyMaskConfig;
  @Output() valueChanged = new EventEmitter<number>();

  @Input()
  set value(value: number) {
    this._value = value;
  }

  get(): number {
    if (isNaN(this._value)) {
      return 0;
    }

    return this._value;
  }

  @Input()
  set precision(value: number) {
    this.options.precision = value;
  }

  onValueChanged() {
    this.updateModel();
    if (!isNaN(this._value)) {
      this.valueChanged.emit(this._value);
    } else {
      this.valueChanged.emit(0);
    }
  }

  writeValue(obj: any) {
    this._value = obj;
  }

  updateModel() {
    this.onModelChange(this._value);
  }
}
