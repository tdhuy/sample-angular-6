import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html'
})
export class SectionComponent {
  @Input() upperLabel = '';

  @Input()
  set jTextProperty({upperLabel}: { upperLabel: string }) {
    this.upperLabel = upperLabel;
  }
}
