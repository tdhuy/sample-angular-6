import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MoleculeContainerComponent } from './molecule-container.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MoleculeContainerComponent],
  exports: [MoleculeContainerComponent]
})

export class MoleculeContainerModule {}
