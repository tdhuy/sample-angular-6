import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-molecule-container',
  templateUrl: './molecule-container.component.html',
  styleUrls: ['./molecule-container.component.scss']
})

export class MoleculeContainerComponent {
  @Input() title: string;
  @Input() tooltip: string;
  @Input() infos: string[];
  @Input() errors: string[];
  @Input() warnings: string[];
  @Input() isRequired: boolean;
  @Input() isEnabled: boolean;
  @Input() leftClass = '';
  @Input() rightClass = '';
}
