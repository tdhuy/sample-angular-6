import { HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { ITextProperty } from '../shared/i-text-property';

export class BaseComponent {

  _left = 'col-sm-3';
  _right = 'col-sm-9';

  @Input()
  @HostBinding('id')
  id = '';

  @Input() title = '';
  @Input() tooltip = '';
  @Input() placeholder = '';
  @Input() infos: string[] = [];
  @Input() warnings: string[] = [];
  @Input() errors: string[] = [];
  @Input() isRequired = false;
  @Input() isDisabled = false;
  @Input() isReadOnly = false;

  @Input()
  set left(col: number) {
    this._left = `control-label col-sm-${col}`;
  }

  @Input()
  set right(col: number) {
    this._right = `col-sm-${col}`;
  }

  @Input()
  set textProperty(value: ITextProperty) {
    if (value) {
      this.title = value.title;
      this.tooltip = value.tooltip;
      this.infos = value.infos;
      this.placeholder = value.placeholder;
    }
  }

  @Output() gotFocus: EventEmitter<FocusEvent | any> = new EventEmitter<FocusEvent>();
  @Output() lostFocus: EventEmitter<FocusEvent | any> = new EventEmitter<FocusEvent>();

  protected onModelChange = (_: any) => {};
  protected onModelTouched = () => {};

  onGotFocus() {
    this.gotFocus.emit({target: this});
  }

  onLostFocus() {
    this.lostFocus.emit({target: this});
  }

  isEnabled() {
    return !this.isDisabled && !this.isReadOnly;
  }

  hasWarning() {
    return this.isEnabled() && this.warnings !== undefined && this.warnings.length >= 1;
  }

  hasError() {
    return this.isEnabled() && this.errors !== undefined && this.errors.length >= 1;
  }

  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onModelTouched = fn;
  }

}
