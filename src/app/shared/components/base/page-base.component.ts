import { OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AlertService } from '../../services/alert.service';
import { ServiceLocator } from '../../services/service-locator';
import { StringHelperService } from '../../services/string-helper.service';
import { GlobalService } from '../../services/global.service';
import { SessionService } from '../../services/session.service';

export abstract class PageBaseComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];

  protected alertService: AlertService;
  protected stringHelperService: StringHelperService;
  protected globalService: GlobalService;
  protected sessionService: SessionService;

  constructor() {
    this.alertService = ServiceLocator.injector.get(AlertService);
    this.stringHelperService = ServiceLocator.injector.get(StringHelperService);
    this.globalService = ServiceLocator.injector.get(GlobalService);
    this.sessionService = ServiceLocator.injector.get(SessionService);
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
