export interface IRequestParams {
  page?: number;

  size?: number;

  sortBy?: string;

  sortDirection?: 'ASC' | 'DESC';

  filter?: {
    [key: string]: string | number
  };
}
