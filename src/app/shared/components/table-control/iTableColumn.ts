/**
 * Filter type:
 * + STRING and NUMBER will show input text
 * + SELECT will show a dropdown menu user can search and choose
 */
import { Observable } from 'rxjs/Observable';

/**
 * Use when config SelectValuesType.SEARCH.
 * This config to detect which API will call
 */
export enum TargetType {
  'DEAL',
  'BRAND',
  'STORE'
}

/**
 * Type of filter
 */
export enum FilterType {
  'STRING',
  'NUMBER',
  'SELECT'
}

/**
 * When filterConfig.dataType = 'SELECT'. Which type data will be show
 * + FIXED: array of value not changed.
 * + SEARCH: use api to get array of value
 */
export enum SelectValuesType {
  'FIXED',
  'SEARCH'
}

/**
 * How to show value at tbody cell.
 * + STRING: show normally
 * + CURRENCY: show as currency, need config unit currency
 * + NUMBER: show number, can round, 1000 split
 * + CUSTOM: You can config custom template to show value. Use 'template' or 'templateUrl' config
 */
export enum CellDisplay {
  'STRING',
  'CURRENCY',
  'USER_BALANCE_WITH_BTN',
  'NUMBER',
  'CUSTOM',
  'IMAGE',
  'DATE',
  'LIST_KEYWORD'
}

export type sortStates = 'NONE' | 'ASC' | 'DESC';

/**
 * CSS style on cell
 */
interface ICellStyle {
  [key: string]: string;
}

/**
 * When filterConfig.dataType='SELECT'. Option values should be.
 */
export interface IFixedOption {
  id: string;
  name: string;
  visibleToUpdate?: boolean;
}

export interface ITableColumn {

  code?: string;

  /**
   * Name of column, show on table thead tag
   */
  headerName: string;

  /**
   * How to format value when show data on table cell
   */
  displayValueAs: CellDisplay;

  /**
   * Path to get nested value from an object.
   * Ex: displayValuePath='properties.width' to get width value
   * {
   *    name: '',
   *    properties: {
   *      width: 100
   *    }
   * }
   */
  displayValuePath: string;

  /**
   * Can table sort by this column
   */
  sortable: boolean;

  sortByField?: string;

  /**
   * Can table filter by this column
   */
  filterable: boolean;

  /**
   * [Option] When filterable is TRUE, should config this field.
   */
  filterConfig?: {
    /**
     * Key query when make request to api search.
     * Ex: queryName='name' then request will have query params 'name' http://domain.com?name=abc
     */
    queryName: string;

    /**
     * Filter can filter by data type
     */
    dataType: FilterType;

    /**
     * How select options loaded
     */
    dataLoadBy?: SelectValuesType;

    /**
     *
     */
    targetType?: TargetType;
  };

  /**
   * Fixed options. When filterConfig.dataType = 'SELECT'
   */
  optionsFilter?: Observable<IFixedOption[]>;

  // content
  template?: string;
  templateUrl?: string;

  /**
   * [Option] Custom cell style at thead
   */
  headerStyle?: ICellStyle;

  /**
   * [Option] Custom cell style on tbody
   */
  contentStyle?: ICellStyle;

}
