import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ShareDirective } from '../../directives/share-directive';
import { SpinnerModule } from '../spinner/spinner.module';
import { TableControlComponent } from './table-control.component';
import { TableHeaderFilterableModule } from '../table-header-filterable/table-header-filterable.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableFooterModule } from '../table-footer/table-footer.module';
import { BsDropdownModule, TooltipModule } from 'ngx-bootstrap';
import { StringHelperService } from '../../services/string-helper.service';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    TableHeaderFilterableModule,
    TableFooterModule,
    BsDropdownModule.forRoot(),
    SpinnerModule,
    ShareDirective,
    TooltipModule.forRoot()
  ],
  declarations: [
    TableControlComponent
  ],
  exports: [TableControlComponent],
  providers: [
    StringHelperService
  ]
})

export class TableControlModule {}
