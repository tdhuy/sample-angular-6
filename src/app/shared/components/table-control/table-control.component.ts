import { DatePipe, DecimalPipe } from '@angular/common';
import {
  AfterViewInit, Component, Input, Output, EventEmitter, ViewChild, OnDestroy,
  ChangeDetectionStrategy, ChangeDetectorRef, AfterContentInit
} from '@angular/core';
import { environment } from '../../../../environments/environment';
import { CATEGORIES } from '../../constants/categories';
import { StickyTableTheadDirective } from '../../directives/sticky-table-thead.directive';
import { GlobalService } from '../../services/global.service';
import { StringHelperService } from '../../services/string-helper.service';
import { CellDisplay, FilterType, IFixedOption, ITableColumn, sortStates } from './iTableColumn';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { IRequestParams } from './iRequestParams';
import { TableFooterComponent } from '../table-footer/table-footer.component';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-table-control',
  templateUrl: './table-control.component.html',
  styleUrls: ['./table-control.component.scss'],
  providers: [DecimalPipe, DatePipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class TableControlComponent implements AfterViewInit, OnDestroy, AfterContentInit {
  CATES = CATEGORIES;
  _columns: ITableColumn[] = [];
  _sortBy = '';
  _sortState: sortStates = 'NONE';
  _itemsSource: any[] = [];
  _selectedItems: any[] = [];
  _page = 1;
  _size = 20;
  _sizeValues: number[] = [20, 50, 100, 200];
  _filterObj: { [key: string]: string | number } = {};
  _requestParams: IRequestParams = {};
  _subscription: Subscription[] = [];
  cellDisplay = CellDisplay;
  startRender = 0;
  visibleSetting = {
    spinner: false
  };
  sortObj: { [key: string]: boolean } = {};
  initSortObj: { [key: string]: boolean } = {};

  @Input() showIndexCol = false;
  @Input() totalRecords = 100;
  @Input() target = '';

  /**
   *
   */
  @Input()
  set page(value: number) {
    this._page = value;
  }

  get page(): number {
    return this._page;
  }

  /**
   *
   */
  @Input()
  set size(value: number) {
    this._size = value;
  }

  get size(): number {
    return this._size;
  }

  /**
   *
   */
  @Input()
  set columns(value: ITableColumn[]) {
    this._columns = value;
  }

  get columns(): ITableColumn[] {
    return this._columns;
  }

  /**
   *
   */
  @Input()
  set itemsSource(value: any[]) {
    this._itemsSource = value;
  }

  get itemsSource(): any[] {
    return this._itemsSource;
  }

  /**
   *
   */
  @Input()
  set sizeValues(values: number[]) {
    this._sizeValues = values;
  }

  get sizeValues(): number[] {
    return this._sizeValues;
  }

  @Input()
  set selectedItems(values: any[]) {
    this._selectedItems = values;
    if (values.length === 0) {
      this.onRenderDone();
    }
  }

  @Input()
  set showLoading(value: boolean) {
    if (value === false) {
      this.onRenderDone();
    }
  }

  get selectedItems(): any[] {
    return this._selectedItems;
  }

  @Output() requireData = new EventEmitter<IRequestParams>();
  @Output() rowSelected = new EventEmitter<number>();
  @Output() doAction = new EventEmitter<{ type: string, item: any, extra?: any }>();

  @ViewChild('tableFooter') tableFooter: TableFooterComponent;
  @ViewChild(StickyTableTheadDirective) stickyHeader: StickyTableTheadDirective;

  constructor(protected decimalPipe: DecimalPipe,
              protected datePipe: DatePipe,
              private cdt: ChangeDetectorRef,
              private globalService: GlobalService,
              private stringHelper: StringHelperService) {

  }

  _sort(column: ITableColumn) {

    if (column.sortable === false) {
      return;
    }

    this.sortObj = {...this.initSortObj};

    const field = column.sortByField || column.filterConfig.queryName;

    if (field === this._sortBy) {
      this._changeSortState();
    } else {
      this._sortBy = field;
      this._sortState = 'DESC';
    }

    if (this._sortState !== 'NONE') {
      this.sortObj[column.code] = true;
    }

    this._requestData();
  }

  _requestData() {
    setTimeout(() => {
      this.visibleSetting.spinner = true;
      // this.globalService.setFullPageLoading(true);
      this._createObjectRequestData();
      this.requireData.emit(this._requestParams);
    }, 0);
  }

  onFilterChanged(filterObj: { [key: string]: string | number }) {
    this._filterObj = filterObj;
    this._requestData();
  }

  onFooterChange(value: { page: number, size: number }) {
    this._page = value.page;
    this._size = value.size;
    this._requestData();
  }

  getImageUrlNested(item, column: ITableColumn) {
    const url = this._getValueNested(item, column.displayValuePath);
    return this.stringHelper.appendStaticPath(url);
  }

  getValueNested(item, column: ITableColumn) {
    let value = this._getValueNested(item, column.displayValuePath);
    switch (column.displayValueAs) {
      case CellDisplay.STRING:
        if (value === null || value === undefined) {
          value = '';
          break;
        }
        value = value.toString();
        break;

      case CellDisplay.NUMBER:
      case CellDisplay.USER_BALANCE_WITH_BTN:
      case CellDisplay.CURRENCY:
        if (!isNaN(value)) {
          value = this.decimalPipe.transform(value);
        }
        break;

      case CellDisplay.CUSTOM:
        break;

      case CellDisplay.DATE:
        if (value === null || value === undefined) {
          value = '';
          break;
        }

        if (!isNaN(value)) {
          const date = new Date(value);
          value = this.datePipe.transform(date, 'dd-MM-yyyy');
        }
        break;
    }

    return value;
  }

  fetchInitData(): Observable<any> {
    return Observable.of(1);
  }

  initComplete() {
    this._requestData();
  }

  ngAfterViewInit() {

  }

  ngAfterContentInit() {
    if (this.itemsSource.length === 0) {
      this.onRenderDone();
    }

    this.fetchInitData().subscribe(() => {
      this.initComplete();
    });

    this.columns.forEach(c => {
      this.sortObj[c.code] = false;
    });

    this.initSortObj = {...this.sortObj};
  }

  ngOnDestroy() {
    this._subscription.forEach((s) => {
      s.unsubscribe();
    });

    this.cdt.markForCheck();
    // this.cdt.detach();
  }

  onRenderDone() {
    this.startRender++;
    this.visibleSetting.spinner = false;

    if (!this.cdt['destroy']) {
      this.cdt.markForCheck();
    }
  }

  // actions
  removeItem(item) {
    this.doAction.emit({
      type: 'REMOVE',
      item
    });
  }

  updateStatus(item) {
    this.doAction.emit({
      type: 'UPDATE_STATUS',
      item
    });
  }

  schedulePublish(item) {
    this.doAction.emit({
      type: 'SCHEDULE_PUBLISH',
      item
    });
  }

  updateItem(item) {
    this.doAction.emit({
      type: 'UPDATE',
      item
    });
  }

  reply(item) {
    this.doAction.emit({
      type: 'REPLY_COMMENT',
      item
    });
  }

  editUserRole(item) {
    this.doAction.emit({
      type: 'UPDATE_ROLES',
      item
    });
  }

  resetPassword(item) {
    this.doAction.emit({
      type: 'RESET_PASSWORD',
      item
    });
  }

  changeUserBalance(item, col) {
    this.doAction.emit({
      type: 'USER_CHANGE_BALANCE',
      item,
      extra: {
        col
      }
    });
  }

  changeExpirationDate(item, col) {
    this.doAction.emit({
      type: 'USER_EXPIRATION_DATE',
      item,
      extra: {
        col
      }
    });
  }

  updateUserType(item) {
    this.doAction.emit({
      type: 'CHANGE_USER_TYPE',
      item
    });
  }

  private _changeSortState() {
    switch (this._sortState) {
      case 'NONE':
        this._sortState = 'DESC';
        break;
      case 'DESC':
        this._sortState = 'ASC';
        break;
      case 'ASC':
        this._sortState = 'NONE';
        break;
    }
  }

  private _createObjectRequestData() {
    const config: IRequestParams = {
      size: this._size,
      page: this._page,
      filter: Object.assign({}, this._filterObj)
    };

    if (this._sortBy !== '' && this._sortState !== 'NONE') {
      config.sortBy = this._sortBy;
      config.sortDirection = this._sortState;
    }

    if (Object.keys(config.filter).length === 0) {
      delete config.filter;
    }

    this._requestParams = config;
  }

  protected _getValueNested(obj: Object, keyStr: string): any {
    const keys = keyStr.split('.');
    let _obj = Object.assign({}, obj);
    keys.forEach((k) => {
      if (_obj === null || _obj === undefined) {
        return '';
      }

      _obj = _obj[k];
    });

    return _obj;
  }

}
