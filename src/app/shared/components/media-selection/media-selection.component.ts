import { Component, OnInit, ViewChild, ElementRef, Input } from "@angular/core";
import { BaseComponent } from "../base/base.component";

@Component({
  templateUrl: './media-selection.component.html',
  selector: 'app-media-selection',
  styleUrls: ['./media-selection.component.scss']
})

export class MediaSelectionComponent extends BaseComponent implements OnInit {
  isOpened = false;
  _links = [];
  @Input() type = 'IMAGE';

  @ViewChild('fm') fm: ElementRef;

  resetSelectingList() {
    this.fm.nativeElement.contentWindow.postMessage({type: 'RESET_LIST'}, '*');
  }

  ngOnInit() {
    window.addEventListener('message', (event) => {
      if (!event.data.type) {
        return;
      }

      if (event.data.type === 'LIST') {
        this._links = this._links.concat(event.data.values);
      }
    });
  }

  openFileManager() {
    this.resetSelectingList();
    this.isOpened = true;
  }

  closeFileManager() {
    this.isOpened = false;
  }

  removeLink(index) {
    this._links.splice(index, 1);
  }
}