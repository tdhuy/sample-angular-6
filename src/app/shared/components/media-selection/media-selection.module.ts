import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MediaSelectionComponent } from "./media-selection.component";
import { MoleculeContainerModule } from "../molecule-container/molecule-container.module";

@NgModule({
  imports: [
    CommonModule,
    MoleculeContainerModule
  ],
  declarations: [
    MediaSelectionComponent
  ],
  exports: [
    MediaSelectionComponent
  ]
})

export class MediaSelectionModule {}