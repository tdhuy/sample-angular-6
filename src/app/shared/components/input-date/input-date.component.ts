import { AfterViewInit, Component, ElementRef, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseComponent } from '../base/base.component';

declare var $: any;

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputDateComponent)
    }
  ]
})

export class InputDateComponent extends BaseComponent implements AfterViewInit, ControlValueAccessor {
  value: any = new Date();
  _dp: any = null;

  @Input() isInline = false;
  @Input() sideBySide = false;
  @Input() format = 'DD-MM-YYYY HH:mm';

  @Output() valueChanged = new EventEmitter<Date>();

  @Input()
  set date(value: Date) {
    if (this._dp) {
      this._dp.date(value);
    } else {
      setTimeout(() => {
        this._dp.date(value);
      }, 200);
    }
  }

  get date(): Date {
    return this.value;
  }

  constructor(private el: ElementRef) {
    super();
  }

  writeValue(obj: any) {
    this.value = obj;
  }

  ngAfterViewInit() {
    let dp;
    if (this.isInline) {
      const el = this.el.nativeElement.querySelector('.app-date-inline');
      dp = $(el).datetimepicker({
        inline: this.isInline,
        sideBySide: this.sideBySide,
        format: this.format
      });
    } else {
      const el = this.el.nativeElement.querySelector('.app-date-not-inline');
      dp = $(el).datetimepicker({
        sideBySide: this.sideBySide,
        format: this.format
      });
    }

    if (dp) {
      dp.on('dp.change', ({date, oldDate}) => {
        this.onValueChanged(date);
      });

      this._dp = dp.data('DateTimePicker');
    }
  }

  onValueChanged(value: any) {
    this.valueChanged.emit(value._d);
  }
}
