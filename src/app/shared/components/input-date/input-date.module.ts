import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { InputDateComponent } from './input-date.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MoleculeContainerModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [InputDateComponent],
  exports: [InputDateComponent]
})

export class InputDateModule {}
