import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpinnerModule } from '../spinner/spinner.module';
import { TableFilterSelect2Component } from './table-filter-select-2.component';

@NgModule({
  imports: [CommonModule, FormsModule, SpinnerModule, ReactiveFormsModule],
  declarations: [TableFilterSelect2Component],
  exports: [TableFilterSelect2Component]
})
export class TableFilterSelect2Module {
}
