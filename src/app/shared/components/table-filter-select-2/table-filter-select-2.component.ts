import {
  Component,
  EventEmitter,
  Input,
  Output,
  OnInit,
  AfterViewInit,
  HostListener,
  ElementRef, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, OnDestroy, AfterContentInit
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { IFilterSelect } from './ifilter-select';
import 'rxjs/add/operator/distinctUntilChanged';
import { TargetType, SelectValuesType } from '../table-control/iTableColumn';

@Component({
  selector: 'app-table-filter-select-2',
  templateUrl: './table-filter-select-2.component.html',
  providers: [],
  styleUrls: ['./table-filder-select-2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class TableFilterSelect2Component implements OnInit, AfterViewInit, OnDestroy, AfterContentInit {
  private _selectedValue = '';
  _selectedItem: any | null = null;
  realItemSource = [];
  isOpened = false;
  isLoading = false;
  private _itemsSource: Observable<any[]>;

  txtSearchControl = new FormControl();

  @Input() loadType: SelectValuesType;

  @Input() target: TargetType;
  /**
   * Gets or sets the path of the property to use as the visual representation of the items.
   */
  @Input() displayMemberPath = '';
  /**
   * Gets or sets the path of the property to use as the value of the items.
   */
  @Input() selectedValuePath = '';
  /**
   * Gets or sets the path of the property to define column
   * */
  @Input() columnCode = '';

  @Input()
  get itemsSource(): Observable<any[]> {
    return this._itemsSource;
  }

  set itemsSource(items: Observable<any[]>) {

    if (!items) {
      this._itemsSource = Observable.of([]);
      this.realItemSource = [];
      return;
    }

    this._itemsSource = items;

    items
      .distinctUntilChanged()
      .subscribe(value => {
        if (value.length !== 0) {
          this._selectedItem = value[0];
          this.realItemSource = value;
        }
      });

    this.cdt.detectChanges();
  }

  @Output() selectedItemChanged = new EventEmitter<IFilterSelect>();

  @ViewChild('searchContent') searchContent: ElementRef;

  @HostListener('document:click', ['$event'])
  handleClick(event: Event) {
    if (!this.el.nativeElement.querySelector('.select-wrap').contains(event.target)) {
      this.isOpened = false;
    }
  }

  constructor(private el: ElementRef,
              private cdt: ChangeDetectorRef) {

    this.txtSearchControl.valueChanges
      .debounceTime(700)
      .distinctUntilChanged()
      .subscribe(value => {
        this.isLoading = true;
        this._requestApi(value);
        this.cdt.detectChanges();
      });
  }

  toggleOpen() {
    this.isOpened = !this.isOpened;
    if (this.isOpened === true) {
      this.initPositionSearchContent();
    }
  }

  chooseOption(item) {
    this._selectedItem = item;
    this._selectedValue = this._selectedItem[this.selectedValuePath];

    const eventData: IFilterSelect = {
      column: this.columnCode,
      selectedValue: this._selectedValue,
      selectedItem: this._selectedItem
    };

    this.isOpened = false;

    this.selectedItemChanged.emit(eventData);
  }

  trackByFn(index, item) {
    return item.id;
  }

  ngOnInit() {
    this._selectedItem = {
      [this.displayMemberPath]: 'All',
      [this.selectedValuePath]: 'NONE'
    };
  }

  ngAfterViewInit() {

  }

  ngAfterContentInit() {
    this._requestApi('');
  }

  ngOnDestroy() {
    this.cdt.detach();
  }

  private _requestApi(name: string) {
    return;
    // const options: any = {
    //   size: 100,
    //   page: 1
    // };
    //
    // const action = name === '' ? 'list' : 'search';
    //
    // let req: any = null;
    // switch (this.target) {
    //   case TargetType.BRAND:
    //     options.name = name;
    //     req = this.brandService[action](options);
    //     break;
    //   case TargetType.STORE:
    //     options.name = name;
    //     req = this.storeService[action](options);
    //     break;
    //   case TargetType.DEAL:
    //     options.title = name;
    //     req = this.dealService[action](options);
    //     break;
    // }
    //
    // req
    //   .subscribe((res) => {
    //     if (res.code === 200) {
    //       const items = res.data.list_data.map((item) => {
    //
    //         let showedName = item.name;
    //
    //         if (this.target === TargetType.DEAL) {
    //           showedName = item.title;
    //         }
    //
    //         return {
    //           [this.displayMemberPath]: showedName + ` - ${item.id}`,
    //           [this.selectedValuePath]: item.id
    //         };
    //       });
    //
    //       items.unshift({
    //         [this.displayMemberPath]: 'All',
    //         [this.selectedValuePath]: 'NONE'
    //       });
    //
    //       this.realItemSource = items;
    //     } else {
    //       this.realItemSource = [];
    //     }
    //     this.isLoading = false;
    //     this.cdt.markForCheck();
    //   }, () => {
    //     this.realItemSource = [];
    //     this.isLoading = false;
    //     this.cdt.detectChanges();
    //   });
  }

  private initPositionSearchContent() {
    const tmp: ClientRect = this.el.nativeElement.getBoundingClientRect();
    this.searchContent.nativeElement.style.top = `${tmp.top}px`;
    this.searchContent.nativeElement.style.left = `${tmp.left}px`;
  }
}
