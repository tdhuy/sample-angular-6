export interface IFilterSelect {
  column: string;

  selectedItem: any | null;

  selectedValue: any;
}
