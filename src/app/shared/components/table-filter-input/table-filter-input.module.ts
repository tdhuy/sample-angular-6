import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TableFilterInputComponent } from './table-filter-input.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [TableFilterInputComponent],
  exports: [TableFilterInputComponent]
})
export class TableFilterInputModule {
}
