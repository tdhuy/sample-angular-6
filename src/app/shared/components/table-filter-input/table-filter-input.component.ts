import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IFilterInput } from './ifilter-input';

@Component({
  selector: 'app-table-filter-input',
  templateUrl: './table-filter-input.component.html',
  styleUrls: [
    './table-filter-input.component.scss'
  ],
  providers: []
})

export class TableFilterInputComponent {
  private _value = '';

  @Input() columnCode = '';

  /**
   * Event emitter fired when change input value
   *
   * @type(EventEmitter<IFilterInput>)
   */
  @Output() valueChanged = new EventEmitter<IFilterInput>();

  get value() {
    return this._value;
  }

  @Input('value')
  set value(val: string) {
    this._value = val;
  }

  onValueChanged($event) {
    this._onValueChanged($event.target.value);
  }

  _onValueChanged(val: string): void {
    const params: IFilterInput = {
      column: '',
      textValue: val
    };
    params.column = this.columnCode;
    this.valueChanged.emit(params);
  }

}
