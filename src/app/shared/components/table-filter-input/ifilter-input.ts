export interface IFilterInput {
  column: string;

  textValue: string;
}
