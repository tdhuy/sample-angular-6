import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SelectModule } from 'ng2-select';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { SelectTextComponent } from './select-text.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SelectModule,
    MoleculeContainerModule
  ],
  declarations: [SelectTextComponent],
  exports: [SelectTextComponent]
})

export class SelectTextModule {}
