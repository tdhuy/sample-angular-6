import { Component, Input, Output, EventEmitter, ViewChild, forwardRef } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { ISelectTextItem } from './i-select-text-item';

import { SelectComponent } from 'ng2-select';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-select-text',
  templateUrl: './select-text.component.html',
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SelectTextComponent),
    }
  ]
})

export class SelectTextComponent extends BaseComponent implements ControlValueAccessor {

  _selectedItems: ISelectTextItem[] = [];

  @Input() itemsSource: ISelectTextItem[] = [];
  @Input() isDisabled = false;

  @Input()
  set selectedItems(values: ISelectTextItem[]) {
    this._selectedItems = values;
  }

  get selectedItems(): ISelectTextItem[] {
    return this._selectedItems;
  }

  @Output() valueChanged = new EventEmitter<any>();
  @Output() textChanged = new EventEmitter<string>();

  @ViewChild('select') select: SelectComponent;

  public selected(value: any) {
    this.updateModel();
    this.valueChanged.emit(value);
  }

  onTextChanged(value: string) {
    this.textChanged.emit(value);
  }

  writeValue(obj: any) {
    this.selectedItems = obj; 
  }

  updateModel() {
    this.onModelChange(this.selectedItems);
  }
}
