import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-input-rich-text-2',
  templateUrl: './input-rich-text-2.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputRichText2Component)
    }
  ]
})

export class InputRichText2Component extends BaseComponent implements ControlValueAccessor {
  @Input() value = '';

  @Output() valueChanged = new EventEmitter<string>();

  writeValue(obj: any) {
    this.value = obj;
  }

  onChange(event: any) {
    console.log('values: ', event);
    this.updateModel();
  }

  onEditorChange(event: any) {}

  onReady(event: any) {}

  onFocus(event: any) {}

  onBlur(event: any) {}

  updateModel() {
    this.onModelChange(this.value);
    this.valueChanged.emit(this.value);
  }
}
