import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from 'ng2-ckeditor';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { InputRichText2Component } from './input-rich-text-2.component';

@NgModule({
  declarations: [
    InputRichText2Component
  ],
  imports: [
    CommonModule,
    CKEditorModule,
    FormsModule,
    MoleculeContainerModule
  ],
  exports: [
    InputRichText2Component
  ]
})

export class InputRichText2Module {

}
