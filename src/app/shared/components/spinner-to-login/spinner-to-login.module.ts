import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SpinnerToLoginComponent } from './spinner-to-login.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SpinnerToLoginComponent
  ],
  exports: [
    SpinnerToLoginComponent
  ]
})

export class SpinnerToLoginModule {}
