import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-spinner-to-login',
  templateUrl: './spinner-to-login.component.html',
  styleUrls: ['./spinner-to-login.component.scss']
})

export class SpinnerToLoginComponent {
  isShowed = false;
  @Input() fullPage = false;

  @Input()
  set isLoading(value: boolean) {
    this.isShowed = value;
  }
}
