import {
  Component, EventEmitter, Input, Output, OnInit, AfterViewInit, ElementRef, ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IFilterSelect } from './ifilter-select';
import 'rxjs/add/operator/distinctUntilChanged';
import { SelectValuesType } from '../table-control/iTableColumn';

@Component({
  selector: 'app-table-filter-select',
  templateUrl: './table-filter-select.component.html',
  providers: [],
  styleUrls: ['./table-filder-select.component.scss']
})

export class TableFilterSelectComponent implements OnInit, AfterViewInit {
  private _selectedValue = '';
  _selectedItem: any | null = null;
  _selectValuesType = SelectValuesType;
  realItemSource = [];
  countRender = 0;
  private _itemsSource: Observable<any[]>;

  /**
   * Gets or sets the path of the property to use as the visual representation of the items.
   */
  @Input() displayMemberPath = '';
  /**
   * Gets or sets the path of the property to use as the value of the items.
   */
  @Input() selectedValuePath = '';
  /**
   * Gets or sets the path of the property to define column
   * */
  @Input() columnCode = '';
  @Input()
  get itemsSource(): Observable<any[]> {
    return this._itemsSource;
  }

  set itemsSource(items: Observable<any[]>) {

    if (!items) {
      this._itemsSource = Observable.of([]);
      this.realItemSource = [];
      return;
    }

    this._itemsSource = items;

    items
      .distinctUntilChanged()
      .subscribe(value => {
        if (value.length !== 0) {
          this._selectedItem = value[0];
          this.realItemSource = value;
        }
      });

    // this.cdt.detectChanges();
  }

  @Output() selectedItemChanged = new EventEmitter<IFilterSelect>();

  constructor(private el: ElementRef,
              private cdt: ChangeDetectorRef) {
  }

  onSelectedItemChanged() {
    this._selectedValue = this._selectedItem[this.selectedValuePath];

    const eventData: IFilterSelect = {
      column: this.columnCode,
      selectedValue: this._selectedValue,
      selectedItem: this._selectedItem
    };

    this.selectedItemChanged.emit(eventData);
  }

  trackByFn(index, item) {
    return item.id;
  }

  ngOnInit() { }

  ngAfterViewInit() { }
}
