import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TableFilterSelectComponent } from './table-filter-select.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [TableFilterSelectComponent],
  exports: [TableFilterSelectComponent]
})
export class TableFilterSelectModule {
}
