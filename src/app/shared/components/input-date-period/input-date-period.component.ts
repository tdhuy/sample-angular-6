import { Component, EventEmitter, Input, Output, forwardRef } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BaseComponent } from '../base/base.component';
import { ITimeRange } from './i-time-range';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@angular/forms/src/directives/control_value_accessor';

@Component({
  selector: 'app-input-date-period',
  templateUrl: './input-date-period.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputDatePeriodComponent)
    }
  ]
})

export class InputDatePeriodComponent extends BaseComponent implements ControlValueAccessor {
  private _startDate = new Date();
  private _endDate = new Date();

  rangeValue = [this._startDate, this._endDate];

  config: Partial<BsDatepickerConfig> = {
    containerClass: 'theme-red'
  };

  format = 'dd/MM/yyyy';

  @Output() valueChanged = new EventEmitter<ITimeRange>();

  @Input()
  set dates(values: Date[]) {
    this.rangeValue = values;
  }

  get dates(): Date[] {
    return this.rangeValue;
  }

  constructor() {
    super();
  }

  writeValue(obj: any) {
    this.dates = obj;
  }

  onValueChanged() {
    setTimeout(() => {
      this.updateModel();
      const outputValue: ITimeRange = {
        startDate: this.rangeValue[0],
        endDate: this.rangeValue[1]
      };

      this.valueChanged.emit(outputValue);
    });
  }

  updateModel() {
    this.onModelChange(this.dates);
  }

  onGotFocus() {
    super.onGotFocus();
  }

  onLostFocus() {
    super.onLostFocus();
    this.onModelTouched();
  }
}
