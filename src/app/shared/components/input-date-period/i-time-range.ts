export interface ITimeRange {
  startDate: Date;

  endDate: Date;
}
