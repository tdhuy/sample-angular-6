import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { InputDatePeriodComponent } from './input-date-period.component';

@NgModule({
  imports: [
    MoleculeContainerModule,
    FormsModule,
    CommonModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [
    InputDatePeriodComponent
  ],
  exports: [
    InputDatePeriodComponent
  ]
})

export class InputDatePeriodModule {}
