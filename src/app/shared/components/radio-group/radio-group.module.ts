import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { StringHelperService } from '../../services/string-helper.service';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { RadioGroupComponent } from './radio-group.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MoleculeContainerModule
  ],
  declarations: [
    RadioGroupComponent
  ],
  exports: [
    RadioGroupComponent
  ],
  providers: [StringHelperService]
})

export class RadioGroupModule {}
