import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { StringHelperService } from '../../services/string-helper.service';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class RadioGroupComponent extends BaseComponent {
  _selectedValue = '';

  @Input() displayPathName = '';
  @Input() selectedPathValue = '';
  @Input() itemsSource: any[] = [];

  @Input()
  set selectValue(value: any) {
    this._selectedValue = value;
    this.cdt.detectChanges();
  }

  get selectValue(): any {
    return this._selectedValue;
  }

  @Output() valueChanged = new EventEmitter<any>();
  name = '';

  constructor(private strHelper: StringHelperService,
              private cdt: ChangeDetectorRef) {
    super();
    this.name = this.strHelper.genRandomString();
  }

  onChanged(value) {
    this.valueChanged.emit(value);
  }

  writeValue(value: any) {
    this._selectedValue = value;
    this.cdt.detectChanges();
  }
}
