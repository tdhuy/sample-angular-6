import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TableCellContentComponent } from './table-cell-content.component';

@NgModule({
  declarations: [
    TableCellContentComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: []
})
export class TableCellContentModule {}
