import { Component, Input } from '@angular/core';
import { CellDisplay, ITableColumn } from '../table-control/iTableColumn';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-table-cell-content',
  templateUrl: './table-cell-content.component.html'
})

export class TableCellContentComponent {

  private _data: Object = {};

  private _column: ITableColumn;

  @Input()
  set column(value: ITableColumn) {
    this._column = value;
  }

  get column(): ITableColumn {
    return this._column;
  }

  @Input()
  set data(value: Object) {
    this._data = value;
  }

  get data(): Object {
    return this._data;
  }

  constructor(private decimalPipe: DecimalPipe) { }

  private _getValueNested(obj: Object, keyStr: string): any {
    const keys = keyStr.split('.');
    let _obj = Object.assign({}, obj);
    keys.forEach((k) => {
      if (_obj.hasOwnProperty(k)) {
        _obj = _obj[k];
      }
    });

    return _obj;
  }

  getValueNested(item, column: ITableColumn) {
    let value = this._getValueNested(item, column.displayValuePath);
    switch (column.displayValueAs) {
      case CellDisplay.STRING:
        if (value == null) {
          value = '';
        }
        value = value.toString();
        break;
      case CellDisplay.NUMBER:
        value = this.decimalPipe.transform(value);
        break;
      case CellDisplay.CURRENCY:
        break;
      case CellDisplay.CUSTOM:
        break;
    }

    return value;
  }
}
