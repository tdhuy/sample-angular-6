import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, forwardRef } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BaseComponent } from '../base/base.component';
import { ISelectTextItem } from '../select-text/i-select-text-item';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@angular/forms/src/directives/control_value_accessor';

@Component({
  selector: 'app-select-search',
  templateUrl: './select-search.component.html',
  styleUrls: ['./select-search-component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SelectSearchComponent)
    }
  ]
})

export class SelectSearchComponent extends BaseComponent implements OnInit, ControlValueAccessor {
  isOpened = false;
  _itemsSource: ISelectTextItem[] = [];
  _txt = '';
  private _bufferItemsSource = [];
  private _textSearchApi = new Subject<string>();
  _displayMemberPath = '';
  _selectedValuePath = '';
  gapBeLevel = {
    0: '',
    1: '',
    2: ' + ',
    3: ' + + '
  };

  @Input() dataFlowType: 'API' | 'FIX' = 'FIX';
  @Input() selectedItem: ISelectTextItem;
  @Input() removeAble = true;

  @Input()
  set itemsSource(values: ISelectTextItem[]) {
    this._itemsSource = values.map(v => {
      const _v = {...v};
      _v.extra = _v.extra || {};
      _v.extra.level = _v.extra.level || 1;
      return _v;
    });
    this._bufferItemsSource = values;
  }

  get itemsSource(): ISelectTextItem[] {
    return this._itemsSource;
  }

  @Input()
  set displayMemberPath(value: string) {
    this._displayMemberPath = value;
    this._propChanged();
  }

  @Input()
  set selectedValuePath(value: string) {
    this._selectedValuePath = value;
    this._propChanged();
  }

  @Output() requestData = new EventEmitter<string>();
  @Output() valueChanged = new EventEmitter<ISelectTextItem>();

  constructor(private el: ElementRef) {
    super();

    this._textSearchApi
      .debounceTime(200)
      .distinctUntilChanged()
      .subscribe(value => {
        this.requestData.emit(value);
      });
  }

  @HostListener('document:click', ['$event'])
  handleClick(event: Event) {
    if (!this.el.nativeElement.querySelector('.select-search').contains(event.target)) {
      this.isOpened = false;
    }
  }

  onSelectItem(item: ISelectTextItem) {
    if (item.extra && item.extra.selectable === false) {
      return;
    }

    this.isOpened = false;
    this.selectedItem = item;
    this.updateModel();
    this.valueChanged.emit(item);
  }

  open() {
    this.isOpened = !this.isOpened;
  }

  onTextChanged($event) {
    this._txt = $event.target.value;
    this._handleGetItemsSource();
  }

  trackByFn(index, item: ISelectTextItem) {
    return item.id;
  }

  ngOnInit() {
    this.requestData.emit(this._txt);
  }

  remove(event) {
    event.stopPropagation();
    this.selectedItem = null;
    this.updateModel();
    this.valueChanged.emit(null);
  }

  writeValue(obj: any) {
    this.selectedItem = obj;
  }

  updateModel() {
    this.onModelChange(this.selectedItem);
  }

  onGotFocus() {
    super.onGotFocus();
  }

  onLostFocus() {
    super.onLostFocus();
    this.onModelTouched();
  }

  private _propChanged() {}

  private _handleGetItemsSource() {
    if (this.dataFlowType === 'FIX') {
      this._filterItemsSource();
    } else if (this.dataFlowType === 'API') {
      this._textSearchApi.next(this._txt);
    }
  }

  private _filterItemsSource() {
    this._itemsSource = this._bufferItemsSource.filter(item => {
      return item.text.indexOf(this._txt) !== -1;
    });
  }
}
