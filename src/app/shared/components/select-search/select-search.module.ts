import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { SelectSearchComponent } from './select-search.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MoleculeContainerModule
  ],
  declarations: [
    SelectSearchComponent
  ],
  exports: [
    SelectSearchComponent
  ]
})

export class SelectSearchModule { }
