import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { CheckBoxComponent } from './check-box.component';

describe('CheckBoxComponent', () => {
  let component: CheckBoxComponent;
  let fixture: ComponentFixture<CheckBoxComponent>;
  let lblElement: DebugElement;
  let inputElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckBoxComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckBoxComponent);
    component = fixture.componentInstance;
    lblElement = fixture.debugElement.query(By.css('label'));
    inputElement = fixture.debugElement.query(By.css('input'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Label', () => {
    it('Should contain class "notAllowed" when isDisabled=true', () => {
      component.isDisabled = true;
      fixture.detectChanges();
      expect(lblElement.classes['notAllowed']).toBeTruthy();
    });

    it('Should not contain class "notAllowed" when isDisabled=false', () => {
      component.isDisabled = false;
      fixture.detectChanges();
      expect(lblElement.classes['notAllowed']).toBeFalsy();
    });
  });

  describe('Input', () => {
    it('Type should be "checkbox"', () => {
      expect(inputElement.attributes['type']).toEqual('checkbox');
    });

    it('Attribute disabled should be true when isDisabled = true', () => {
      component.isDisabled = true;
      fixture.detectChanges();
      expect(inputElement.attributes['disabled']).toBeTruthy();
    });

    it('Attribute disabled should be null when isDisabled = false', () => {
      component.isDisabled = false;
      fixture.detectChanges();
      expect(inputElement.attributes['disabled']).toBeFalsy();
    });

    it('Id should be blank when initializing', () => {
      expect(inputElement.nativeElement.id).toEqual('');
    });

    it('Id is set correctly', () => {
      component.id = 'id1';
      fixture.detectChanges();
      expect(inputElement.nativeElement.id).toEqual('id1');
    });

    it('Value should be empty when initializing', () => {
      expect(inputElement.nativeElement.value).toEqual('');
    });

    it('Value is set correctly', () => {
      component.value = 'abc';
      fixture.detectChanges();
      expect(inputElement.nativeElement.value).toEqual('abc');
    });

    it('isChecked is false when initializing', () => {
      expect(inputElement.nativeElement.checked).toBeFalsy();
    });

    it('isChecked is true when checkbox is checked', () => {
      inputElement.nativeElement.click();
      fixture.detectChanges();
      expect(component.isChecked).toBeTruthy();
    });

    it('isChecked is false when checkbox is unchecked', () => {
      inputElement.nativeElement.click();
      fixture.detectChanges();
      inputElement.nativeElement.click();
      fixture.detectChanges();
      expect(component.isChecked).toBeFalsy();
    });

    it('onChange is called when checkbox change status', () => {
      spyOn(component, 'onChange');
      inputElement.nativeElement.click();
      fixture.detectChanges();
      expect(component.onChange).toHaveBeenCalledTimes(1);

      inputElement.nativeElement.click();
      fixture.detectChanges();
      expect(component.onChange).toHaveBeenCalledTimes(2);
    });

    it('Value is emitted correctly when value change', () => {
      spyOn(component.change, 'emit');
      component.value = 'checkbox_value';
      inputElement.nativeElement.click();
      fixture.detectChanges();
      expect(component.change.emit).toHaveBeenCalledWith('checkbox_value');
    });
  });
});

@Component({
  selector: 'app-test-checkbox-component',
  template: `
    <app-check-box [isChecked]="false"
                   [value]="abc"
                   [id]="id111"
                   [isDisabled]="false">
      Test ng-content
    </app-check-box>
  `
})
class TestCheckboxComponent {}

describe('Test ng-content', () => {
  let component: TestCheckboxComponent;
  let fixture: ComponentFixture<TestCheckboxComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TestCheckboxComponent, CheckBoxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('HTML content is passed correctly', () => {
    expect(fixture.debugElement.query(By.css('app-check-box')).nativeElement.innerText.trim()).toEqual('Test ng-content');
  });
});
