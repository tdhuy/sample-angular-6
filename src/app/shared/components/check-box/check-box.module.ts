import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CheckBoxComponent } from './check-box.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule],
  declarations: [
    CheckBoxComponent],
  exports: [CheckBoxComponent]
})
export class CheckBoxModule {
}
