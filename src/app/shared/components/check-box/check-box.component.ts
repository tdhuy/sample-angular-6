import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 * Represents a CheckBox component.
 */
@Component({
  selector: 'app-check-box',
  templateUrl: './check-box.component.html',
  styleUrls: ['./check-box.component.scss']
})
export class CheckBoxComponent {

  /**
   * Gets or sets the id of the checkbox.
   */
  @Input() id = '';

  /**
   * Gets or sets the value of the checkbox.
   */
  @Input() value: any = '';

  /**
   * Gets or sets a value indicating whether the user can click the checkbox.
   */
  @Input() isDisabled = false;

  /**
   * Gets or sets a value indicating whether the checkbox is checked.
   */
  @Input()
  get isChecked(): boolean {
    return this._isChecked;
  }

  set isChecked(value: boolean) {
    this._isChecked = value;
  }

  private _isChecked = false;

  @Input() contentText = '';

  /**
   * Occurs when the check status is changed.
   *
   * @type {EventEmitter<any>} values
   */
  @Output() change = new EventEmitter<any>();

  /**
   * Raises the [valueChange]{@link CheckBoxComponent#change} event.
   */
  onChange(event: Event) {
    event.stopPropagation();
    const emitValue: any = {};
    emitValue[this.value] = this._isChecked;
    this.change.emit(emitValue);
  }
}
