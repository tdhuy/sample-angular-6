import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CheckBoxModule } from '../check-box/check-box.module';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { CheckBoxGroupComponent } from './check-box-group.component';

@NgModule({
  imports: [CommonModule, FormsModule, CheckBoxModule, MoleculeContainerModule],
  declarations: [CheckBoxGroupComponent],
  exports: [CheckBoxGroupComponent, CheckBoxModule]
})
export class CheckBoxGroupModule {
}
