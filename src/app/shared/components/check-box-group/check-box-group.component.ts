import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseComponent } from '../base/base.component';
import { CheckBoxComponent } from '../check-box/check-box.component';

export const CHECK_BOX_GROUP_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CheckBoxGroupComponent),
  multi: true
};

/**
 * The component used to select someone from multiple values.
 * 複数の値からいくつか選択する際に使用するコンポーネントです。
 */
@Component({
  selector: 'app-check-box-group',
  templateUrl: './check-box-group.component.html',
  styleUrls: ['./check-box-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  providers: [CHECK_BOX_GROUP_VALUE_ACCESSOR]
})
export class CheckBoxGroupComponent<T> extends BaseComponent implements ControlValueAccessor {

  _numberItemInRow = 3;

  _width = '33.33%';

  /**
   * Gets or sets the id of the radio group.
   */
  @Input() id = '';

  /**
   * Gets or sets the path of the property to use as the visual representation of the radio.
   */
  @Input() displayMemberPath = '';

  /**
   * Gets or sets the path of the property to use as the value of the radio.
   */
  @Input() selectedValuePath = '';

  /**
   * Gets or sets the value of the combo box.
   */
  @Input()
  get itemsSource(): T[] {
    return this._itemsSource;
  }

  set itemsSource(items: T[]) {
    this._itemsSource = items;
  }

  @Input()
  set numberItemInRow(value: number) {
    this._numberItemInRow = value;
    this.setWidth();
  }

  get numberItemInRow(): number {
    return this._numberItemInRow;
  }

  private _itemsSource: T[] = [];

  /**
   *  Child checkboxes.
   */
  @ViewChildren(CheckBoxComponent) checkBoxes: QueryList<CheckBoxComponent>;

  /**
   * Gets or sets the value of the selected item in the component.
   */
  @Input() selectedValues: any[] = [];

  /**
   * Occurs when the selectedValues property value has changed.
   *
   * @type {EventEmitter<any[]>} selected values
   */
  @Output() valueChanged = new EventEmitter<any[]>();

  onModelChange = (_: any) => { };

  onModelTouched = () => { };

  writeValue(obj: any): void {
    this.selectedValues = obj;
  }

  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onModelTouched = fn;
  }

  /**
   * Raises the valueChanged event.
   * valueChangedイベントを発生させます。
   */
  onChanged() {
    this.selectedValues = this.checkBoxes
      .filter((checkBox: CheckBoxComponent) => checkBox.isChecked)
      .map((checkBox: CheckBoxComponent) => checkBox.value);

    this.valueChanged.emit(this.selectedValues);
    this.onModelChange(this.selectedValues);
    this.onModelTouched();
  }

  trackByFn(index: number) {
    return index;
  }

  /**
   * check boxがチェック状態かを返します。
   * @param value
   * @returns {boolean}
   */
  getIsChecked(value: any) {
    if (this.selectedValues === null) {
      return false;
    }

    return this.selectedValues.indexOf(value) !== -1;
  }

  private setWidth() {
    this._width = `${(100 / this._numberItemInRow).toString()}%`;
  }
}
