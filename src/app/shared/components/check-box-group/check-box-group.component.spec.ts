import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CheckBoxComponent } from '../check-box/check-box.component';
import { CheckBoxModule } from '../check-box/check-box.module';
import { MoleculeContainerComponent } from '../molecule-container/molecule-container.component';
import { MoleculeContainerModule } from '../molecule-container/molecule-container.module';
import { CheckBoxGroupComponent } from './check-box-group.component';

describe('CheckBoxGroupComponent', () => {
  let component: CheckBoxGroupComponent<null>;
  let fixture: ComponentFixture<CheckBoxGroupComponent<null>>;
  let mcElement: DebugElement;
  let elCheckboxGr: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CheckBoxModule, MoleculeContainerModule, BrowserAnimationsModule],
      declarations: [CheckBoxGroupComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckBoxGroupComponent);
    component = fixture.componentInstance;
    mcElement = fixture.debugElement.query(By.directive(MoleculeContainerComponent));
    component.selectedValuePath = 'gyoshaCd';
    component.displayMemberPath = 'gyoshaNm';
    component.itemsSource = [
      {gyoshaNm: 'XX', gyoshaCd: 'xx'},
      {gyoshaNm: 'YY', gyoshaCd: 'yy'},
      {gyoshaNm: 'ZZ', gyoshaCd: 'zz'}
    ] as any[];
    fixture.detectChanges();
    elCheckboxGr = fixture.debugElement.queryAll(By.directive(CheckBoxComponent));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test molecule Component', () => {
    // begin: test case for molecule container
    it('Title should be empty', () => {
      component.itemsSource = [];
      fixture.detectChanges();
      expect(mcElement.nativeElement.querySelector('label')).toBeNull();
    });

    it('Title should be set value', () => {
      const data = 'new title';
      component.title = data;
      fixture.detectChanges();
      expect(mcElement.nativeElement.querySelector('label').innerText).toEqual(data);
    });

    it('Tooltip should be set value', () => {
      const data = 'tooltip value';
      component.tooltip = data;
      fixture.detectChanges();
      expect(mcElement.nativeElement.querySelector('.glyphicon-question-sign')).toBeTruthy();
    });

    it('Tooltip should empty as initialization', () => {
      expect(mcElement.nativeElement.querySelector('.glyphicon-question-sign')).toBeFalsy();
    });

    it('Infos should empty as initialization', () => {
      expect(mcElement.nativeElement.querySelectorAll('p.text-primary').length).toBe(0);
    });

    it('Info labels text should be set value', () => {
      const data = ['info value 1', 'info value 2'];
      component.infos = data;
      fixture.detectChanges();
      const values = mcElement.nativeElement.querySelectorAll('p.text-primary');
      expect(values.length).toEqual(2);
      expect(values[0].innerText).toEqual(data[0]);
      expect(values[1].innerText).toEqual(data[1]);
    });

    it('Warning message should not display as initialization', () => {
      expect(mcElement.nativeElement.querySelectorAll('p.text-warning').length).toBe(0);
    });

    it('Warning messages should be displayed', () => {
      const data = ['warning 1', 'warning 2'];
      component.warnings = data;
      component.isDisabled = false;
      fixture.detectChanges();
      const values = mcElement.nativeElement.querySelectorAll('p.text-warning');
      expect(values.length).toEqual(2);
      expect(values[0].innerText).toEqual(data[0]);
      expect(values[1].innerText).toEqual(data[1]);
    });

    it('Error message should not display as initialization', () => {
      expect(mcElement.nativeElement.querySelectorAll('p.text-danger').length).toBe(0);
    });

    it('Error message should be displayed', () => {
      const data = ['error 1', 'error 2'];
      component.errors = data;
      component.isDisabled = false;
      fixture.detectChanges();
      const values = mcElement.nativeElement.querySelectorAll('p.text-danger');
      expect(values.length).toEqual(2);
      expect(values[0].innerText).toEqual(data[0]);
      expect(values[1].innerText).toEqual(data[1]);
    });

    it('"必須" should be displayed', () => {
      component.isRequired = true;
      fixture.detectChanges();
      expect(mcElement.nativeElement.querySelector('.label-danger').innerText).toEqual('必須');
    });

    it('"必須" should be undisplayed', () => {
      component.isRequired = false;
      fixture.detectChanges();
      expect(mcElement.nativeElement.querySelector('.label-danger')).toBeFalsy();
    });
    // end: test case for molecule container
  });
  describe('Test Check Box Group Input', () => {
    it('Check Box Group Input should be disable when isDisable = true', () => {
      component.isDisabled = true;
      fixture.detectChanges();
      expect(elCheckboxGr[0].nativeElement.querySelector('input').disabled).toBe(true);
    });

    it('Check Box Group Input should be enable when isDisable = false', () => {
      component.isDisabled = false;
      fixture.detectChanges();
      expect(elCheckboxGr[0].nativeElement.querySelector('input').disabled).toBe(false);
    });

    it('onChanged event is called when its value change', () => {
      spyOn(component, 'onChanged');
      elCheckboxGr[1].nativeElement.querySelector('input').click();
      fixture.detectChanges();
      expect(component.onChanged).toHaveBeenCalled();
    });

    it('Check Box Group Input should be render enough quantity equal to itemsSource.length(Check ngFor)', () => {
      expect(elCheckboxGr.length).toBe(3);
    });

    it('Check value from component to html of Check Box Group Input', () => {
      expect(elCheckboxGr[0].nativeElement.querySelector('input').value).toBe('xx');
    });

    it('Check Id from component to html of Check Box Group Input', () => {
      component.id = 'test';
      fixture.detectChanges();
      expect(elCheckboxGr[0].nativeElement.querySelector('input').id).toBe('test0');
    });

    it('Check function registerOnChange', () => {
      component.registerOnChange('yy');
      expect(component.onModelChange).toBe('yy');
    });

    it('Check function registerOnTouched', () => {
      component.registerOnTouched('yy');
      expect(component.onModelTouched).toBe('yy');
    });

    it('Check checked when select value', () => {
      elCheckboxGr[1].nativeElement.querySelector('input').click();
      expect(component.checkBoxes['_results'][1].isChecked).toBe(true);
    });

    it('Check writeValue function when select value', () => {
      component.writeValue(['yy', 'zz']);
      expect(component.checkBoxes['_results'][1].isChecked).toBe(true);
      expect(component.checkBoxes['_results'][2].isChecked).toBe(true);
    });

    it('Expected value correct when select multi item.', () => {
      elCheckboxGr[1].nativeElement.querySelector('input').click();
      elCheckboxGr[2].nativeElement.querySelector('input').click();
      expect(component.checkBoxes['_results'][1].isChecked).toBe(true);
      expect(component.checkBoxes['_results'][2].isChecked).toBe(true);
    });
  });
});
