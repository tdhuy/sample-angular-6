import { IFixedOption } from '../components/table-control/iTableColumn';

const ST: { [key: string]: IFixedOption } = {
  1: {id: '1', name: 'Active'},
  2: {id: '2', name: 'Pending or wait comfirm', visibleToUpdate: false},
  3: {id: '3', name: 'Blocked'},
  4: {id: '4', name: 'Delete'},
  5: {id: '5', name: 'Payment paid'},
  6: {id: '6', name: 'Payment unpaid'},
  7: {id: '7', name: 'Payment free'},
  8: {id: '8', name: 'Child accepted'},
  9: {id: '9', name: 'Child waiting'},
  10: {id: '10', name: 'Child rejected'},
  11: {id: '11', name: 'Child deleted'},
  12: {id: '12', name: 'Child none'}
};

export const getStatusName = (value: string | number): string => {
  if (value === null || value === undefined) {
    return '__not_defined__';
  }

  for (const k in ST) {
    if (ST[k] && ST[k].id === value.toString()) {
      return ST[k].name;
    }
  }

  return '__not_defined__';
};

export const newsStatus: IFixedOption[] = [
  ST[1],
  ST[2],
  ST[3],
  ST[4]
];

export const projectStatus: IFixedOption[] = [
  ST[1],
  ST[2],
  ST[3],
  ST[4]
];

export const postSaleStatus: IFixedOption[] = [
  ST[1],
  ST[2],
  ST[3],
  ST[4]
];

export const userStatus: IFixedOption[] = [
  ST[1],
  ST[2],
  ST[3],
  Object.assign({}, ST[4], {visibleToUpdate: false})
];

export const priorityStatus: IFixedOption[] = [
  ST[1],
  ST[2],
  ST[3],
  ST[4]
];
