import { environment } from '../../../environments/environment';

export namespace APIs {
  export const uploadImage = environment.apiHost + 'api/v1/images/upload';
  export const getImage = environment.apiHost + 'api/v1/images/get/{id}';
  export const postBuy = environment.apiHost + 'api/v1/buys/add';
  export const register = environment.apiHost + 'api/v1/users/register';
  export const login = environment.apiHost + 'api/v1/admins/login';
  export const postDetail = environment.apiHost + 'admin/v1/posts/detail/{id}';
  export const updatePostSeo = environment.apiHost + 'admin/v1/posts/update/url/{id}';
  export const search = environment.apiHost + 'api/v1/search';
  export namespace Admin {
    export const Login = environment.apiHost + 'admin/v1/admins/login';
    export const Create = environment.apiHost + 'admin/v1/admins/create';
    export const List = environment.apiHost + 'admin/v1/admins/list';
    export const UpdateInfo = environment.apiHost + 'admin/v1/admins/update';
    export const UpdateStatus = environment.apiHost + 'admin/v1/admins/status/{id}';
  }

  export namespace post {
    export const list = environment.apiHost + 'admin/v1/posts/list';
    export const updateBuy = environment.apiHost + 'admin/v1/buys/update/{id}';
    export const updateSale = environment.apiHost + 'admin/v1/sales/update/{id}';
  }

  export namespace project {
    export const detail = environment.apiHost + 'admin/v1/posts/detail/{id}';
    export const add = environment.apiHost + 'admin/v1/projects/add';
    export const list = environment.apiHost + 'admin/v1/posts/list';
    export const projectType = environment.apiHost + 'admin/v1/projects/types';
    export const update = environment.apiHost + 'admin/v1/projects/update/{id}';
  }

  export namespace news {
    export const detail = environment.apiHost + 'admin/v1/posts/detail/{id}';
    export const add = environment.apiHost + 'admin/v1/news/add';
    export const update = environment.apiHost + 'admin/v1/news/update/{id}';
    export const list = environment.apiHost + 'admin/v1/posts/list';
    export const category = environment.apiHost + 'admin/v1/news/cats';
  }

  export namespace users {
    export const list = environment.apiHost + 'admin/v1/users/list';
    export const updateUserType = environment.apiHost + 'admin/v1/users/user-type/{id}';
    export const update = environment.apiHost + 'admin/v1/users/update/{id}';
    export const updateStatus = environment.apiHost + 'admin/v1/users/status';
    export const updateMainBalance = environment.apiHost + 'admin/v1/payments/add/{userId}';
    export const updatePromoBalance = environment.apiHost + 'admin/v1/promos/add/{userId}';
  }

  export namespace priority {
    export const list = environment.apiHost + 'admin/v1/vips/list';
    export const update = environment.apiHost + 'admin/v1/vips/update/{id}';
  }

  export namespace searchbox {
    export const list = environment.apiHost + 'admin/v1/categories/list';
    export const detail = environment.apiHost + 'admin/v1/categories/detail/{id}';
    export const update = environment.apiHost + 'admin/v1/categories/update/{id}';
  }

  export namespace Tag {
    export const List = environment.apiHost + 'admin/v1/tags/list';
    export const Detail = environment.apiHost + 'admin/v1/tags/detail/{id}';
    export const Update = environment.apiHost + 'admin/v1/tags/update/{id}';
  }
}
