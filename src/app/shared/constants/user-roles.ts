export interface IRole {
  title: any;
  code: any;
  id: any;
}

export namespace USER_ROLES {
  export const All = ['All', 'ALL', 0];
  export const SuperAdmin = ['Supper Admin', 'ROLE_SUPER_ADMIN', 10];
  export const NormalUser = ['Normal User', 'ROLE_USER', -1];
  export const SaleAdmin = ['Sale Admin', 'ROLE_SALE_ADMIN', 9];
  export const BD = ['BD', 'ROLE_BD', 4];
  export const ContentManager = ['Content Manager', 'ROLE_CONTENT_MANAGER', 5];
  export const Content = ['Content', 'ROLE_CONTENT', 3];
  export const Operator = ['Operator', 'ROLE_OPERATOR', 7];
  export const MarketingManager = ['Marketing Manager', 'ROLE_MARKETING_MANAGER', 8];
  export const Marketing = ['Marketing', 'ROLE_MARKETING', 6];
  export const CustomerService = ['Customer Service', 'ROLE_CUSTOMER_SERVICE', 2];

  export const List: IRole[] = [
    {title: All[0], code: All[1], id: All[2]},
    {title: SuperAdmin[0], code: SuperAdmin[1], id: SuperAdmin[2]},
    // {title: NormalUser[0], code: NormalUser[1], id: All[2]},
    {title: SaleAdmin[0], code: SaleAdmin[1], id: SaleAdmin[2]},
    {title: BD[0], code: BD[1], id: BD[2]},
    {title: ContentManager[0], code: ContentManager[1], id: ContentManager[2]},
    {title: Content[0], code: Content[1], id: Content[2]},
    {title: Operator[0], code: Operator[1], id: Operator[2]},
    {title: MarketingManager[0], code: MarketingManager[1], id: MarketingManager[2]},
    {title: Marketing[0], code: Marketing[1], id: Marketing[2]},
    {title: CustomerService[0], code: CustomerService[1], id: CustomerService[2]}
  ];
}
