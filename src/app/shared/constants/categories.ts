import { environment } from '../../../environments/environment';
const domain = ``;

export const CATEGORIES = {
  1: {
    'id': 1,
    'name': 'Ẩm thực',
    'imageLink': `${domain}assets/images/categories/food.png`
  },
  2: {
    'id': 2,
    'name': 'Làm đẹp',
    'imageLink': `${domain}assets/images/categories/beauty.png`
  },
  3: {
    'id': 3,
    'name': 'Giải trí',
    'imageLink': `${domain}assets/images/categories/entertainment.png`
  },
  4: {
    'id': 4,
    'name': 'Mua sắm',
    'imageLink': `${domain}assets/images/categories/shopping.png`
  },
  5: {
    'id': 5,
    'name': 'Đào tạo',
    'imageLink': `${domain}assets/images/categories/education.png`
  },
  6: {
    'id': 6,
    'name': 'Đi lại',
    'imageLink': `${domain}assets/images/categories/walk.png`
  },
  7: {
    'id': 7,
    'name': 'Game thẻ',
    'imageLink': `${domain}assets/images/categories/game.png`
  },
  8: {
    'id': 8,
    'name': 'Khác',
    'imageLink': `${domain}assets/images/categories/other.png`
  },
  9: {
    'id': 9,
    'name': 'Trà sữa',
    'imageLink': `${domain}assets/images/categories/milktea.png`
  },
  10: {
    'id': 10,
    'name': 'Mua sắm online',
    'imageLink': `${domain}assets/images/categories/onlineshopping.png`
  }
};
