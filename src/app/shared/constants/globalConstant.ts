import Data from './selector';

const {cateList, cateListBuy, cityListOTher1, priceLevel, areaList} = Data;

export namespace GlobalConstant {
  export const CateSaleList = cateList || [];
  export const CateBuyList = cateListBuy || [];
  export const CityListOther1 = cityListOTher1 || [];
  export const PriceLevel = priceLevel || [];
  export const AreaList = areaList || [];

  export const ProjectTypes = [
    {text: 'Căn hộ, Chung cư', id: 1},
    {text: 'Cao ốc văn phòng', id: 2},
    {text: 'Trung tâm thương mại', id: 3},
    {text: 'Khu đô thị mới', id: 4},
    {text: 'Khu phức hợp', id: 5},
    {text: 'Nhà ở xã hội', id: 6},
    {text: 'Khu nghỉ dưỡng, Sinh thái', id: 7},
    {text: 'Khu công nghiệp', id: 8},
    {text: 'Dự án khác', id: 9},
    {text: 'Biệt thự, liền kề', id: 10}
  ];

  export const NewsCategories = [
    {text: 'Tin thị trường', id: 1},
    {text: 'Phân tích - Nhận định', id: 2},
    {text: 'Chính sách - quản lý', id: 3},
    {text: 'Thông tin quy hoạch', id: 4},
    {text: 'Bất động sản thế giới', id: 5},
    {text: 'Tài chính - Chứng khoán - BĐS', id: 6},
    {text: 'Tư vấn luật', id: 7, extra: {selectable: false}},
    {text: 'Trình tự, thủ tục', id: 71, extra: {level: 2}},
    {text: 'Quyền sở hữu', id: 72, extra: {level: 2}},
    {text: 'Quyền sở hữu', id: 73, extra: {level: 2}},
    {text: 'Xây dựng - Hoàn công', id: 74, extra: {level: 2}},
    {text: 'Nghĩa vụ tài chính', id: 75, extra: {level: 2}},
    {text: 'Các vấn đề có yếu tố nước ngoài', id: 76, extra: {level: 2}},
    {text: 'Lời khuyên', id: 8, extra: {selectable: false}},
    {text: 'Lời khuyên cho người bán', id: 81, extra: {level: 2}},
    {text: 'Lời khuyên cho người bán', id: 82, extra: {level: 2}},
    {text: 'Lời khuyên cho người bán', id: 83, extra: {level: 2}},
    {text: 'Lời khuyên cho người thuê', id: 84, extra: {level: 2}},
    {text: 'Lời khuyên cho người cho thuê', id: 85, extra: {level: 2}}
  ];

  export const PostTypeSale = 1;
  export const PostTypeBuy = 2;
  export const PostTypeProject = 3;
  export const PostTypeNews = 4;

}
