export namespace File {
  export namespace MimeTypes {
    export const png = 'image/png';
    export const jpg = 'image/jpg';
    export const jpeg = 'image/jpeg';
    export const gif = 'image/gif';
  }

  export enum Types {
    'png',
    'jpg',
    'jpeg',
    'gif'
  }

  export const TypePNG = 'png';
  export const TypeJPG = 'jpg';
  export const TypeJPEG = 'jpeg';
  export const TypeGIF = 'gif';
}
