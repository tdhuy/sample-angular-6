import { Directive, ElementRef, NgZone, OnDestroy, Input } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import { OnInit, AfterContentInit } from "@angular/core/src/metadata/lifecycle_hooks";

@Directive({
  selector: '[appStickyControlButtonsTable]'
})

export class StickyControlButtonsTable implements AfterContentInit {
  _marginTop = 50;
  _elOffsetTop = 0;
  constructor(private el: ElementRef, private zone: NgZone) {
    this.zone.runOutsideAngular(() => {
      Observable.fromEvent(window, 'scroll')
        .subscribe(res => {
          this._onWindowScroll(res);
        });
    });
  }

  private _getScrollTopValue(): number {
    return (window.pageYOffset || document.scrollingElement.scrollTop) - (document.scrollingElement.clientTop || 0);
  }

  private _onWindowScroll(event: any) {
    this._handleTopPosition();
  }

  private _handleTopPosition() {
    const scrollTop = this._getScrollTopValue();
    if (scrollTop + this._marginTop - 2 >= this._elOffsetTop) {
      this.el.nativeElement.style.position = "fixed";
      this.el.nativeElement.style.top = `${this._marginTop}px`;
    } else {
      this.el.nativeElement.style.position = "absolute";
      this.el.nativeElement.style.top = 0;
    }
  }

  private _getOffsetTop(elem): number {
    let offsetTop = 0;
    do {
      if (!isNaN(elem.offsetTop)) {
        offsetTop += elem.offsetTop;
      }
    } while (elem = elem.offsetParent);
    return offsetTop;
  }

  ngAfterContentInit() {
    this._elOffsetTop = this._getOffsetTop(this.el.nativeElement);
  }
}