import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RepeatDoneDirective } from './repeat-done.directive';
import { StickyTableTheadDirective } from './sticky-table-thead.directive';
import { StickyControlButtonsTable } from './sticky-control-button-table.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    RepeatDoneDirective,
    StickyTableTheadDirective,
    StickyControlButtonsTable,
  ],
  exports: [
    RepeatDoneDirective,
    StickyTableTheadDirective,
    StickyControlButtonsTable
  ]
})

export class ShareDirective {}
