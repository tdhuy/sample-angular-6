import {
  Directive,
  ElementRef,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  OnInit
} from '@angular/core';

export class OnSetValueIcheck {
  onSetValue($event) {
    if ($event.target === null || typeof $event.target === 'undefined') {
      console.error('Target is undefined');
      return;
    }

    eval('this.' + $event.target + ' = ' + $event.value);
  }
}

declare var $: any;

@Directive({
  selector: '[icheck]',
  inputs: ['target']
})

export class IcheckDirective implements AfterViewInit, OnChanges {
  $: any;

  target: string;

  constructor(private el: ElementRef) {
    this.$ = $;
  }

  private isInit = true;

  @Input() ngModel: any;
  @Input() value: any;

  @Output() callback: EventEmitter<any> = new EventEmitter();

  ngAfterViewInit(): void {
    this.$(this.el.nativeElement).iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue'
    }).on('ifChanged', (event) => {
      const inputType = event.target.type;

      if (inputType === 'checkbox') {
        this.callback.emit({
          target: this.target,
          value: event.target.checked
        });
      } else {
        this.callback.emit({
          target: this.target,
          value: event.target.value
        });
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    const value = changes.ngModel.currentValue;
    const ele = this.el.nativeElement;

    if (ele.type === 'checkbox') {
      this.$(ele).iCheck(value ? 'check' : 'uncheck');
    } else {
      this.$(ele).iCheck(value === ele.value ? 'check' : 'uncheck');
    }
  }
}
