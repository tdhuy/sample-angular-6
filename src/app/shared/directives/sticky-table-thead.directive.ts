import { Directive, ElementRef, Input, NgZone, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';

@Directive({
  selector: '[appStickyTableThead]'
})
export class StickyTableTheadDirective implements OnDestroy {
  private _eventName = 'scroll';
  private _scrollThs = [];
  private _fixedThs = [];
  private _fixedTHead: any = null;
  private _elOffsetTop = 0;
  private _marginTop = 0;

  @Input()
  set marginTop(value: number) {
    this._marginTop = value;
    this._handleTopPosition();
  }

  @Input()
  set render(value: any) {
    this._init();
  }

  constructor(private el: ElementRef, private zone: NgZone) {
    this.zone.runOutsideAngular(() => {
      Observable.fromEvent(window, 'scroll')
        .subscribe(res => {
          this._onWindowScroll(res);
        });
    });

    setTimeout(() => {
      this._init();
    });
  }

  private _getScrollThs() {
    const theads = this.el.nativeElement.querySelectorAll('thead');
    return theads[0].querySelectorAll('th');
  }

  private _getFixedThs() {
    const theads = this.el.nativeElement.querySelectorAll('thead');
    return theads[1].querySelectorAll('th');
  }

  private _getFixedTHead() {
    return this.el.nativeElement.querySelector('thead.fixed-header');
  }

  private _init() {
    this._fixedThs = this._getFixedThs();
    this._scrollThs = this._getScrollThs();
    this._fixedTHead = this._getFixedTHead();

    this._elOffsetTop = this._getOffsetTop(this.el.nativeElement);
    this._syncWithThs();
  }

  private _getScrollTopValue(): number {
    return (window.pageYOffset || document.scrollingElement.scrollTop) - (document.scrollingElement.clientTop || 0);
  }

  private _onWindowScroll(event: any) {
    this._handleTopPosition();
  }

  private _getOffsetTop(elem): number {
    let offsetTop = 0;
    do {
      if (!isNaN(elem.offsetTop)) {
        offsetTop += elem.offsetTop;
      }
    } while (elem = elem.offsetParent);
    return offsetTop;
  }

  private _syncWithThs() {
    if (this._fixedThs.length !== this._scrollThs.length) {
      return;
    }

    this._scrollThs.forEach((th1, index) => {
      const target = this._fixedThs[index];
      this._copyStyle(th1, target);
    });
  }

  private _handleTopPosition() {
    if (!this._fixedTHead) {
      return;
    }

    const scrollTop = this._getScrollTopValue();
    if (scrollTop + this._marginTop - 1 >= this._elOffsetTop) {
      this._fixedTHead.style.top = `${scrollTop - this._elOffsetTop + this._marginTop - 1}px`;
    } else {
      this._fixedTHead.style.top = 0;
    }
  }

  private _copyStyle(from, to) {
    to.style.width = `${from.offsetWidth}px`;
    to.style.minWidth = `${from.offsetWidth}px`;
    to.style.maxWidth = `${from.offsetWidth}px`;
  }

  ngOnDestroy() {
    window.removeEventListener(this._eventName, () => {});
  }
}
