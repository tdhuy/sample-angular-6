import { Directive, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appRepeatDone]'
})
export class RepeatDoneDirective {
  private _isLoading = false;

  @Input() totalRender = 0;

  @Input('ready')
  set ready(isReady: Boolean) {
    if (isReady) {
      this._isLoading = true;
      setTimeout(() => {
        this.repeatDoneCb.emit('Repeat done');
        this._isLoading = false;
      }, this.totalRender * 50);
    }
  }

  @Output() repeatDoneCb: EventEmitter<any> = new EventEmitter();

}
