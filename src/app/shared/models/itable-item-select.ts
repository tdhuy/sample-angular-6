export interface ITableItemSelected {
  active: Boolean;

  index: number;

  data: {[key: string]: any};
}
