/**
 * 外部定義メッセージのリンクを表します。
 */
export interface MessageLink {

  /**
   * リンクのタイトル
   */
  title: string;

  /**
   * リンク先のURL
   */
  url: string;
}
