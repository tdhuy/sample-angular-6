/**
 * 外部定義できるメッセージを表します。
 */
import { MessageLink } from './message-link';

export interface MessageJson {

  /**
   * 分類
   */
  reason: string;

  /**
   * 本文
   */
  message: string;

  /**
   * 説明
   */
  description?: string;

  /**
   * 外部へのリンク
   */
  links?: MessageLink[];
}
