import { Injectable } from '@angular/core';
import { Message } from './message';
import { MessageJson } from './message-json';
import { MessageType } from './message-type.enum';

@Injectable()
export class MessageService {

  messageData = {};

  constructor() { }

  get(id: string, params: any = {}): Message {
    const base = this.getMessageObj(id);

    return {
      code: `${base.reason}${id}`,
      type: this.getType(base.reason),
      description: base.description ? base.description : '',
      message: this.getReplacedMessage(base.message, params),
      links: base.links.map((link) => {
        return {
          title: link.title,
          url: link.url
        };
      })
    };
  }

  gets(args: { id: string, params?: any }[]): Message[] {
    if (!args) {
      return [];
    }

    const messages: Message[] = [];

    args.forEach((value) => {
      messages.push(this.get(value.id, value.params));
    });

    return messages;
  }

  private getType(reason: string): MessageType {
    if (!reason) {
      throw new Error(`reason is not declared.`);
    }

    const type = reason.substr(0, 1);
    switch (type) {
      case 'S':
        return MessageType.S;
      case 'E':
        return MessageType.E;
      case 'W':
        return MessageType.W;
      case 'I':
        return MessageType.I;
      default:
        throw new Error(`Unknown MessageType. type=${type}`);
    }
  }

  private getReplacedMessage(message: string, params: any) {
    Object.keys(params).forEach((key: any) => {
      message = message.replace(key, params[key]);
    });

    return message;
  }

  private getMessageObj(id: string): MessageJson {
    const obj = (<any>this.messageData)[id];
    if (!obj) {
      throw new Error(`message is not declared. id=${id}`);
    }

    return obj;
  }
}
