export enum MessageType {
  /**
   * S:System
   */
  S,

  /**
   * E:Error
   */
  E,

  /**
   * W:Warning
   */
  W,

  /**
   * I:Information
   */
  I
}
