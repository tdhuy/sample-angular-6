/**
 * 外部定義のメッセージを表します。
 */
import { MessageLink } from './message-link';
import { MessageType } from './message-type.enum';

export interface Message {
  /**
   * コード(英数3桁+数値5桁)
   */
  code: string;

  /**
   * 種別
   */
  type: MessageType;

  /**
   * 本文
   */
  message: string;

  /**
   * 説明
   */
  description: string;

  /**
   * 外部へのリンク
   */
  links: MessageLink[];
}
