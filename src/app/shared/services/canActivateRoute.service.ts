import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, NavigationEnd, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { filter } from 'rxjs/operators';
import { TargetTypes } from '../../layout/shared/constant/aside-link.constant';
import { USER_ROLES } from '../constants/user-roles';
import { AlertService } from './alert.service';
import { SessionService } from './session.service';
import { UserService } from './user/user.service';

@Injectable()
export class CanActivateRoute implements CanActivateChild {
  accessableNavs: TargetTypes[] = [];
  history = [];

  constructor(private session: SessionService,
              private router: Router,
              private alertService: AlertService,
              private userService: UserService) {
    // this.accessableNavs = this.userService.getAvtiveAccessable(this.session.user);

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({urlAfterRedirects}: NavigationEnd) => {
        this.history = [...this.history, urlAfterRedirects];
      });
  }

  canActivateChild(route: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (1) {
      return true;
    }

    if (this.session.user.user_roles.indexOf(USER_ROLES.SuperAdmin[1]) !== -1) {
      return true;
    }

    if (route.data.targetType !== TargetTypes.Dashboard && this.accessableNavs.indexOf(route.data.targetType) === -1) {
      this.alertService.error(['Permission denied when enter url: ' + state.url]);
      if (this.history.length === 0) {
        this.router.navigate(['dashboard']);
      }
      return false;
    }

    return true;
  }
}
