import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { AsideLinkItem } from '../../layout/shared/constant/aside-link.constant';
import { ITableColumn } from '../components/table-control/iTableColumn';

@Injectable()
export class GlobalService {
  asideNavLinks: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  pageLoading: Subject<boolean> = new Subject<boolean>();
  pageLoadingCover: Boolean = true;
  loggedInUser: any;
  sidebarCollapsed: Boolean = false;

  pageLoadingToLoginPage: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {
    this.pageLoading.next(true);
    this.pageLoading.subscribe((value) => {
      this.toggleBodyScroll(value);
    });
  }

  setFullPageLoading(value: boolean) {
    setTimeout(() => {
      this.pageLoading.next(value);
    }, 0);
  }

  getFullPageLoadingStatus(): Observable<boolean> {
    return this.pageLoading.asObservable();
  }

  setFullPageLoadingLogin(value: boolean) {
    this.pageLoadingToLoginPage.next(value);
  }

  getFullPageLoadingLogin(): Observable<boolean> {
    return this.pageLoadingToLoginPage.asObservable();
  }

  getColByColName(columns: ITableColumn[], colNm: string): ITableColumn {
    const filtered = columns.find(item => {
      return item.headerName === colNm;
    });

    return filtered;
  }

  getDistinctByProperty(array: any[], property: string): any[] {
    const flags = [], output = [], l = array.length;
    for (let i = 0; i < l; i++) {
      if (flags[array[i][property]]) {
        continue;
      }

      flags[array[i][property]] = true;
      output.push(array[i]);
    }

    return output;
  }

  private toggleBodyClassLoading() {

  }

  toggleBodyScroll(isHidden: boolean) {
    const body = document.querySelector('body');
    body.style.overflow = isHidden ? 'hidden' : 'auto';
  }
}
