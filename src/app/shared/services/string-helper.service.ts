import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { File } from '../constants/file-type';
import { environment } from 'src/environments/environment';
import { APIs } from 'src/app/shared/constants/api';
import { GlobalConstant } from '../constants/globalConstant';
import directions from '../constants/directions';
import { infoTypes } from 'src/app/shared/constants/infoTypes';

@Injectable()
export class StringHelperService {

  private mimeTypes = [
    File.MimeTypes.png,
    File.MimeTypes.jpg,
    File.MimeTypes.jpeg,
    File.MimeTypes.gif
  ];

  getImageDataURL(url): Observable<any> {
    return Observable.create((observer: Observer<string>) => {
      let data, canvas, ctx;
      const img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
        canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        data = canvas.toDataURL();
        observer.next(data);
      };

      img.src = url;
    });
  }

  genRandomString(len: number = 10): string {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < len; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

  appendStaticPath(url: string): string {
    return url;
  }

  appendHostGetImage(url): string {
    return APIs.getImage.replace('{id}', url);
  }

  isImage(acceptTypes: string[], url): boolean {
    const regex = this._createRegex(acceptTypes);
    url = url.toLowerCase();
    return (url.match(regex) != null);
  }

  convertToSlug(str: string): string {
    return str
      .toLowerCase()
      .replace(/[^\w ]+/g, '')
      .replace(/ +/g, '-')
      ;
  }

  convertToSlug2(str: string): string {
    let slug = str.toLowerCase();
    slug = slug.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    slug = slug.replace(/đ/gi, 'd');
    slug = slug.replace(/\s+/gi, '-');
    return slug;
  }

  getMimeTypeFromExtension(ext: string) {
    switch (ext) {
      case File.TypePNG:
        return File.MimeTypes.png;
      case File.TypeJPG:
        return File.MimeTypes.jpg;
      case File.TypeJPEG:
        return File.MimeTypes.jpeg;
      case File.TypeGIF:
        return File.MimeTypes.gif;
    }

    return '';
  }

  getTypeFromUrl(url: string): string {
    const items = url.split('.');
    if (items.length <= 1) {
      return '';
    }
    return items.pop();
  }

  removePrefixBase64(base64Str: string): string {
    this.mimeTypes.forEach((type) => {
      base64Str = base64Str.replace(`data:${type};base64,`, '');
    });

    return base64Str;
  }

  copyTextToClipboard(text) {
    const txtArea = document.createElement('textarea');

    txtArea.style.position = 'fixed';
    txtArea.style.top = '0';
    txtArea.style.left = '0';
    txtArea.style.opacity = '0';
    txtArea.value = text;
    document.body.appendChild(txtArea);
    txtArea.select();
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'successful' : 'unsuccessful';
      if (successful) {
        return true;
      }
    } catch (err) {
    }
    document.body.removeChild(txtArea);
    return false;
  }

  startOfDay(date: Date): Date {
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date;
  }

  private _createRegex(acceptTypes: string[]): RegExp {
    const expression = `.${acceptTypes.join('|')}$`;
    return new RegExp(expression, 'i');
  }

  getCityByCode(cd: string): any {
    return GlobalConstant.CityListOther1.find(city => {
      return city.code === cd;
    });
  }

  getDistrictByValue(city: any, value: number): any {
    return city.district.find(d => {
      return d.id === value;
    });
  }

  getWardByValue(district: any, value: number): any {
    return district.ward.find(w => {
      return w.id === value;
    });
  }
  getStreetByValue(district: any, value: number): any {
    return district.street.find(w => {
      return w.id === value;
    });
  }
  getProjectByValue(district: any, value: number): any {
    return district.project.find(w => {
      return w.id.toString() === value;
    });
  }

  getBedroomCountByValue(value: number): any {
    if (value > 5)
      return  {text: value.toString() + "+", value: value.toString()};
    if(value > 0 && value < 5)
      return  {text: value.toString() + "+", value: value.toString()};
    return  {text: "Không xác định", value: value.toString()};
  }

  getFormilitySaleByValue(value: number): any {
    return GlobalConstant.CateSaleList.find(c => {
      return c.id === value;
    });
  }

  getFormilityBuyByValue(value: number): any {
    return GlobalConstant.CateBuyList.find(c => {
      return c.id === value;
    });
  }

  getTypeByValue(formality: any, value: number | string): any {
    return formality.children.find(t => {
      return t.id.toString() === value.toString();
    });
  }

  getUnitByValue(formality: any, value: number | string): any {
    // console.log('formality', formality);
    return formality.prices.find(p => {
      return p.id.toString() === value.toString();
    });
  }

  getDirectionsByValue(value: number | string): any {
    if (value === undefined || value == null) {
      return '';
    }

    return directions.find(d => {
      return d.value.toString() === value.toString();
    });
  }

  getPriorityByValue(value: number | string): any {
    return infoTypes.find(i => {
      return i.value.toString() === value.toString();
    });
  }

}
