import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { APIs } from "../constants/api";

@Injectable()
export class PostService {
  constructor(private _http: HttpClient) {}

  getDetail(id: string): Observable<any> {
    return this._http.get(APIs.postDetail.replace('{id}', id));
  }

  updateSeoInfo(id: string, data: any): Observable<any> {
    const url = APIs.updatePostSeo.replace('{id}', id);
    return this._http.post(url, data);
  }
}