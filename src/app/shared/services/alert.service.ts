import { Injectable } from '@angular/core';
import { BsModalRef, ModalOptions } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ChangeUsrBalanceComponent } from '../../alert/change-usr-balance/change-usr-balance.component';
import { ChangeUsrExpirationDateComponent } from '../../alert/change-usr-expiration-date/change-usr-expiration-date.component';
import { SuccessAlertModalComponent } from '../../alert/success-alert-modal/success-alert-modal.component';
import { ErrorAlertModalComponent } from '../../alert/error-alert-modal/error-alert-modal.component';
import { ConfirmAlertModalComponent } from '../../alert/confirm-alert-modal/confirm-alert-modal.component';
import { UpdatePriorityComponent } from '../../alert/update-priority/update-priority.component';
import { UserService } from './user/user.service';
import { IFixedOption } from 'src/app/shared/components/table-control/iTableColumn';
import { UpdateStatusModalComponent } from 'src/app/alert/update-status-modal/update-status-modal.component';
import { UpdateUserTypeComponent } from '../../alert/update-user-type/update-user-type.component';

@Injectable()
export class AlertService {

  public bsModalRef: BsModalRef;

  public configConfirmModal: ModalOptions = {
    animated: true,
    keyboard: false,
    backdrop: true,
    class: 'custom-modal',
    ignoreBackdropClick: true
  };

  constructor(private modalService: BsModalService,
              private userService: UserService) { }

  showSuccess(msgs) {
    const customConfig = {...this.configConfirmModal};
    customConfig.class += ' width-360';
    const modalSucess = this.modalService.show(SuccessAlertModalComponent, customConfig);

    // transfer msgs to modal componnent
    modalSucess.content.msgs = msgs;
  }

  error(msgs) {
    const customConfig = {...this.configConfirmModal};
    customConfig.class += ' width-400';
    const modalError = this.modalService.show(ErrorAlertModalComponent, customConfig);

    // transfer msgs to modal componnent
    modalError.content.msgs = msgs;
  }

  confirm(msgs, callback) {
    const customConfig = {...this.configConfirmModal};
    customConfig.class += ' width-400';
    const modalConfirm = this.modalService.show(ConfirmAlertModalComponent, customConfig);

    // transfer msgs to modal componnent
    modalConfirm.content.msgs = msgs;
    modalConfirm.content.callback = () => {
      modalConfirm.hide();
      callback();
    };
  }

  updateStatus(status: IFixedOption[], selectedStr, callback) {
    const customConfig = {...this.configConfirmModal};
    customConfig.class += ' width-400';
    const modalConfirm = this.modalService.show(UpdateStatusModalComponent, customConfig);

    modalConfirm.content.status = status.filter(s => s.visibleToUpdate !== false);
    modalConfirm.content.selectedItem = status.find(item => item.name === selectedStr);
    modalConfirm.content.callback = (s: any) => {
      modalConfirm.hide();
      callback(s);
    };
  }

  changeUserBalance(user: any, type: string, callback: Function) {
    const customConfig = {...this.configConfirmModal};
    customConfig.class += ' width-500';
    const modalChangeBalance = this.modalService.show(ChangeUsrBalanceComponent, customConfig);

    modalChangeBalance.content.user = user;
    modalChangeBalance.content.type = type;
    modalChangeBalance.content.callback = (s: any) => {
      modalChangeBalance.hide();
      callback(s);
    };
  }

  changeUserExpirationDate(user: any, callback: Function) {
    const customConfig = {...this.configConfirmModal};
    customConfig.class += ' width-500';
    const modalChangeExpiration = this.modalService.show(ChangeUsrExpirationDateComponent, customConfig);

    modalChangeExpiration.content.user = user;
    modalChangeExpiration.content.callback = (s: any) => {
      modalChangeExpiration.hide();
      callback(s);
    };
  }

  updatePriority(priority: any, type, callback: Function) {
    const customConfig = {...this.configConfirmModal};
    customConfig.class += ' width-500';
    const modalUpdatePriority = this.modalService.show(UpdatePriorityComponent, customConfig);

    modalUpdatePriority.content.priority = priority;
    modalUpdatePriority.content.type = type;
    modalUpdatePriority.content.callback = (s: any) => {
      modalUpdatePriority.hide();
      callback(s);
    };
  }

  updateUserType(user: any, type: any, callback: Function) {
    const customConfig = { ...this.configConfirmModal };
    customConfig.class += ' width-400';
    const modalUpdateUserType = this.modalService.show(UpdateUserTypeComponent, customConfig);

    modalUpdateUserType.content.user = user;
    modalUpdateUserType.content.type = type;
    modalUpdateUserType.content.callback = (s: any) => {
      modalUpdateUserType.hide();
      callback(s);
    };

  }
}
