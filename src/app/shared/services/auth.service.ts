import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { APIs } from '../constants/api';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {

  }

  login(params: any) {
    return this.http.post(APIs.Admin.Login, params);
  }

  logout() {
    const promise = new Promise((resolve, reject) => {
      return resolve();
    });

    return promise;
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    for (const property in toConvert) {
      if (toConvert[property]) {
        const encodedKey = encodeURIComponent(property);
        const encodedValue = encodeURIComponent(toConvert[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
    }
    return formBody.join('&');
  }
}
