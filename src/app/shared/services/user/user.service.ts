import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  private _defaultHeaders: HttpHeaders;

  constructor(private _http: HttpClient) {
    this._defaultHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json');
  }
}
