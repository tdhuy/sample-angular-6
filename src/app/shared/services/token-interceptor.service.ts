import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse
} from '@angular/common/http';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { ErrorAlertModalComponent } from '../../alert/error-alert-modal/error-alert-modal.component';
import { GlobalService } from './global.service';
import { SessionService } from './session.service';
import HttpCode from '../constants/http_code';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private sessionService: SessionService,
              private globalService: GlobalService,
              private modalService: BsModalService,
              private router: Router) {
  }

  error(msgs) {
    const customConfig = {
      animated: true,
      keyboard: false,
      backdrop: true,
      class: 'custom-modal',
      ignoreBackdropClick: true
    };
    customConfig.class += ' width-400';
    const modalError = this.modalService.show(ErrorAlertModalComponent, customConfig);

    modalError.content.msgs = msgs;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        access_token: this.sessionService.token
      }
    });

    return next.handle(request)
      .do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.body && event.body) {

            switch (event.body.code) {
              case HttpCode.SUCCESS:
                break;
              case HttpCode.UNAUTHORIZED:
                this.sessionService.remove();
                this.globalService.setFullPageLoadingLogin(true);
                this.router.navigate(['/login']);
                break;
              case HttpCode.PERMISSION_DENIED:
                this.error([event.body.message]);
                this.globalService.setFullPageLoadingLogin(false);
                break;
              default:
                break;
            }
          }
        }
      });
  }
}
