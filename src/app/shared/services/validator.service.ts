import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { IFileValue } from '../components/shared/i-file-value';
import { error } from 'util';
import { ValidationErrors } from '@angular/forms/src/directives/validators';

export namespace ErrorNames {
  export const required = ['required', 'Bắt buộc'];
  export const patternUrl = ['patternUrl', 'Url sai'];
  export const patternNumber = ['patternNumber', 'Chỉ được phéo nhập số'];
  export const patternHotlineNumber = ['patternHotlineNumber', 'Chỉ được phéo nhập số và khoảng trắng'];
  export const fileNotLoad = ['fileNotLoad', 'Chưa có file'];
  export const minLength = ['minLength', 'Quá ngắn'];
  export const maxLength = ['maxLength', 'Quá dài'];
  export const slug = ['slug', 'Slug sai'];
  export const areStoreSelected = ['areaStore', 'Chưa chọn store nào'];
}

@Injectable()
export class ValidatorsService {
  static isURL(str: string): boolean {
    const pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return pattern.test(str);
  }

  public checkUrlSeo(control: AbstractControl) {
    const regex = new RegExp(/^[a-zA-Z0-9\-]+$/, 'g');
    if (!regex.test(control.value)) {
      return { [ErrorNames.patternUrl[0]]: true };
    }
  }

  public checkUrl(control: AbstractControl) {
    const value = control.value;

    if (!ValidatorsService.isURL(value.toString())) {
      return {[ErrorNames.patternUrl[0]]: true};
    }
  }

  public checkFileLoadBase64(control: AbstractControl) {
    const data: IFileValue = control.value;
    if (data.url === '') {
      return {
        fileNotLoad: true
      };
    }
  }

  public checkHotline(control: AbstractControl) {
    const reg = /^(\d|\s)+$/;
    const value = control.value;
    if (!reg.test(value.toString())) {
      return {[ErrorNames.patternHotlineNumber[0]]: true};
    }
  }

  public checkNumber(control: AbstractControl) {
    const reg = /^\d+$/;
    const value = control.value;
    if (!reg.test(value.toString())) {
      return {[ErrorNames.patternNumber[0]]: true};
    }
  }

  public checkMinLength(minLength: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      const errors: ValidationErrors = {};
      const value = control.value;

      if (value.toString().length < minLength) {
        errors[ErrorNames.minLength[0]] = true;
      }
      return errors;
    };
  }

  public checkMaxLength(maxLength: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      const errors: ValidationErrors = {};
      const value = control.value;

      if (value.toString().length > maxLength) {
        errors[ErrorNames.maxLength[0]] = true;
      }
      return errors;
    };
  }

  public checkAlphabet(control: AbstractControl) {
    // TODO
  }

  public checkSlugString(control: AbstractControl) {
    const value = control.value;
    const reg = new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$');
    if (!reg.test(value.toString())) {
      return {[ErrorNames.slug[0]]: true};
    }
  }

  public checkAreaStoreSelected(control: AbstractControl) {
    const value = control.value;
    if (value.length === 0) {
      return {[ErrorNames.areStoreSelected[0]]: true};
    }
  }
}
