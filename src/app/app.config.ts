import { environment } from '../environments/environment';

export interface IAppConfig {
  apiEndpoint: string;
  staticEndPoint: string;
}

export const AppConfig: IAppConfig = {
  apiEndpoint: '',
  staticEndPoint: ''
};
